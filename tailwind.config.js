import { nextui } from '@nextui-org/react'
/** @type {import('tailwindcss').Config} */

export default {
  content: [
    './index.html',
    './src/**/*.{html,js,jsx,tsx}',
    './node_modules/@nextui-org/theme/dist/**/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    extend: {
      colors: {
        background: '#ffffff',
        foreground: '#222',
        primary: '#EC126D', // old : #333
        secondary: '#777',
        gray: '#bababa',
        lightgray: '#d7d7d7',
        success: '#5ac392',
        warning: '#F2AC29',
        danger: '#a12c2c',

        purewhite: '#ffffff',
        white: '#e7e7e7',
        black: '#282828',

        color0: '#101010',
        color1: '#d22a2c',
        color2: '#f5763f',
        color3: '#f0d15d',
        color4: '#89ab54',
        color5: '#67c1d8',
        color6: '#1977aa',
        color7: '#523c7c',
        color8: '#ee5e8a',
        color9: '#f5f6f7',

        focus: '#F182F6',
        google: '#E34133',
        facebook: '#4064AE',
      },
      gridTemplateColumns: {
        'game-header': '60px auto 60px',
      },
    },
  },
  darkMode: 'class',
  plugins: [nextui()],
  safelist: [
    { pattern: /text-./ },
    { pattern: /border-color./ },
    { pattern: /bg-color./ },
    // { pattern: /grid-cols-./, variants: ['sm', 'md', 'lg'] },
    'text-[50px]',
    'border-color2',
  ],
}
