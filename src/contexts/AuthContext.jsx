import React, { useContext, useState, useEffect } from 'react'
import { auth, firestore } from '../firebase/firebase-config'
// import { GoogleAuthProvider } from 'firebase/auth'
import { onAuthStateChanged } from 'firebase/auth'
import { doc, onSnapshot } from 'firebase/firestore'
import PropTypes from 'prop-types'

const AuthContext = React.createContext()

export function useAuth() {
    return useContext(AuthContext)
}

export function AuthProvider({ children }) {
    const [user, setUser] = useState(null)
    const [username, setUsername] = useState('')
    const [photoURL, setPhotoURL] = useState('')
    const [loading, setLoading] = useState(true)
    const [isMe, setIsMe] = useState(false)

    // Supprimer ce qui n'est plus nécessaire
    // const [userLoggedIn, setUserLoggedIn] = useState(false)
    // const [isEmailUser, setIsEmailUser] = useState(false)
    // const [isGoogleUser, setIsGoogleUser] = useState(false)
    // const [isAnonymousUser, setIsAnonymousUser] = useState(false)

    useEffect(() => {
        const unsubscribe = onAuthStateChanged(auth, (user) => {
            if (user) {
                initializeUser(user)
            } else {
                clearUserState()
            }
        })
        return () => unsubscribe()
    }, [])

    async function initializeUser(user) {
        setLoading(true)
        setUser(user)
        await fetchUserDetailsFromFirestore(user.uid)
        if (user.uid === 'PZgb0ZlXeZSSifgIaAm7xt8Qifi1') setIsMe(true)
    }

    async function fetchUserDetailsFromFirestore(uid) {
        const userRef = doc(firestore, 'users', uid)
        try {
            const unsubscribe = await onSnapshot(userRef, (doc) => {
                const data = doc.data()
                setUsername(data.username || '')
                setPhotoURL(data.photoURL || '')

                if (!unsubscribe) console.log('Error while subscribing to User Ref...')
            })
        } catch (error) {
            console.error('Error fetching user details:', error)
        } finally {
            setLoading(false)
        }
    }

    function clearUserState() {
        setUsername('')
        setPhotoURL('')
        setUser(null)
        setLoading(false)
    }

    const value = {
        user,
        setUser,
        username,
        setUsername,
        photoURL,
        setPhotoURL,

        isMe,
    }

    return <AuthContext.Provider value={value}>{!loading && children}</AuthContext.Provider>
}

AuthProvider.propTypes = {
    children: PropTypes.any.isRequired,
}
