import { Button } from '@nextui-org/react'
import PropTypes from 'prop-types'

function ModalConfirm({ onClose, onConfirm, message }) {
    const onConfirmClicked = () => {
        onConfirm()
        onClose()
    }

    return (
        <div className="popup-overlay p-3 text-center">
            <h2 className="text-white">{message}</h2>

            <div className="mt-12 flex gap-3">
                <Button className="w-full" type="submit" color="danger" label="save settings" onClick={onClose}>
                    Non
                </Button>
                <Button
                    className="w-full"
                    type="submit"
                    color="success"
                    label="save settings"
                    onClick={onConfirmClicked}
                >
                    Oui
                </Button>
            </div>
        </div>
    )
}

export default ModalConfirm

ModalConfirm.propTypes = {
    onClose: PropTypes.func.isRequired,
    onConfirm: PropTypes.func.isRequired,
    message: PropTypes.string.isRequired,
}
