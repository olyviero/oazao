import PropTypes from 'prop-types'
import { getCardImageURL } from '@utils/imgs'

export default function Card({ x, y }) {
  return (
    <div className="">
      <img src={getCardImageURL(`${x}.${y}.jpg`)} alt="w-full h-full" />
    </div>
  )
}

Card.propTypes = {
  x: PropTypes.number.isRequired,
  y: PropTypes.number.isRequired,
}
