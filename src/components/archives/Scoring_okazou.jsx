import { useState, useEffect } from 'react'
import { Button, Input, Select, SelectItem } from '@nextui-org/react'
import { customAlert } from '@ui/customAlert'
import MyButton from '@ui/MyButton'
import ScoringItemFast from '@tools/ScoringItemFast'
import ScoringItemSplit from '@tools/ScoringItemSplit'

export default function Scoring() {
  const [players, setPlayers] = useState(() => {
    const savedPlayers = localStorage.getItem('scoring')
    return savedPlayers ? JSON.parse(savedPlayers) : []
  })
  const [newPlayerName, setNewPlayerName] = useState('')
  const [displayMode, setDisplayMode] = useState('fast')

  useEffect(() => {
    console.log(displayMode)
  }, [displayMode])

  useEffect(() => {
    localStorage.setItem('scoring', JSON.stringify(players))
  }, [players])

  const addPlayer = () => {
    if (newPlayerName) {
      setPlayers([...players, { name: newPlayerName, points: 0 }])
      setNewPlayerName('')
    }
  }

  const addPoints = (index, value) => {
    const newPlayers = [...players]
    newPlayers[index].points += value
    setPlayers(newPlayers)
  }

  const removePoints = (index, value) => {
    const newPlayers = [...players]
    newPlayers[index].points -= value
    setPlayers(newPlayers)
  }

  const confirmDeletePlayer = (index) => {
    customAlert('Êtes-vous sûr de vouloir supprimer ce joueur ?', () => deletePlayer(index))
  }

  const deletePlayer = (index) => {
    const newPlayers = players.filter((_, idx) => idx !== index)
    setPlayers(newPlayers)
  }

  const handleResetPlayers = () => {
    customAlert('Supprimer les joueurs ET leurs scores ?', () => resetPlayers())
  }

  const resetPlayers = () => {
    localStorage.removeItem('scoring')
    setPlayers([])
  }

  const handleResetScores = () => {
    customAlert('Garder les joueurs mais réinitialiser les scores ?', () => resetScores())
  }

  const resetScores = () => {
    const playersWithResetScores = players.map((player) => ({
      ...player,
      points: 0,
    }))
    setPlayers(playersWithResetScores)
    localStorage.setItem('scoring', JSON.stringify(playersWithResetScores)) // Sauvegarder l'état mis à jour dans localStorage
  }

  return (
    <div>
      <div className="flex flex-col justify-between gap-3">
        <div className="fbc gap-3">
          <MyButton classNames="bg-color1" onClick={handleResetPlayers}>
            Réinitialiser
          </MyButton>
          <MyButton classNames="bg-color2" onClick={handleResetScores}>
            Scores à 0
          </MyButton>
          <Select label="Affichage" className="max-w-xs" defaultSelectedKeys={['fast']} onChange={(e) => setDisplayMode(e.target.value)}>
            <SelectItem key={'fast'}>Rapide</SelectItem>
            <SelectItem key={'split'}>En 2 temps</SelectItem>
          </Select>
        </div>
        <div className="flex items-center gap-3">
          <Input
            clearable
            bordered
            fullWidth
            maxLength="23"
            size="lg"
            placeholder="Nom du joueur"
            value={newPlayerName}
            onChange={(e) => setNewPlayerName(e.target.value)}
          />
          <Button color="success" onClick={addPlayer}>
            + Joueur
          </Button>
        </div>
      </div>
      {displayMode && displayMode === 'fast' ? (
        <div className="mt-10 flex flex-col gap-3">
          {players &&
            players.map((player, index) => (
              <ScoringItemFast
                key={index}
                player={player}
                index={index}
                confirmDeletePlayer={confirmDeletePlayer}
                addPoints={addPoints}
                removePoints={removePoints}
              />
            ))}
        </div>
      ) : (
        <div className="mt-10 grid w-full grid-cols-[repeat(auto-fill,minmax(250px,1fr))] gap-3">
          {players &&
            players.map((player, index) => (
              <ScoringItemSplit
                key={index}
                player={player}
                index={index}
                confirmDeletePlayer={confirmDeletePlayer}
                addPoints={addPoints}
                removePoints={removePoints}
              />
            ))}
        </div>
      )}
    </div>
  )
}
