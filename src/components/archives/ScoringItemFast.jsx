import MyButton from '@ui/MyButton'

export default function ScoringItemFast({ player, index, confirmDeletePlayer, addPoints, removePoints }) {
  return (
    <div key={index}>
      <div className="fccc gap-3 rounded-md border-1 border-gray p-3 md:flex-row md:gap-10">
        <div className="fbc w-full">
          <div className="fcc gap-3">
            <button className="text-xl" onClick={() => confirmDeletePlayer(index)}>
              🗑️
            </button>
            <div className="text-medium font-bold">{player.name}</div>
          </div>
          <div className="text-2xl">{player.points}</div>
        </div>

        <div className="fbc gap-1 md:max-w-[400px] md:justify-end">
          <MyButton classNames="bg-purewhite text-color1 border-color1 border-1 min-w-[0px] w-full" onClick={() => removePoints(index, 10)}>
            - 10
          </MyButton>
          <MyButton classNames="bg-purewhite text-color1 border-color1 border-1 min-w-[0px] w-full" onClick={() => removePoints(index, 5)}>
            - 5
          </MyButton>
          <MyButton classNames="bg-purewhite text-color1 border-color1 border-1 min-w-[0px] w-full" onClick={() => removePoints(index, 1)}>
            - 1
          </MyButton>
          <div className="flex min-w-[50px]"></div>
          <MyButton classNames="bg-purewhite text-color4 border-color4 border-1 min-w-[0px] w-full" onClick={() => addPoints(index, 1)}>
            + 1
          </MyButton>
          <MyButton classNames="bg-purewhite text-color4 border-color4 border-1 min-w-[0px] w-full" onClick={() => addPoints(index, 5)}>
            + 5
          </MyButton>
          <MyButton classNames="bg-purewhite text-color4 border-color4 border-1 min-w-[0px] w-full" onClick={() => addPoints(index, 10)}>
            + 10
          </MyButton>
        </div>
      </div>
    </div>
  )
}
