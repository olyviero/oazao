import MyButton from '@ui/MyButton'
import { GrEdit } from 'react-icons/gr'
import { RiDeleteBin2Line } from 'react-icons/ri'

export default function ScoringItemSplit({ player, index, confirmDeletePlayer, addPoints, removePoints }) {
  return (
    <div key={index} className="fccc gap-3 rounded-md border-1 border-gray p-3">
      <div className="fbc">
        <div className="text-xl font-bold">{player.name}</div>
        <div className="text-4xl">{player.points}</div>
      </div>
      <div className="fbc gap-1">
        <MyButton classNames="bg-color1" onClick={() => confirmDeletePlayer(index)}>
          <RiDeleteBin2Line size={18} />
        </MyButton>

        <MyButton classNames="" onClick={() => addPoints(index, 10)}>
          <GrEdit size={18} />
        </MyButton>
      </div>
    </div>
  )
}
