import { useAuth } from '../contexts/AuthContext'
import { useState, useEffect } from 'react'
import { addDoc, collection, query, where, getDocs } from 'firebase/firestore' // Utilise getDocs au lieu de getDocFromServer
import { firestore } from '../firebase/firebase-config'
import { Select, Button, SelectItem, Textarea } from '@nextui-org/react'

export default function SuggestionsBox() {
    const { user } = useAuth()
    const [type, setType] = useState('')
    const [text, setText] = useState('')
    const [isSent, setIsSent] = useState(false)
    const [suggestions, setSuggestions] = useState([])

    const handleSuggestionSubmit = async () => {
        if (text === '') return
        await addDoc(collection(firestore, 'suggestions'), {
            userId: user.uid,
            type,
            text,
            createdAt: Date.now(),
        })

        setIsSent(true)
        setType('')
        setText('')
        fetchSuggestions() // Refetch suggestions after adding a new one
    }

    const handleSelectType = (e) => {
        setType(e.target.value)
    }

    const fetchSuggestions = async () => {
        const suggestionsQuery = query(collection(firestore, 'suggestions'), where('userId', '==', user.uid))
        const querySnapshot = await getDocs(suggestionsQuery) // Corrige cette ligne
        const fetchedSuggestions = querySnapshot.docs.map((doc) => ({
            id: doc.id,
            ...doc.data(),
        }))
        setSuggestions(fetchedSuggestions)
    }

    useEffect(() => {
        if (user) {
            fetchSuggestions()
        }
    }, [user])

    useEffect(() => {
        console.log(suggestions)
    }, [suggestions])

    return (
        <div className="flex flex-col gap-3 rounded-xl bg-secondary/20 p-3">
            <h3>Suggestions</h3>
            <Select label="Sujet" className="w-full" onChange={handleSelectType}>
                <SelectItem key="gameIdea" value="gameIdea">
                    Idée de jeu
                </SelectItem>
                <SelectItem key="funcIdea" value="funcIdea">
                    Idée de fonctionnalité
                </SelectItem>
                <SelectItem key="bugReport" value="bugReport">
                    Bug
                </SelectItem>
                <SelectItem key="other" value="other">
                    Autre
                </SelectItem>
            </Select>

            <Textarea
                variant="bordered"
                type="text"
                color="primary"
                value={text}
                label="🐱 Exprimez-vous !"
                onChange={(e) => setText(e.target.value)}
                required
            />
            <Button type="submit" color={isSent ? 'success' : 'primary'} onClick={handleSuggestionSubmit}>
                {isSent ? 'Merci ! ❤️' : 'Envoyer'}
            </Button>

            <div className="mt-4">
                <h4>Vos suggestions précédentes :</h4>
                {suggestions.map((s) => (
                    <div key={s.id} className="border-t border-gray-200 p-2">
                        <p>Type: {s.type}</p>
                        <p>Texte: {s.text}</p>
                        <p>Date: {s.createdAt && new Date(s.createdAt).toLocaleString()}</p>
                    </div>
                ))}
            </div>
        </div>
    )
}
