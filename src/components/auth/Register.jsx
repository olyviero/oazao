import React, { useState } from 'react'
import { Navigate, Link, useNavigate } from 'react-router-dom'
import { useAuth } from '../../contexts/AuthContext'
import { doCreateUserWithEmailAndPassword } from '../../firebase/auth'

import { Input, Button } from '@nextui-org/react'
import logo from '../../assets/logo.png'

const Register = () => {
  const navigate = useNavigate()

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [username, setUsername] = useState('')
  // const [confirmPassword, setconfirmPassword] = useState('')
  const [isRegistering, setIsRegistering] = useState(false)
  const [errorMessage, setErrorMessage] = useState('')

  const { userLoggedIn } = useAuth()

  const onSubmit = async (e) => {
    e.preventDefault()
    if (!isRegistering) {
      setIsRegistering(true)
      await doCreateUserWithEmailAndPassword(username, email, password)
    }
  }

  return (
    <>
      {userLoggedIn && <Navigate to={'/home'} replace={true} />}
      <img src={logo} alt="logo oazao" className="mx-auto my-14 max-w-80" />
      <main className="flex w-full place-content-center place-items-center self-center">
        <div className="w-96 space-y-5 rounded-xl p-3">
          <div className="mb-3 text-center">
            <div className="mt-3">
              <h3 className="text-secondary">Create a New Account</h3>
            </div>
          </div>
          <form onSubmit={onSubmit} className="space-y-4">
            <Input
              variant="bordered"
              type="text"
              value={username}
              label="Pseudo (optionnel)"
              onChange={(e) => setUsername(e.target.value)}
            />
            <Input
              variant="bordered"
              type="email"
              value={email}
              label="Email"
              onChange={(e) => setEmail(e.target.value)}
              required
            />
            <Input
              variant="bordered"
              type="password"
              value={password}
              label="Mot de passe (min 6 caractères)"
              onChange={(e) => setPassword(e.target.value)}
              required
            />
            {/* <Input
              variant="bordered"
              disabled={isRegistering}
              type="password"
              value={confirmPassword}
              label="password confirmation"
              onChange={(e) => setconfirmPassword(e.target.value)}
              required
            /> */}

            {errorMessage && <span className="font-bold text-red-600">{errorMessage}</span>}

            <Button
              // disabled={isRegistering}
              type="submit"
              className={`bg-primary w-full ${isRegistering ? 'bg-gray-300 cursor-not-allowed' : 'transition-color'}`}
            >
              {isRegistering ? "En cours d'enregistrement" : "S'enregistrer"}
            </Button>
          </form>
          <p className="text-center">
            Déjà membre ? <Link to="/login">Se connecter</Link>
          </p>
        </div>
      </main>
    </>
  )
}

export default Register
