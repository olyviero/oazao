import { useState } from 'react'
import { Navigate, useNavigate, Link } from 'react-router-dom'
import { doSignInWithEmailAndPassword, doSignInWithGoogle, doSignInAnonymously } from '@/firebase/auth'
import { useAuth } from '@/contexts/AuthContext'
import { Input } from '@nextui-org/react'
import SeparatorWithWord from '@/components/ui/SeparatorWithWord'
import Spacer from '@ui/Spacer'
import MyInput from '@/components/ui/MyInput'
import MyButton from '@/components/ui/MyButton'

import InstallPWA from '@components/InstallPWA'
// import logo from '../../assets/logo.png'

const Login = () => {
  const { userLoggedIn } = useAuth()

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [username, setUsername] = useState('')
  const [isSigningIn, setIsSigningIn] = useState(false)

  const navigate = useNavigate()

  const onSubmit = async (e) => {
    e.preventDefault()
    if (!isSigningIn) {
      setIsSigningIn(true)
      await doSignInWithEmailAndPassword(email, password)
      // TODO : doSendEmailVerification()
    }
  }

  const onGoogleSignIn = (e) => {
    e.preventDefault()
    if (!isSigningIn) {
      setIsSigningIn(true)
      doSignInWithGoogle()
        .then(() => navigate('/profile'))
        .catch((err) => {
          console.log(err)
          setIsSigningIn(false)
        })
    }
  }

  const onAnonymousSignIn = (e) => {
    e.preventDefault()
    if (!isSigningIn && username !== '') {
      setIsSigningIn(true)
      doSignInAnonymously(username)
        .then(() => navigate('/profile'))
        .catch((err) => {
          console.log(err)
          setIsSigningIn(false)
        })
    }
  }

  return (
    <div>
      {userLoggedIn && <Navigate to={'/profile'} replace={true} />}
      <main className="flex w-full place-content-center place-items-center self-center">
        <div className="w-96 space-y-5 p-3">
          <h2>Bienvenue</h2>
          <p>Tu dois être connecté pour accéder au contenu</p>
          <MyButton
            onClick={(e) => {
              onGoogleSignIn(e)
            }}
            fullWidth
            classNames={`${isSigningIn ? 'bg-gray-300 cursor-not-allowed' : 'transition-color'}`}
          >
            <svg className="h-5 w-5" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
              <g clipPath="url(#clip0_17_40)">
                <path
                  d="M47.532 24.5528C47.532 22.9214 47.3997 21.2811 47.1175 19.6761H24.48V28.9181H37.4434C36.9055 31.8988 35.177 34.5356 32.6461 36.2111V42.2078H40.3801C44.9217 38.0278 47.532 31.8547 47.532 24.5528Z"
                  fill="#4285F4"
                />
                <path
                  d="M24.48 48.0016C30.9529 48.0016 36.4116 45.8764 40.3888 42.2078L32.6549 36.2111C30.5031 37.675 27.7252 38.5039 24.4888 38.5039C18.2275 38.5039 12.9187 34.2798 11.0139 28.6006H3.03296V34.7825C7.10718 42.8868 15.4056 48.0016 24.48 48.0016Z"
                  fill="#34A853"
                />
                <path
                  d="M11.0051 28.6006C9.99973 25.6199 9.99973 22.3922 11.0051 19.4115V13.2296H3.03298C-0.371021 20.0112 -0.371021 28.0009 3.03298 34.7825L11.0051 28.6006Z"
                  fill="#FBBC04"
                />
                <path
                  d="M24.48 9.49932C27.9016 9.44641 31.2086 10.7339 33.6866 13.0973L40.5387 6.24523C36.2 2.17101 30.4414 -0.068932 24.48 0.00161733C15.4055 0.00161733 7.10718 5.11644 3.03296 13.2296L11.005 19.4115C12.901 13.7235 18.2187 9.49932 24.48 9.49932Z"
                  fill="#EA4335"
                />
              </g>
              <defs>
                <clipPath id="clip0_17_40">
                  <rect width="48" height="48" fill="white" />
                </clipPath>
              </defs>
            </svg>
            {isSigningIn ? 'Connexion...' : 'Continuer avec Google'}
          </MyButton>

          <SeparatorWithWord word="ou" />

          <form onSubmit={onSubmit} className="space-y-5">
            <Input
              variant="bordered"
              type="email"
              color="primary"
              value={email}
              label="email"
              onChange={(e) => setEmail(e.target.value)}
              required
              classNames={{
                inputWrapper: 'focus-within:!border-color4 focus-within:border-2',
              }}
            />

            <Input
              variant="bordered"
              type="password"
              color="primary"
              value={password}
              label="password"
              onChange={(e) => setPassword(e.target.value)}
              required
              classNames={{
                inputWrapper: 'focus-within:!border-color4 focus-within:border-2',
              }}
            />

            <MyButton
              onClick={(e) => {
                onAnonymousSignIn(e)
              }}
              fullWidth
              classNames={`${isSigningIn ? 'bg-gray-300 cursor-not-allowed' : 'transition-color'}`}
            >
              {isSigningIn ? 'Connexion...' : 'Se connecter'}
            </MyButton>

            <SeparatorWithWord word="ou" />

            <MyInput
              variant="bordered"
              value={username}
              color="primary"
              type="text"
              callback={(e) => setUsername(e.target.value)}
              label="pseudo"
              required={true}
            />

            <MyButton
              onClick={(e) => {
                onAnonymousSignIn(e)
              }}
              fullWidth
              classNames={`${isSigningIn ? 'bg-gray-300 cursor-not-allowed' : 'transition-color'}`}
            >
              {isSigningIn ? 'Connexion ...' : 'Connexion anonyme'}
            </MyButton>
          </form>
          <p className="text-center">
            Pas encore membre ? <Link to="/register">S'enregistrer</Link>
          </p>
        </div>
      </main>

      <Spacer size={1} />

      <div className="fccc">
        <InstallPWA />
      </div>
    </div>
  )
}

export default Login
