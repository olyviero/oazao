import { Splide, SplideSlide, SplideTrack } from '@splidejs/react-splide'
import '@splidejs/react-splide/css'

export default function Slider() {
  return (
    <div className="w-full">
      <Splide
        options={{
          type: 'loop',
          cover: true,
          height: '500px',
          arrows: false,
          autoplay: true,
          interval: 5000,
        }}
        hasTrack={false}
        aria-label="My Favorite Images"
      >
        <SplideTrack>
          <SplideSlide>
            <img src="/imgs/banners/banner-main.jpg" alt="Image 1" />
          </SplideSlide>
          <SplideSlide>
            <img src="/imgs/banners/banner-sf.jpg" alt="Image 1" />
          </SplideSlide>
        </SplideTrack>

        {/* <div className="splide__arrows" /> */}
      </Splide>
    </div>
  )
}
