import { useRef, useState } from 'react'
import PropTypes from 'prop-types'
import { Button } from '@nextui-org/react'
import { getStorage, ref as storageRef, uploadBytes, getDownloadURL } from 'firebase/storage'

import MyButton from '@ui/MyButton'

export default function ImageUploader({ setThumbnailUrl, gameKey, thumbnailUrl, callBack }) {
  const MAX_FILE_SIZE = 400 * 1024 // 400 Ko

  const [file, setFile] = useState(null)
  const fileInputRef = useRef(null)

  const handleFileChange = (event) => {
    const file = event.target.files[0]
    if (file) {
      if (file.size > MAX_FILE_SIZE) {
        alert('File size exceeds the 400KB limit.')
      } else {
        setFile(file)
      }
    }
  }

  const uploadImage = async () => {
    if (!file) {
      alert('Please select an image to upload.')
      return
    }
    const storage = getStorage()
    const imageRef = storageRef(storage, `gamesThumbnails/${gameKey}`)
    try {
      const snapshot = await uploadBytes(imageRef, file)
      const imageUrl = await getDownloadURL(snapshot.ref)
      setThumbnailUrl(imageUrl) // Update the thumbnail URL in the parent component
      // callBack()
      console.log('Image uploaded successfully!')
    } catch (error) {
      console.error('Error uploading image: ', error)
      alert('Failed to upload image.')
    }
  }

  return (
    <div className="flex gap-3">
      <input type="file" onChange={handleFileChange} ref={fileInputRef} style={{ display: 'none' }} />
      <MyButton classNames="bg-color2" onClick={() => fileInputRef.current.click()}>
        Choisir une image
      </MyButton>

      <MyButton classNames="bg-color4" onClick={uploadImage}>
        Upload
      </MyButton>
      <img src={thumbnailUrl} className="max-w-20" />
    </div>
  )
}

ImageUploader.propTypes = {
  setThumbnailUrl: PropTypes.func.isRequired,
  gameKey: PropTypes.string.isRequired,
  callback: PropTypes.func,
}
