import { useParams, useNavigate } from 'react-router-dom'
import { useState, useEffect } from 'react'
import { Input, Textarea, Switch } from '@nextui-org/react'
import MyButton from '@ui/MyButton'

import { getToolByName } from '@utils/games'

import { GrEdit } from 'react-icons/gr'
import { FaPlus } from 'react-icons/fa'
import GroupItem from '@tools/GroupItem'
import EntityItem from '@tools/EntityItem'

export default function GroupsManager() {
  const navigate = useNavigate()
  let { groupId } = useParams()

  const [groups, setGroups] = useState(() => {
    const savedGroups = localStorage.getItem('groups')
    return savedGroups ? JSON.parse(savedGroups) : []
  })

  // Routing
  const [currentScreen, setCurrentScreen] = useState('list')

  // Creation
  const [isTeam, setIsTeam] = useState(false)
  const [groupName, setGroupName] = useState('')
  const [groupDescription, setGroupDescription] = useState('')
  const [entities, setEntities] = useState([])

  const [entityName, setEntityName] = useState('')

  const handleAddEntity = () => {
    if (entityName) {
      setEntities([...entities, { id: entities.length, name: entityName, score: 0 }])
      setEntityName('')
    }
  }

  const handleDeleteEntity = (id) => {
    const newPlayers = entities.filter((entity) => entity.id !== id)
    setEntities(newPlayers)
  }

  const handleCreateOrUpdateGroup = () => {
    let updatedGroups = groups.map((group) => ({ ...group, isCurrent: false }))
    let groupUpdated = false

    if (groupId) {
      const id = parseInt(groupId, 10)
      const existingGroupIndex = updatedGroups.findIndex((group) => group.id === id)

      if (existingGroupIndex !== -1) {
        updatedGroups[existingGroupIndex] = {
          ...updatedGroups[existingGroupIndex],
          name: groupName,
          description: groupDescription,
          isTeam: isTeam,
          entities: entities,
          isCurrent: true,
        }
        groupUpdated = true
      }
    } else {
      const newGroup = {
        id: Math.max(...groups.map((g) => g.id)) + 1, // Ajustement pour éviter les conflits d'ID
        name: groupName,
        description: groupDescription,
        isTeam: isTeam,
        entities: entities,
        isCurrent: true,
      }
      updatedGroups.push(newGroup)
      groupUpdated = true
    }

    if (groupUpdated) {
      setGroups(updatedGroups)
      setGroupName('')
      setGroupDescription('')
      setEntities([])
      setCurrentScreen('list')

      // Assurez-vous de mettre la navigation dans un effet pour exécuter après les mises à jour d'état
      setTimeout(() => {
        navigate(`/tools/${getToolByName('groups-manager')}`)
      }, 0)
    }
  }

  const handleCancelCreateGroup = () => {
    groupId = null
    setCurrentScreen('list')
    navigate(`/tools/${getToolByName('groups-manager')}`)
  }

  const handleDeleteGroup = (id) => {
    const updatedGroups = groups.filter((group) => group.id !== id)
    setGroups(updatedGroups)
  }

  // Save data on change
  useEffect(() => {
    localStorage.setItem('groups', JSON.stringify(groups))
  }, [groups])

  // Detect edition mode
  useEffect(() => {
    if (groupId) {
      const savedGroups = localStorage.getItem('groups')
      if (savedGroups) {
        const parsedGroups = JSON.parse(savedGroups)
        const group = parsedGroups.find((group) => group.id === parseInt(groupId, 10))

        if (group) {
          setGroupName(group.name)
          setGroupDescription(group.description)
          setIsTeam(group.isTeam)
          setEntities(group.entities)
          setCurrentScreen('edition')
        }
      }
    }
  }, [groupId])

  return (
    <div className="fccc mt-10 w-full gap-3">
      {currentScreen === 'list' && (
        <>
          <MyButton classNames="bg-color4 w-full" onClick={() => setCurrentScreen('creation')}>
            <FaPlus /> Nouveau groupe
          </MyButton>

          <div className="fcbc w-full gap-3">
            {groups.map((group) => (
              <GroupItem key={group.id} group={group} handleDeleteGroup={handleDeleteGroup} />
            ))}
          </div>
        </>
      )}
      {(currentScreen === 'creation' || currentScreen === 'edition') && (
        <>
          <h3>{groupId ? 'Edition' : 'Création'}</h3>
          <div className="fcbc gap-3">
            <Switch isSelected={isTeam} onValueChange={setIsTeam}>
              Mode équipes
            </Switch>
            <Input label="Nom du groupe" value={groupName} onChange={(e) => setGroupName(e.target.value)} />
            <Textarea
              label="Description"
              value={groupDescription}
              onChange={(e) => setGroupDescription(e.target.value)}
            />
          </div>
          <h3>{isTeam ? 'Equipes' : 'Joueurs'}</h3>
          <div className="fbc gap-3">
            <Input
              clearable
              bordered
              fullWidth
              maxLength="23"
              size="lg"
              placeholder={`${isTeam ? "Nom d'équipe..." : 'Nom du joueur...'}`}
              value={entityName}
              onChange={(e) => setEntityName(e.target.value)}
            />
            <MyButton classNames="bg-color4" onClick={handleAddEntity}>
              + Ajouter
            </MyButton>
          </div>

          <div className="fcbc gap-3">
            {entities &&
              entities.map((entity, index) => (
                <EntityItem
                  key={index}
                  entity={entity}
                  handleDeleteEntity={handleDeleteEntity}
                  isTeam={isTeam}
                  isInGame={false}
                />
              ))}
          </div>

          <div className="fbc mt-20">
            <MyButton classNames="bg-color1" onClick={handleCancelCreateGroup}>
              Annuler
            </MyButton>
            <MyButton classNames="bg-color4" onClick={handleCreateOrUpdateGroup}>
              {groupId ? (
                <>
                  <GrEdit /> Modifier groupe
                </>
              ) : (
                <>
                  <FaPlus /> Créer Groupe
                </>
              )}
            </MyButton>
          </div>
        </>
      )}

      {currentScreen === 'group' && (
        <div>
          <h2>Détails du groupe</h2>
          {/* Afficher les détails du groupe */}
          <MyButton classNames="bg-color4" onClick={() => setCurrentScreen('list')}>
            Retour à la liste
          </MyButton>
        </div>
      )}
    </div>
  )
}
