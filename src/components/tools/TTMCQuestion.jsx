import ReactFlipCard from 'reactjs-flip-card'
import MyButton from '../ui/MyButton'

import { GiCheckedShield } from 'react-icons/gi'

const styles = {
  card: {
    backgroundColor: '#FCFCFC',
    cursor: 'pointer',
    borderRadius: '0.5rem',
    border: '1px solid gray',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
    textAlign: 'center',
  },
}

export default function TTMCQuestion({ difficulty, question, color }) {
  return (
    <div className="fccc gap-10">
      <div className={`text-4xl font-bold text-color${color} text-center `}>
        <GiCheckedShield /> {difficulty}/10
      </div>

      {/* <div className="w-[400px] !max-w-[100%]"> */}
      <div>
        <ReactFlipCard
          frontStyle={styles.card}
          backStyle={styles.card}
          containerStyle={{
            width: '500px',
            maxWidth: '80vw',
            height: '200px',
          }}
          frontComponent={
            <>
              <h3 className="mt-0">Question</h3>
              <div className="flex">{question.question}</div>
              <p className="text-tiny text-color4">retourner</p>
            </>
          }
          backComponent={
            <>
              <h3>Réponse</h3>
              <div className="flex">{question.answer}</div>
            </>
          }
          flipTrigger="onClick"
        />
      </div>
    </div>
  )
}
