import { useState, useEffect } from 'react'
import { Button, Input, Select, SelectItem } from '@nextui-org/react'
import { customAlert } from '@ui/customAlert'
import MyButton from '@ui/MyButton'

import { ImCross } from 'react-icons/im'
import { GiBlaster } from 'react-icons/gi'

export default function EntityItem({ entity, handleDeleteEntity, handleEntityClick, isTeam, isInGame }) {
  const confirmDeleteEntity = () => {
    customAlert(`Êtes-vous sûr de vouloir supprimer ${isTeam ? 'cette équipe' : 'ce joueur'} ?`, () =>
      handleDeleteEntity(entity?.id)
    )
  }

  return (
    <div className="fbc gap-3">
      {!isInGame && (
        <MyButton classNames="p-1 bg-transparent min-w-[Opx] text-color1" onClick={confirmDeleteEntity}>
          <ImCross size={30} />
        </MyButton>
      )}

      <div className={`fbc rounded-md border-1 p-3 ${entity.wasLastEdited ? 'border-2 border-color4' : ''}`}>
        <MyButton classNames="bg-transparent text-black w-full" onClick={() => handleEntityClick(entity.id)}>
          <div className="fbc gap-3">
            <div className="fcc gap-3 font-bold">
              {entity?.name || ''}
              {entity.wasLastEdited && <GiBlaster className="text-color4" size={30} />}
            </div>
            <div className="font-bold">{entity?.score || 0}</div>
          </div>
        </MyButton>
      </div>
    </div>
  )
}
