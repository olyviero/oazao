import { useState, useEffect } from 'react'
import { Button, Input, Switch } from '@nextui-org/react'
import { customAlert } from '@ui/customAlert'
import MyButton from '@ui/MyButton'
import MyNumberInput from '@ui/MyNumberInput'

import { ImCross } from 'react-icons/im'
import { FaPlus } from 'react-icons/fa6'
import { FaMinus } from 'react-icons/fa'
import { FaSave } from 'react-icons/fa'

import NumKeyboard from '@ui/NumKeyboard'

export default function EntityItemEdit({ currentEntity, handleCancelEdit, handleSaveEdit, pointsGoDown, keyboard }) {
  const [pointsDiff, setPointsDiff] = useState(0)
  const [operation, setOperation] = useState('add')
  const [newScore, setNewScore] = useState(0)

  useEffect(() => {
    setOperation(pointsGoDown ? 'remove' : 'add')
  }, [pointsGoDown])

  useEffect(() => {
    if (operation === 'add') {
      setNewScore(Number(currentEntity.score + pointsDiff))
    }
    if (operation === 'remove') {
      setNewScore(Number(currentEntity.score - pointsDiff))
    }
  }, [pointsDiff, operation, currentEntity])

  const handleAddPointsDiff = (pts) => {
    setPointsDiff(pointsDiff + pts)
  }

  const handleConcatPoints = (value) => {
    setPointsDiff(Number(`${pointsDiff}${value}`))
  }

  return (
    <div className="fccc w-full gap-6">
      <div className="fcc font-bold">
        <div className="text-3xl">{currentEntity.name}</div>
        {/* <MyButton
          classNames="bg-color1"
          // onClick={confirmDeleteEntity}
        >
          <ImCross size={30} /> Joueur
        </MyButton> */}
      </div>

      <div className="fac">
        <div className="fccc">
          <div className="font-bold text-gray">score actuel</div>
          <div className="text-[60px] leading-[7rem] text-gray md:text-[120px]">{currentEntity.score}</div>
        </div>

        <div className="fccc">
          <div
            className={`font-bold ${
              (pointsGoDown && operation === 'remove') || (!pointsGoDown && operation === 'add')
                ? 'text-color4'
                : 'text-color1'
            }`}
          >
            nouveau score
          </div>
          <div
            className={`text-[60px] md:text-[120px] leading-[7rem] ${
              (pointsGoDown && operation === 'remove') || (!pointsGoDown && operation === 'add')
                ? 'text-color4'
                : 'text-color1'
            }`}
          >
            {newScore}
          </div>
        </div>
      </div>

      <div className="fcc gap-3">
        <div className="fccc gap-2">
          <Switch
            defaultSelected={pointsGoDown}
            size="lg"
            color="default"
            startContent={<FaMinus />}
            endContent={<FaPlus />}
            onChange={() => setOperation(operation === 'add' ? 'remove' : 'add')}
          />
          {operation === 'add' ? 'Ajouter' : 'Retirer'}
        </div>
        <MyNumberInput
          variant="bordered"
          value={pointsDiff}
          color="primary"
          min={0}
          max={999999999}
          noBtns
          fontSize="50px"
          type="number"
          width="100px"
          callback={(e) => setPointsDiff(Number(e.target.value))}
          required={true}
        />
        <MyButton classNames="bg-color2 h-[60px]" onClick={() => setPointsDiff(0)}>
          <ImCross size={30} />
        </MyButton>
      </div>

      {keyboard === 'default' && <NumKeyboard callback={handleConcatPoints} />}

      {keyboard === 'steps' && (
        <div className="fccc w-full gap-3">
          <div className="grid w-full grid-cols-5 gap-3">
            <MyButton
              classNames="bg-purewhite text-black border-black border-1 min-w-[0px]"
              onClick={() => handleAddPointsDiff(1)}
            >
              1
            </MyButton>
            <MyButton
              classNames="bg-purewhite text-black border-black border-1 min-w-[0px]"
              onClick={() => handleAddPointsDiff(2)}
            >
              2
            </MyButton>
            <MyButton
              classNames="bg-purewhite text-black border-black border-1 min-w-[0px]"
              onClick={() => handleAddPointsDiff(3)}
            >
              3
            </MyButton>
            <MyButton
              classNames="bg-purewhite text-black border-black border-1 min-w-[0px]"
              onClick={() => handleAddPointsDiff(4)}
            >
              4
            </MyButton>
            <MyButton
              classNames="bg-purewhite text-black border-black border-1 min-w-[0px]"
              onClick={() => handleAddPointsDiff(5)}
            >
              5
            </MyButton>
          </div>
          <div className="grid w-full grid-cols-5 gap-3">
            <MyButton
              classNames="bg-purewhite text-black border-black border-1 min-w-[0px]"
              onClick={() => handleAddPointsDiff(10)}
            >
              10
            </MyButton>
            <MyButton
              classNames="bg-purewhite text-black border-black border-1 min-w-[0px]"
              onClick={() => handleAddPointsDiff(20)}
            >
              20
            </MyButton>
            <MyButton
              classNames="bg-purewhite text-black border-black border-1 min-w-[0px]"
              onClick={() => handleAddPointsDiff(30)}
            >
              30
            </MyButton>
            <MyButton
              classNames="bg-purewhite text-black border-black border-1 min-w-[0px]"
              onClick={() => handleAddPointsDiff(40)}
            >
              40
            </MyButton>
            <MyButton
              classNames="bg-purewhite text-black border-black border-1 min-w-[0px]"
              onClick={() => handleAddPointsDiff(50)}
            >
              50
            </MyButton>
          </div>
          <div className="grid w-full grid-cols-5 gap-3">
            <MyButton
              classNames="bg-purewhite text-black border-black border-1 min-w-[0px]"
              onClick={() => handleAddPointsDiff(100)}
            >
              100
            </MyButton>
            <MyButton
              classNames="bg-purewhite text-black border-black border-1 min-w-[0px]"
              onClick={() => handleAddPointsDiff(200)}
            >
              200
            </MyButton>
            <MyButton
              classNames="bg-purewhite text-black border-black border-1 min-w-[0px]"
              onClick={() => handleAddPointsDiff(300)}
            >
              300
            </MyButton>
            <MyButton
              classNames="bg-purewhite text-black border-black border-1 min-w-[0px]"
              onClick={() => handleAddPointsDiff(400)}
            >
              400
            </MyButton>
            <MyButton
              classNames="bg-purewhite text-black border-black border-1 min-w-[0px]"
              onClick={() => handleAddPointsDiff(500)}
            >
              500
            </MyButton>
          </div>
        </div>
      )}

      <div className="fbc">
        <MyButton classNames="bg-color1 h-[60px]" onClick={handleCancelEdit}>
          <ImCross size={30} /> Annuler
        </MyButton>
        <MyButton
          disabled={pointsDiff === 0}
          classNames="bg-color4 h-[60px]"
          onClick={() => handleSaveEdit(currentEntity.id, newScore)}
        >
          <FaSave size={30} /> Enregistrer
        </MyButton>
      </div>
    </div>
  )
}
