import { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'

import { Switch, Select, SelectItem } from '@nextui-org/react'
import { customAlert } from '@ui/customAlert'
// import MyNumberInput from '@ui/MyNumberInput'
import MyButton from '@ui/MyButton'

import EntityItem from '@tools/EntityItem'
import EntityItemEdit from '@tools/EntityItemEdit'

import { TbSortDescending } from 'react-icons/tb'
import { IoSettingsOutline } from 'react-icons/io5'
import { MdOutlineChangeCircle } from 'react-icons/md'

import { LiaSortAlphaDownSolid } from 'react-icons/lia'
import { LiaSortAlphaDownAltSolid } from 'react-icons/lia'
import { GiPerspectiveDiceSixFacesRandom } from 'react-icons/gi'

import { LiaSortAmountDownSolid } from 'react-icons/lia'
import { LiaSortAmountUpAltSolid } from 'react-icons/lia'

import { getToolByName } from '@utils/games'

export default function Scoring() {
  const navigate = useNavigate()

  // Routing
  const [currentScreen, setCurrentScreen] = useState('list')
  const [currentEntity, setCurrentEntity] = useState(null)

  // Settings / Options
  const [displayGroupSelect, setDisplayGroupSelect] = useState(false)
  const [displayGroupOrder, setDisplayGroupOrder] = useState(false)
  const [displayGroupOptions, setDisplayGroupOptions] = useState(false)

  // Groups
  const [groups, setGroups] = useState(() => {
    const savedGroups = localStorage.getItem('groups')
    return savedGroups ? JSON.parse(savedGroups) : []
  })

  useEffect(() => {
    console.log(groups)
  }, [groups])
  const [currentGroup, setCurrentGroup] = useState(null)

  // Assurer qu'il y a toujours un groupe actuel si la liste n'est pas vide
  useEffect(() => {
    let activeGroup = groups.find((group) => group.isCurrent)
    if (!activeGroup && groups.length > 0) {
      activeGroup = { ...groups[0], isCurrent: true } // Marquer le premier groupe comme actuel
      const updatedGroups = groups.map((group, index) => ({
        ...group,
        isCurrent: index === 0,
      }))
      setGroups(updatedGroups)
      localStorage.setItem('groups', JSON.stringify(updatedGroups))
    }
    setCurrentGroup(activeGroup)
  }, [groups])

  const handleGroupChange = (selectedGroupId) => {
    const updatedGroups = groups.map((group) => ({
      ...group,
      isCurrent: group.id.toString() === selectedGroupId,
    }))
    setGroups(updatedGroups)
    localStorage.setItem('groups', JSON.stringify(updatedGroups))
    const newCurrentGroup = updatedGroups.find((group) => group.isCurrent)
    setCurrentGroup(newCurrentGroup)
  }

  // Edit Entity mode
  const handleEntityClick = (entityId) => {
    setCurrentScreen('entity')
    const foundEntity = currentGroup.entities.find((entity) => entity.id === entityId)

    if (foundEntity) {
      setCurrentEntity(foundEntity)
    } else {
      console.log('Aucune entité trouvée avec cet ID:', entityId)
      // Gérer le cas où aucune entité n'est trouvée
    }
  }

  const handleCancelEdit = () => {
    setCurrentEntity(null)
    setCurrentScreen('list')
  }

  const handleSaveEdit = (entityId, newScore) => {
    const updatedGroups = groups.map((group) => {
      if (group.id === currentGroup.id) {
        const updatedEntities = group.entities.map((entity) => {
          if (entity.id === entityId) {
            return { ...entity, score: newScore, wasLastEdited: true }
          } else {
            return { ...entity, wasLastEdited: false }
          }
        })
        return { ...group, entities: updatedEntities }
      }
      return group
    })

    setGroups(updatedGroups)
    localStorage.setItem('groups', JSON.stringify(updatedGroups))
    setCurrentEntity(null)
    setCurrentScreen('list')
  }

  // Sorting Functions
  const sortEntitiesByNameAsc = () => {
    const updatedGroups = groups.map((group) => {
      if (group.id === currentGroup.id) {
        const sortedEntities = [...group.entities].sort((a, b) => a.name.localeCompare(b.name))
        return { ...group, entities: sortedEntities }
      }
      return group
    })
    setGroups(updatedGroups)
  }

  const sortEntitiesByNameDesc = () => {
    const updatedGroups = groups.map((group) => {
      if (group.id === currentGroup.id) {
        const sortedEntities = [...group.entities].sort((a, b) => b.name.localeCompare(a.name))
        return { ...group, entities: sortedEntities }
      }
      return group
    })
    setGroups(updatedGroups)
  }

  const sortEntitiesByScoreAsc = () => {
    const updatedGroups = groups.map((group) => {
      if (group.id === currentGroup.id) {
        const sortedEntities = [...group.entities].sort((a, b) => a.score - b.score)
        return { ...group, entities: sortedEntities }
      }
      return group
    })
    setGroups(updatedGroups)
  }

  const sortEntitiesByScoreDesc = () => {
    const updatedGroups = groups.map((group) => {
      if (group.id === currentGroup.id) {
        const sortedEntities = [...group.entities].sort((a, b) => b.score - a.score)
        return { ...group, entities: sortedEntities }
      }
      return group
    })
    setGroups(updatedGroups)
  }

  const shuffleEntities = () => {
    const updatedGroups = groups.map((group) => {
      if (group.id === currentGroup.id) {
        const shuffledEntities = [...group.entities].sort(() => Math.random() - 0.5)
        return { ...group, entities: shuffledEntities }
      }
      return group
    })
    setGroups(updatedGroups)
  }

  const confirmResetGroupScores = (index) => {
    customAlert('Êtes-vous sûr de vouloir réinitialiser les scores ?', () => resetGroupScores(index))
  }

  const resetGroupScores = () => {
    const updatedGroups = groups.map((group) => {
      if (group.id === currentGroup.id) {
        const resetEntities = group.entities.map((entity) => ({
          ...entity,
          score: 0,
        }))
        return { ...group, entities: resetEntities }
      }
      return group
    })
    setGroups(updatedGroups)
  }

  const handleGroupPointsGoDown = () => {
    const updatedGroups = groups.map((group) => {
      if (group.id === currentGroup.id) {
        return { ...group, pointsGoDown: !currentGroup.pointsGoDown }
      }
      return group
    })
    setGroups(updatedGroups)
  }

  const handleKeyboardChange = (k) => {
    const updatedGroups = groups.map((group) => {
      if (group.id === currentGroup.id) {
        return { ...group, keyboard: k }
      }
      return group
    })
    setGroups(updatedGroups)
  }

  return (
    <div className="fccc w-full gap-3">
      {!currentGroup && (
        <div className="fccc gap-3">
          <h3>Vous n'avez pas encore de groupe de jeu</h3>
          <MyButton
            classNames="bg-color4 min-w-[0px]"
            onClick={() => navigate(`/tools/${getToolByName('groups-manager')}`)}
          >
            Créer un groupe
          </MyButton>
        </div>
      )}
      {currentScreen === 'list' && currentGroup && (
        <div className="fccc w-full gap-3">
          <div className="fbc w-full">
            <div className="flex flex-col text-left">
              <h3>{currentGroup.name}</h3>
              <p>{currentGroup.description}</p>
            </div>

            <div className="fcc gap-3">
              <MyButton classNames="bg-color6 min-w-[0px]" onClick={() => setDisplayGroupSelect(!displayGroupSelect)}>
                <MdOutlineChangeCircle size={25} />
              </MyButton>
              <MyButton classNames="bg-color8 min-w-[0px]" onClick={() => setDisplayGroupOrder(!displayGroupOrder)}>
                <TbSortDescending size={25} />
              </MyButton>
              <MyButton classNames="bg-color2 min-w-[0px]" onClick={() => setDisplayGroupOptions(!displayGroupOptions)}>
                <IoSettingsOutline size={25} />
              </MyButton>
            </div>
          </div>

          {displayGroupSelect && (
            <div className="fcc w-full gap-3">
              <Select
                label="Changer de groupe"
                className=""
                defaultSelectedKeys={[`${currentGroup.id}`]}
                onChange={(e) => handleGroupChange(e.target.value)}
              >
                {groups.map((group) => (
                  <SelectItem key={group.id} value={group.id}>
                    {group.name}
                  </SelectItem>
                ))}
              </Select>

              <MyButton
                classNames="bg-color4 min-w-[0px]"
                onClick={() => navigate(`/tools/${getToolByName('groups-manager')}`)}
              >
                <IoSettingsOutline size={25} /> Groupes
              </MyButton>
            </div>
          )}

          {displayGroupOrder && (
            <div className="fbc gap-3 rounded-md border-1 border-gray p-3">
              <div className="fcc gap-3">
                <MyButton classNames="bg-color8 min-w-[0px] md:px-3 px-1" onClick={sortEntitiesByNameAsc}>
                  <LiaSortAlphaDownSolid size={25} />
                </MyButton>
                <MyButton classNames="bg-color8 min-w-[0px] md:px-3 px-1" onClick={sortEntitiesByNameDesc}>
                  <LiaSortAlphaDownAltSolid size={25} />
                </MyButton>
              </div>

              <div className="fccc">
                <MyButton classNames="bg-color8 min-w-[0px] md:px-3 px-1" onClick={shuffleEntities}>
                  <GiPerspectiveDiceSixFacesRandom size={25} />
                </MyButton>
              </div>

              <div className="fcc gap-3">
                <MyButton classNames="bg-color8 min-w-[0px] md:px-3 px-1" onClick={sortEntitiesByScoreAsc}>
                  <LiaSortAmountUpAltSolid size={25} />
                </MyButton>
                <MyButton classNames="bg-color8 min-w-[0px] md:px-3 px-1" onClick={sortEntitiesByScoreDesc}>
                  <LiaSortAmountDownSolid size={25} />
                </MyButton>
              </div>
            </div>
          )}

          {displayGroupOptions && (
            <div className="fccc w-full gap-3 rounded-md border-1 border-gray p-3">
              <Select
                label="Type de clavier"
                className=""
                defaultSelectedKeys={[`${currentGroup.keyboard || null}`]}
                onChange={(e) => handleKeyboardChange(e.target.value)}
              >
                <SelectItem key="default" value="default">
                  Défaut : 0 à 9
                </SelectItem>
                <SelectItem key="steps" value="steps">
                  Ajout auto : unité, dizaine, centaine
                </SelectItem>
              </Select>
              <Switch isSelected={currentGroup.pointsGoDown} onValueChange={handleGroupPointsGoDown}>
                Mode scores dégressif
              </Switch>
              <MyButton classNames="bg-color1 min-w-[0px]" onClick={confirmResetGroupScores}>
                Remettre les scores à 0
              </MyButton>
            </div>
          )}

          <div className="fccc w-full gap-3">
            {currentGroup?.entities?.map((entity) => (
              <EntityItem
                key={entity.id}
                entity={entity}
                isInGame={true}
                handleEntityClick={handleEntityClick}
                isTeam={entity.isTeam}
              />
            ))}
          </div>
        </div>
      )}
      {currentScreen === 'entity' && currentEntity && (
        <EntityItemEdit
          currentEntity={currentEntity}
          handleCancelEdit={handleCancelEdit}
          handleSaveEdit={handleSaveEdit}
          pointsGoDown={currentGroup.pointsGoDown || false}
          keyboard={currentGroup.keyboard || 'default'}
        />
      )}
    </div>
  )
}
