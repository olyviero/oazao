import { useState } from 'react'

import MyNumberInput from '@ui/MyNumberInput'
import MyButton from '@ui/MyButton'
import Spacer from '@ui/Spacer'
import NumKeyboard from '@ui/NumKeyboard'

import TTMCQuestion from '@tools/TTMCQuestion'

import { MdOutlineArrowBackIos } from 'react-icons/md'

import { ttmcQuestions } from '@datas/ttmc'
import { getWordByCardID } from '@utils/datasHelpers'
import { ImCross } from 'react-icons/im'

export default function TTMC() {
  const [cardNumber, setCardNumber] = useState(0)
  const [cardIsSelected, setCardIsSelected] = useState(false)
  const [difficultySelected, setDifficultySelected] = useState(false)
  const [question, setQuestion] = useState(null)

  const handleNewCard = () => {
    setCardNumber(null)
    setCardIsSelected(false)
    setDifficultySelected(null)
  }

  const handleConcatPoints = (value) => {
    if (Number(`${cardNumber}${value}`) < 160) setCardNumber(Number(`${cardNumber}${value}`))
  }

  const handleDifficultySelected = (difficulty) => {
    setDifficultySelected(difficulty)

    const card = ttmcQuestions.find((card) => card.cardNumber === cardNumber)
    if (!card) {
      console.error(`Card with number ${cardNumber} not found`)
      return
    }

    const questionFound = card.questions.find((q) => q.level === difficulty)
    if (!questionFound) {
      console.error(`Question with difficulty ${difficulty} not found in card number ${cardNumber}`)
      return
    }

    setQuestion(questionFound)
  }

  return (
    <div className="fccc gap-3">
      {!cardIsSelected && !difficultySelected && (
        <>
          <h2 className="text-center">N° carte</h2>
          <MyNumberInput
            variant="bordered"
            value={cardNumber}
            color="primary"
            min={0}
            max={159}
            noBtns
            fontSize="50px"
            type="number"
            width="300px"
            callback={(e) => setCardNumber(Number(e.target.value))}
            required={true}
          />

          <NumKeyboard callback={handleConcatPoints} noMultipleZeros={true} />

          <div className="fbc gap-3">
            <MyButton classNames="bg-color2 min-w-[150px]" onClick={() => setCardNumber(0)}>
              <ImCross size={20} />
            </MyButton>
            <MyButton
              disabled={cardNumber === null}
              classNames="bg-color4 min-w-[150px]"
              onClick={() => setCardIsSelected(true)}
            >
              GO
            </MyButton>
          </div>
        </>
      )}

      {cardIsSelected && !difficultySelected && (
        <>
          <div className="fbc">
            <MyButton classNames="bg-color2" onClick={() => setCardIsSelected(false)}>
              <MdOutlineArrowBackIos /> Retour
            </MyButton>
            <p className="text-center font-bold">Carte n°{cardNumber}</p>
          </div>

          <h2 className="text-center">
            Tu te mets combien
            <br />
            en <span className="text-color4">{getWordByCardID(cardNumber) || ''}</span> ?
          </h2>

          <div className="fcc w-full flex-wrap gap-3">
            {Array.from({ length: 10 }).map((_, key) => (
              <div className="cursor-pointer" key={key}>
                <MyButton
                  classNames={`bg-transparent text-3xl w-full text-black border-1 `}
                  onClick={() => handleDifficultySelected(key + 1)}
                >
                  {key + 1}
                </MyButton>
              </div>
            ))}
          </div>
        </>
      )}

      {cardIsSelected && difficultySelected && question && (
        <>
          <div className="fbc">
            <MyButton classNames="bg-color2" onClick={() => setDifficultySelected(null)}>
              <MdOutlineArrowBackIos /> Retour
            </MyButton>
            <p className="text-center font-bold">Carte n°{cardNumber}</p>
          </div>

          <div className="flex flex-col gap-3">
            <TTMCQuestion difficulty={difficultySelected} question={question} color={4} />
          </div>

          <Spacer size={1} />

          <MyButton classNames="bg-color4" onClick={() => handleNewCard(null)}>
            <MdOutlineArrowBackIos /> Nouvelle carte
          </MyButton>
        </>
      )}
    </div>
  )
}
