import { useState, useEffect } from 'react'
import { Input } from '@nextui-org/react'
import { shuffleArray } from '@utils/maths'
import { customAlert } from '@ui/customAlert'
import { v4 } from 'uuid'

import { IoMdOptions } from 'react-icons/io'

import MyNumberInput from '@ui/MyNumberInput'
import MyButton from '@ui/MyButton'
import Spacer from '@ui/Spacer'

import { Modal, ModalContent, ModalHeader, ModalBody, ModalFooter, useDisclosure } from '@nextui-org/react'
import { modalMotionProps, modalClassNames } from '@utils/uiConfigs'

import { GiPerspectiveDiceSixFacesRandom } from 'react-icons/gi'

export default function GridGenerator() {
  const { isOpen, onOpen, onOpenChange, onClose } = useDisclosure()
  const colorClasses = ['black', 'red', 'orange', 'yellow', 'green', 'turquoise', 'blue', 'purple', 'pink', 'white']

  const [gridConfig, setGridConfig] = useState(() => {
    const savedConfig = localStorage.getItem('gridGenerator_config')
    return savedConfig
      ? JSON.parse(savedConfig)
      : {
          grid: [],
          gridUid: v4(),
          rowSize: 5,
          colors: Array(10).fill(1),
          colorVisibility: Array(10).fill(true),
          minRandom: 0,
          maxRandom: 10,
        }
  })

  useEffect(() => {
    localStorage.setItem('gridGenerator_config', JSON.stringify(gridConfig))
  }, [gridConfig])

  const generateGrid = () => {
    let tempGrid = []
    gridConfig.colors.forEach((count, index) => {
      tempGrid = [...tempGrid, ...Array(count).fill(index)] // stocke l'indice de la couleur, pas le nom
    })
    tempGrid = shuffleArray(tempGrid)
    setGridConfig({ ...gridConfig, grid: tempGrid, gridUid: v4() })
  }

  const resetSettings = () => {
    localStorage.removeItem('gridGenerator_config')
    const defaultConfig = {
      grid: [],
      gridUid: v4(),
      rowSize: 5,
      colors: [1, 8, 0, 0, 0, 0, 9, 0, 0, 7],
      colorVisibility: Array(10).fill(true),
      minRandom: 0,
      maxRandom: 10,
    }
    setGridConfig(defaultConfig)
  }

  const handleColorChange = (index, value) => {
    const newColors = [...gridConfig.colors]
    newColors[index] = parseInt(value, 10)
    setGridConfig({ ...gridConfig, colors: newColors })
  }

  const toggleColorVisibility = (index) => {
    const newVisibility = [...gridConfig.colorVisibility]
    newVisibility[index] = !newVisibility[index]
    setGridConfig({ ...gridConfig, colorVisibility: newVisibility })
  }

  const setAllColors = (value) => {
    const newColors = Array(10).fill(value)
    setGridConfig({ ...gridConfig, colors: newColors })
  }

  const setRandomColors = () => {
    const newColors = gridConfig.colors.map(
      () => Math.floor(Math.random() * (gridConfig.maxRandom - gridConfig.minRandom + 1)) + gridConfig.minRandom
    )
    setGridConfig({ ...gridConfig, colors: newColors })
  }

  const updateMinRandom = (value) => {
    setGridConfig({ ...gridConfig, minRandom: parseInt(value, 10) })
  }

  const updateMaxRandom = (value) => {
    setGridConfig({ ...gridConfig, maxRandom: parseInt(value, 10) })
  }

  const updateRowSize = (value) => {
    setGridConfig({ ...gridConfig, rowSize: parseInt(value, 10) })
  }

  const openModal = () => {
    onOpen()
  }
  return (
    <>
      <div className="flex w-full flex-col text-center">
        <div className="flex justify-center gap-3">
          <MyButton classNames="bg-color8 min-w-0" onClick={openModal}>
            <IoMdOptions size={20} />
          </MyButton>
          <MyButton onClick={generateGrid}>Générer</MyButton>
          <MyButton classNames="bg-color1" onClick={resetSettings}>
            Réinitialiser
          </MyButton>
        </div>

        <Spacer size={1} />

        <p className="text-tiny">{gridConfig.gridUid}</p>

        <Spacer size={1} />

        <div className={`grid gap-1`} style={{ gridTemplateColumns: `repeat(${gridConfig.rowSize}, minmax(0, 1fr))` }}>
          {gridConfig.grid.map((colorIndex, index) => (
            <div
              key={index}
              className={`flex border-1 border-primary justify-center items-center grid-item h-[50px] ${
                gridConfig.colorVisibility[colorIndex] ? `bg-color${colorIndex}` : 'bg-secondary/50'
              }`}
            ></div>
          ))}
        </div>
      </div>

      <Modal
        backdrop="opaque"
        isOpen={isOpen}
        onOpenChange={onOpenChange}
        scrollBehavior="outside"
        motionProps={modalMotionProps}
        classNames={modalClassNames}
      >
        <ModalContent>
          {(onClose) => (
            <>
              <ModalHeader className="flex w-full flex-col items-center justify-center gap-1">
                Filtres de jeux
              </ModalHeader>
              <ModalBody>
                <div className="flex flex-col gap-3">
                  <div className="flex flex-wrap justify-center gap-3">
                    {gridConfig.colors.map((color, index) => (
                      <Input
                        key={index}
                        classNames={{
                          base: `w-16 h-[50px]'}`,
                          inputWrapper: `border-2 border-color${index}`,
                          input: 'text-center',
                        }}
                        variant="bordered"
                        value={color}
                        min={0}
                        noBtns
                        fontSize="28px"
                        type="number"
                        onChange={(e) => handleColorChange(index, e.target.value)}
                      />
                    ))}
                  </div>
                </div>

                <Spacer size={1} />

                <div className="flex flex-col gap-3 py-3">
                  <div className="mx-auto flex w-full max-w-md justify-center gap-3">
                    <MyNumberInput
                      variant="bordered"
                      value={gridConfig.rowSize}
                      color="primary"
                      btnClassNames="!bg-black"
                      min={1}
                      max={50}
                      type="number"
                      callback={(e) => updateRowSize(e.target.value)}
                      label="Nombre de cases par lignes"
                    />
                  </div>

                  <h3>Visibilité des couleur</h3>
                  <div className="flex flex-wrap justify-center gap-3">
                    {gridConfig.colors.map((color, index) => (
                      <div key={index} className="flex items-center gap-2">
                        <button
                          onClick={() => toggleColorVisibility(index)}
                          className={`h-12 w-12 rounded-full border-1 ${
                            gridConfig.colorVisibility[index] ? 'border-primary' : 'border-color1 opacity-30'
                          } ${`bg-color${index}`}`}
                        ></button>
                      </div>
                    ))}
                  </div>

                  <h3>Préremplir</h3>
                  <div className="flex flex-wrap justify-center gap-3">
                    {Array.from({ length: 11 }, (_, i) => (
                      <MyButton
                        key={i}
                        classNames="bg-black font-bold text-2xl min-w-1 w-[40px]"
                        onClick={() => setAllColors(i)}
                      >
                        {`${i}`}
                      </MyButton>
                    ))}
                  </div>

                  <h3>Aléatoire</h3>
                  <div className="flex justify-center gap-3">
                    <Input
                      classNames={{
                        base: `w-16 h-[50px]'}`,
                        inputWrapper: `border-2 border-gray`,
                        input: 'text-center',
                      }}
                      label="min"
                      variant="bordered"
                      value={gridConfig.minRandom}
                      min={0}
                      noBtns
                      fontSize="28px"
                      type="number"
                      onChange={(e) => updateMinRandom(parseInt(e.target.value))}
                    />
                    <Input
                      classNames={{
                        base: `w-16 h-[50px]'}`,
                        inputWrapper: `border-2 border-gray`,
                        input: 'text-center',
                      }}
                      label="max"
                      variant="bordered"
                      value={gridConfig.maxRandom}
                      min={0}
                      noBtns
                      fontSize="28px"
                      type="number"
                      onChange={(e) => updateMaxRandom(parseInt(e.target.value))}
                    />
                    <MyButton classNames="bg-black font-bold text-2xl" onClick={setRandomColors}>
                      <GiPerspectiveDiceSixFacesRandom />
                    </MyButton>
                  </div>
                </div>
              </ModalBody>
              <ModalFooter className="flex w-full justify-between">
                <MyButton classNames="bg-color1" onClick={onClose}>
                  Fermer
                </MyButton>
              </ModalFooter>
            </>
          )}
        </ModalContent>
      </Modal>
    </>
  )
}
