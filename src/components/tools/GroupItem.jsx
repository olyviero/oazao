import { useNavigate } from 'react-router-dom'
import { customAlert } from '@ui/customAlert'
import MyButton from '@ui/MyButton'

import { getToolByName } from '@utils/games'

import { ImCross } from 'react-icons/im'
import { IoPerson } from 'react-icons/io5'
import { RiTeamFill } from 'react-icons/ri'

export default function GroupItem({ group, handleDeleteGroup }) {
  const navigate = useNavigate()

  const confirmDeleteEntity = () => {
    customAlert(`Êtes-vous sûr de vouloir supprimer ce groupe ?`, () => handleDeleteGroup(group?.id))
  }

  return (
    <div
      className="fbc cursor-pointer gap-3"
      onClick={() => navigate(`/tools/${getToolByName('groups-manager')}/group/${group.id}`)}
    >
      <MyButton classNames="p-1 bg-transparent min-w-[Opx] text-color1" onClick={confirmDeleteEntity}>
        <ImCross size={30} />
      </MyButton>

      <div className="fbc rounded-md border-1 p-3">
        <div className="fcc gap-3">
          <div className="">{group.name}</div>
        </div>
        <div className="fcc gap-3">
          {group.isTeam ? <RiTeamFill size={30} /> : <IoPerson size={30} />} {group.entities.length}
        </div>
      </div>
    </div>
  )
}
