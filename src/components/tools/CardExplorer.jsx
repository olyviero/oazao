import { Splide, SplideSlide, SplideTrack } from '@splidejs/react-splide'
import '@splidejs/react-splide/css'

import Spacer from '@ui/Spacer'
import { useState } from 'react'

import { Tabs, Tab, Card, CardBody, Accordion, AccordionItem } from '@nextui-org/react'

import { getChemicalByCardID, getGeographyByCardID, getPokemonByCardID, getArtistByCardID } from '@utils/datasHelpers'
import { addLeadingZeroUnder10 } from '@utils/formaters'

export default function CardExplorer() {
  const [currentCardId, setCurrentCardId] = useState(0)

  const cardsUrls = () => {
    const urls = []
    for (let x = 0; x <= 15; x++) {
      for (let y = 0; y <= 9; y++) {
        urls.push(`/imgs/cards-front-border/${x}.${y}.jpg`)
      }
    }
    return urls
  }

  const handleSlideChange = (splide, prev, next) => {
    setCurrentCardId(parseInt(splide.index, 10))
  }
  return (
    <>
      <Splide
        options={{
          type: 'loop',
          height: '500px',
          arrows: false,
          interval: 5000,
          perPage: 3,
          gap: 20,
          lazyLoad: 'nearby',
          focus: 'center',
          breakpoints: {
            768: {
              perPage: 1,
            },
          },
        }}
        hasTrack={false}
        aria-label="Oazao Images"
        onMounted={handleSlideChange}
        onMoved={handleSlideChange}
      >
        <SplideTrack>
          {cardsUrls().map((url, index) => (
            <SplideSlide key={index} data-x={2} data-y={2}>
              <img data-splide-lazy={url} className="mx-auto rounded-lg border-1 border-color0/30 text-center shadow-xl" data-bob="jackie" />
            </SplideSlide>
          ))}
        </SplideTrack>
      </Splide>

      <Spacer size={2} />

      <div className="mx-auto mb-12 flex w-full flex-col gap-3">
        {currentCardId !== null && (
          <div>
            <h3 className="text-6xl">{addLeadingZeroUnder10(currentCardId)}</h3>

            <Spacer size={1} />

            <Accordion variant="splitted">
              <AccordionItem aria-label="chemistry" title="Chimie">
                {currentCardId >= 0 && currentCardId <= 118 && (
                  <div className="mx-auto flex max-w-[800px] flex-col border-1 border-black/30 p-3">
                    <div className="flex justify-between">
                      <div className="label font-bold">Element chimique</div>
                      <div className="value">{getChemicalByCardID(currentCardId).chName}</div>
                    </div>

                    <div className="flex justify-between">
                      <div className="label font-bold">Symbol</div>
                      <div className="value">{getChemicalByCardID(currentCardId).chSymbol}</div>
                    </div>
                  </div>
                )}
              </AccordionItem>

              <AccordionItem aria-label="geography" title="Géographie">
                {currentCardId >= 1 && currentCardId <= 95 && (
                  <div className="mx-auto flex max-w-[800px] flex-col border-1 border-black/30 p-3">
                    <div className="flex justify-between">
                      <div className="label font-bold">Département</div>
                      <div className="value">{getGeographyByCardID(currentCardId).name}</div>
                    </div>

                    <div className="flex justify-between">
                      <div className="label font-bold">Chef Lieu</div>
                      <div className="value">{getGeographyByCardID(currentCardId).capital}</div>
                    </div>

                    <div className="flex justify-between">
                      <div className="label font-bold">Nombre d'arrondissement</div>
                      <div className="value">{getGeographyByCardID(currentCardId).nbDistrict}</div>
                    </div>

                    <div className="flex justify-between">
                      <div className="label font-bold">Noms des arrondisements</div>
                      <div className="value">{getGeographyByCardID(currentCardId).districtNames}</div>
                    </div>

                    <div className="flex justify-between">
                      <div className="label font-bold">Nombre de communes</div>
                      <div className="value">{getGeographyByCardID(currentCardId).nbCommune}</div>
                    </div>

                    <div className="flex justify-between">
                      <div className="label font-bold">Nombre de cantons</div>
                      <div className="value">{getGeographyByCardID(currentCardId).nbCounty}</div>
                    </div>

                    <div className="flex justify-between">
                      <div className="label font-bold">Nombre de cantons</div>
                      <div className="value">{getGeographyByCardID(currentCardId).nbCounty}</div>
                    </div>

                    <div className="flex justify-between">
                      <div className="label font-bold">Région administrative</div>
                      <div className="value">{getGeographyByCardID(currentCardId).adminRegion}</div>
                    </div>

                    <div className="flex justify-between">
                      <div className="label font-bold">Superficie cadastrale (km2)</div>
                      <div className="value">{getGeographyByCardID(currentCardId).surface}</div>
                    </div>

                    <div className="flex justify-between">
                      <div className="label font-bold">Population</div>
                      <div className="value">{getGeographyByCardID(currentCardId).population}</div>
                    </div>

                    <div className="flex justify-between">
                      <div className="label font-bold">Densité (hab/km2)</div>
                      <div className="value">{getGeographyByCardID(currentCardId).density}</div>
                    </div>
                  </div>
                )}
              </AccordionItem>

              <AccordionItem aria-label="pokemons" title="Pokemons (1ère génération)">
                {currentCardId >= 1 && currentCardId <= 151 && (
                  <div className="mx-auto flex max-w-[800px] flex-col border-1 border-black/30 p-3">
                    <div className="flex justify-between">
                      <div className="label font-bold">Nom</div>
                      <div className="value">{getPokemonByCardID(currentCardId).pokemon}</div>
                    </div>

                    <div className="flex justify-between">
                      <div className="label font-bold">Type</div>
                      <div className="value">{getPokemonByCardID(currentCardId).type}</div>
                    </div>
                  </div>
                )}
              </AccordionItem>

              <AccordionItem aria-label="artists" title="100 meilleurs artistes d'après Rolling Stones">
                {currentCardId >= 1 && currentCardId <= 100 && (
                  <div className="mx-auto flex max-w-[800px] flex-col border-1 border-black/30 p-3">
                    <div className="flex justify-between">
                      <div className="label font-bold">Nom</div>
                      <div className="value">{getArtistByCardID(currentCardId).artist}</div>
                    </div>
                  </div>
                )}
              </AccordionItem>
            </Accordion>
          </div>
        )}
      </div>
    </>
  )
}
