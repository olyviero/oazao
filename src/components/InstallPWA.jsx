import { useEffect, useState } from 'react'
import { MdInstallMobile } from 'react-icons/md'

const InstallPWA = () => {
  const [supportsPWA, setSupportsPWA] = useState(false)
  const [promptInstall, setPromptInstall] = useState(null)

  useEffect(() => {
    const handler = (e) => {
      e.preventDefault()
      console.log('we are being triggered :D')
      setSupportsPWA(true)
      setPromptInstall(e)
    }
    window.addEventListener('beforeinstallprompt', handler)

    return () => window.removeEventListener('transitionend', handler)
  }, [])

  const onClick = (evt) => {
    evt.preventDefault()
    // if (!promptInstall) {
    //   return
    // }
    promptInstall.prompt()
  }
  // if (!supportsPWA) {
  //   return null
  // }
  return (
    <button
      className="link-button rounded-md border-1 border-black p-3"
      id="setup_button"
      aria-label="Install app"
      title="Install app"
      onClick={onClick}
    >
      <MdInstallMobile /> Installer l'application
    </button>
  )
}

export default InstallPWA
