import PropTypes from 'prop-types'

export default function Page({ children }) {
  return <div className="mx-auto mt-4 flex w-full max-w-screen-xl flex-col gap-3 px-3 xl:px-0">{children}</div>
}

Page.propTypes = {
  children: PropTypes.node,
}
