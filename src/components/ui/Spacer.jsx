export default function Spacer({ size }) {
  return <div style={{ padding: `${size}rem` }}></div>
}
