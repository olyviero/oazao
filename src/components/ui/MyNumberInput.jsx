import { Input } from '@nextui-org/react'
import { useState, useEffect } from 'react'

import MyButton from '@/components/ui/MyButton'

export default function MyNumberInput({
  variant,
  value,
  type,
  color,
  callback,
  label,
  required,
  min,
  max,
  noBtns,
  fontSize,
  width,
  btnClassNames,
}) {
  const [inputValue, setInputValue] = useState(value)

  useEffect(() => {
    setInputValue(value)
  }, [value])

  const handleDecrease = () => {
    if (inputValue > min) {
      const newValue = parseInt(inputValue, 10) - 1
      setInputValue(newValue)
      callback({ target: { value: newValue } })
    }
  }

  const handleIncrease = () => {
    if (inputValue < max) {
      const newValue = parseInt(inputValue, 10) + 1
      setInputValue(newValue)
      callback({ target: { value: newValue } })
    }
  }

  const handleChange = (e) => {
    const newValue = parseInt(e.target.value, 10)
    if (!isNaN(newValue) && newValue >= min && newValue <= max) {
      setInputValue(newValue)
      callback(e)
    }
  }

  return (
    <div className="flex items-center justify-between gap-3">
      {label && <p className="text-center font-bold">{label}</p>}
      {!noBtns && (
        <MyButton
          classNames={`${btnClassNames} bg-color4 font-bold text-2xl h-[50px] min-w-1 w-[40px]`}
          onClick={handleDecrease}
        >
          -
        </MyButton>
      )}

      <Input
        variant={variant}
        type={type || 'text'}
        color={color || ''}
        value={inputValue || 0}
        min={min || null}
        max={max || null}
        onChange={handleChange}
        {...(required ? { required } : null)}
        classNames={{
          base: `mx-auto !overflow-hidden ${
            width ? `w-[${width}]` : 'max-w-[80px]'
          }`,
          inputWrapper:
            'focus-within:!border-color4 focus-within:border-2 min-h-[50px] h-auto',
          input: `${
            fontSize ? `text-[${fontSize}]` : 'text-[3xl]'
          } text-center`,
        }}
      />
      {!noBtns && (
        <MyButton
          classNames={`${btnClassNames} bg-color4 font-bold h-[50px] text-2xl min-w-1 w-[40px]`}
          onClick={handleIncrease}
        >
          +
        </MyButton>
      )}
    </div>
  )
}
