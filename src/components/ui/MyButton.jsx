import { Button } from '@nextui-org/react'

function MyButon({ children, color, onClick, classNames, fullWidth, disabled }) {
  return (
    <Button
      // disabled={isSigningIn}
      type="submit"
      onClick={onClick}
      color={color}
      {...(disabled && { disabled: true })}
      className={`${disabled && 'opacity-50'} font-medium text-purewhite ${!color && 'bg-color4'} ${fullWidth ? 'w-full' : 'btn-w-auto'} ${classNames}`}
    >
      {children}
    </Button>
  )
}

export default MyButon
