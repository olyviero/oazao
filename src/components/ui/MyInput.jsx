import { Input } from '@nextui-org/react'

export default function MyInput({ variant, value, type, color, callback, label, required }) {
  const validateEmail = (value) => value.match(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}$/i)

  return (
    <Input
      variant={variant}
      type={type || 'text'}
      color={color || 'primary'}
      value={value}
      label={label}
      onChange={callback}
      {...(required ? { required } : null)}
      classNames={{
        inputWrapper: 'focus-within:!border-color4 focus-within:border-2',
      }}
    />
  )
}
