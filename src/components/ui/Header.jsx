import PropTypes from 'prop-types'
import { useNavigate } from 'react-router-dom'
import { IoIosArrowBack } from 'react-icons/io'

export default function Header({ title, children, noreturn }) {
  const navigate = useNavigate()
  return (
    <div className="mb-3 grid w-full grid-cols-3 p-3">
      {noreturn ? (
        <div></div>
      ) : (
        <IoIosArrowBack className="cursor-pointer text-color0" size={30} onClick={() => navigate(-1)} />
      )}

      <div className="fcc text-center font-bold">{title}</div>
      <div className="fcc justify-self-end">{children || ''}</div>
    </div>
  )
}

Header.propTypes = {
  title: PropTypes.string,
  children: PropTypes.node,
  noreturn: PropTypes.bool,
}
