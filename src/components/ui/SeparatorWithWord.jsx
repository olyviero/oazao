function SeparatorWithWord({ word }) {
  return (
    <div className="flex w-full flex-row text-center">
      <div className="mb-2 mr-2 w-full border-b-1 border-lightgray"></div>
      <div className="w-fit text-sm font-bold uppercase text-lightgray">{word}</div>
      <div className="mb-2 ml-2 w-full border-b-1 border-lightgray"></div>
    </div>
  )
}

export default SeparatorWithWord
