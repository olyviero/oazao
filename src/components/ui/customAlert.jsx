import ModalConfirm from '../ModalConfirm'
import { confirmAlert } from 'react-confirm-alert'
import 'react-confirm-alert/src/react-confirm-alert.css'

export const customAlert = (message, onConfirm, additionalOptions = {}) => {
  const defaultOptions = {
    customUI: ({ onClose }) => <ModalConfirm onClose={onClose} onConfirm={onConfirm} message={message} />,
    closeOnEscape: true,
    closeOnClickOutside: true,
    keyCodeForClose: [8, 32],
    overlayClassName: 'overlay-custom-class-name',
  }

  const options = { ...defaultOptions, ...additionalOptions }
  confirmAlert(options)
}
