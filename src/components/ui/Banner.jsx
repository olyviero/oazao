import PropTypes from 'prop-types'

export default function Banner({ img, height }) {
  return (
    <div
      className="w-full"
      style={{
        backgroundImage: `url(${img})`,
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.2)',
        height: `${height}px`,
      }}
    ></div>
  )
}

Banner.propTypes = {
  img: PropTypes.string.isRequired,
  height: PropTypes.string.isRequired,
}
