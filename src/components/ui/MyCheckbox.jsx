import { useCheckbox, Chip, VisuallyHidden, tv } from '@nextui-org/react'
import { BsCheck } from 'react-icons/bs'

const checkbox = tv({
  slots: {
    base: 'hover:bg-default-200 border-1 border-lightgray',
    content: 'text-[14px]',
  },
  variants: {
    isSelected: {
      true: {
        base: 'border-color0 bg-color4 hover:bg-color4/50 hover:border-color4 border-1',
        content: 'text-primary-foreground',
      },
    },
    isFocusVisible: {
      true: {
        base: 'outline-none ring-2 ring-focus ring-offset-2 ring-offset-background',
      },
    },
  },
})

export const MyCheckbox = (props) => {
  const { children, isSelected, isFocusVisible, getBaseProps, getLabelProps, getInputProps } = useCheckbox({
    ...props,
  })

  const styles = checkbox({ isSelected, isFocusVisible })

  return (
    <label {...getBaseProps()}>
      <VisuallyHidden>
        <input {...getInputProps()} />
      </VisuallyHidden>
      <Chip
        size="sm"
        classNames={{
          base: styles.base(),
          content: styles.content(),
        }}
        color="primary"
        // startContent={isSelected ? <BsCheck /> : null}
        variant="faded"
        {...getLabelProps()}
      >
        {children ? children : isSelected ? 'Enabled' : 'Disabled'}
      </Chip>
    </label>
  )
}
