import { Link } from 'react-router-dom'

import { IoIosArrowForward } from 'react-icons/io'
import { BsFillStarFill } from 'react-icons/bs'

import { FaPeopleGroup } from 'react-icons/fa6'

export default function LinkBox({ txt, txt2, txt3, to }) {
  return (
    <Link to={to} className="w-full rounded-md border-1 border-lightgray p-3">
      <div className="fbc">
        <div className="fcc">
          <div className="text-color0">{txt}</div>
          {txt2 && txt2 !== '' && (
            <div className="fcc ml-3 gap-1 text-color3">
              {txt2}
              <BsFillStarFill />
            </div>
          )}
          {txt3 && txt3 !== '' && (
            <div className="fcc ml-3 gap-1 text-tiny text-color2">
              <FaPeopleGroup /> {txt3}
            </div>
          )}
        </div>
        <IoIosArrowForward size={20} className="text-color0" />
      </div>
    </Link>
  )
}
