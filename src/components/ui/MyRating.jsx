import { Rating } from 'react-simple-star-rating'
import { useState, useEffect } from 'react'

import { BsFillStarFill } from 'react-icons/bs'
import { GiInvertedDice1, GiInvertedDice2, GiInvertedDice3, GiInvertedDice4, GiInvertedDice5 } from 'react-icons/gi'
import { FaFaceSadCry, FaFaceFrown, FaFaceGrimace, FaFaceSmileBeam, FaFaceKissWinkHeart } from 'react-icons/fa6'

export default function MyRating({ callback, iconSet, size, readonly, value }) {
  const [icons, setIcons] = useState([])

  useEffect(() => {
    switch (iconSet) {
      case 'star':
        setIcons([
          { icon: <BsFillStarFill key={0} size={size} /> },
          { icon: <BsFillStarFill key={1} size={size} /> },
          { icon: <BsFillStarFill key={2} size={size} /> },
          { icon: <BsFillStarFill key={3} size={size} /> },
          { icon: <BsFillStarFill key={4} size={size} /> },
        ])
        break
      case 'face':
        setIcons([
          { icon: <FaFaceSadCry key={0} size={size} /> },
          { icon: <FaFaceFrown key={1} size={size} /> },
          { icon: <FaFaceGrimace key={2} size={size} /> },
          { icon: <FaFaceSmileBeam key={3} size={size} /> },
          { icon: <FaFaceKissWinkHeart key={4} size={size} /> },
        ])
        break
      case 'dice':
        setIcons([
          { icon: <GiInvertedDice1 key={0} size={size} /> },
          { icon: <GiInvertedDice2 key={1} size={size} /> },
          { icon: <GiInvertedDice3 key={2} size={size} /> },
          { icon: <GiInvertedDice4 key={3} size={size} /> },
          { icon: <GiInvertedDice5 key={4} size={size} /> },
        ])
        break
      default:
        setIcons([
          { icon: <BsFillStarFill key={0} size={size} /> },
          { icon: <BsFillStarFill key={1} size={size} /> },
          { icon: <BsFillStarFill key={2} size={size} /> },
          { icon: <BsFillStarFill key={3} size={size} /> },
          { icon: <BsFillStarFill key={4} size={size} /> },
        ])
        break
    }
  }, [iconSet, size])

  return (
    <Rating
      SVGstyle={{ display: 'inline' }}
      customIcons={icons}
      onClick={callback}
      size={size}
      initialValue={value || null}
      readonly={readonly}
      fillColorArray={['#f14f45', '#f16c45', '#f18845', '#f1b345', '#f1d045']}
      transition
      allowFraction
      // showTooltip
      // tooltipDefaultText="Mon vote"
      // tooltipArray={[
      //   'Vraiment nul',
      //   'Nul',
      //   'Mauvais',
      //   'Plutôt mauvais+',
      //   'Moyen',
      //   'Moyen +',
      //   'Sympa',
      //   'Cool',
      //   'Génial',
      //   'Merveilleux',
      // ]}
    />
  )
}
