import PropTypes from 'prop-types'

import MyButton from '@ui/MyButton'

export default function NumKeyboard({ callback, noMultipleZeros }) {
  return (
    <div className="fccc w-full gap-3">
      <div className="grid w-full grid-cols-3 gap-3">
        <MyButton classNames="bg-purewhite text-black border-black border-1 min-w-[0px]" onClick={() => callback('1')}>
          1
        </MyButton>
        <MyButton classNames="bg-purewhite text-black border-black border-1 min-w-[0px]" onClick={() => callback('2')}>
          2
        </MyButton>
        <MyButton classNames="bg-purewhite text-black border-black border-1 min-w-[0px]" onClick={() => callback('3')}>
          3
        </MyButton>
      </div>
      <div className="grid w-full grid-cols-3 gap-3">
        <MyButton classNames="bg-purewhite text-black border-black border-1 min-w-[0px]" onClick={() => callback('4')}>
          4
        </MyButton>
        <MyButton classNames="bg-purewhite text-black border-black border-1 min-w-[0px]" onClick={() => callback('5')}>
          5
        </MyButton>
        <MyButton classNames="bg-purewhite text-black border-black border-1 min-w-[0px]" onClick={() => callback('6')}>
          6
        </MyButton>
      </div>
      <div className="grid w-full grid-cols-3 gap-3">
        <MyButton classNames="bg-purewhite text-black border-black border-1 min-w-[0px]" onClick={() => callback('7')}>
          7
        </MyButton>
        <MyButton classNames="bg-purewhite text-black border-black border-1 min-w-[0px]" onClick={() => callback('8')}>
          8
        </MyButton>
        <MyButton classNames="bg-purewhite text-black border-black border-1 min-w-[0px]" onClick={() => callback('9')}>
          9
        </MyButton>
      </div>
      {!noMultipleZeros ? (
        <div className="grid w-full grid-cols-3 gap-3">
          <MyButton
            classNames="bg-purewhite text-black border-black border-1 min-w-[0px]"
            onClick={() => callback('0')}
          >
            0
          </MyButton>
          <MyButton
            classNames="bg-purewhite text-black border-black border-1 min-w-[0px]"
            onClick={() => callback('00')}
          >
            00
          </MyButton>
          <MyButton
            classNames="bg-purewhite text-black border-black border-1 min-w-[0px]"
            onClick={() => callback('000')}
          >
            000
          </MyButton>
        </div>
      ) : (
        <div className="grid w-full grid-cols-3 gap-3">
          <MyButton
            classNames="bg-purewhite text-black border-black border-1 min-w-[0px]"
            onClick={() => callback('0')}
          >
            0
          </MyButton>
        </div>
      )}
    </div>
  )
}

NumKeyboard.propTypes = {
  callback: PropTypes.func.isRequired,
  noMultipleZeros: PropTypes.bool,
}
