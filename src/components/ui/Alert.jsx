export default function Alert({ children, bgColor, txtColor = 'black' }) {
  return <div className={`${bgColor && 'p-3'} bg-color${bgColor}/50 rounded-md text-${txtColor}`}>{children}</div>
}
