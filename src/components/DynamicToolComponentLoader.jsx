import { Suspense, lazy } from 'react'

import { gamesTools } from '@/datas/gamesTools'

function dynamicImportComponent(componentName) {
  return lazy(() => import(`@tools/${componentName}.jsx`))
}

export default function DynamicToolComponentLoader({ toolId }) {
  const tool = gamesTools.find((t) => t.id === toolId)
  if (!tool) {
    return <div>Tool not found!</div>
  }

  const Component = dynamicImportComponent(tool.component)

  return (
    <Suspense fallback={<div>Loading...</div>}>
      <Component />
    </Suspense>
  )
}
