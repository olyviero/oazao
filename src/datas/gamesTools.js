// prettier-ignore
// prettier-ignore-file

// prettier-ignore
/* eslint-disable quotes */

// ----------------------------------------
// Important : ids must never change !!!!!!
// ----------------------------------------
export const gamesTools = [
  {
    id: 0,
    name: 'groups-manager',
    component: 'GroupsManager',
    title: 'Groupes',
    description: 'Crées plusieurs groupes qui auront chacun leur propre compteurs de points.',
  },
  { id: 1, name: 'scoring', component: 'Scoring', title: 'Compteur de points', description: 'Un compteur de points pour chacun de vos groupes.' },
  {
    id: 2,
    name: 'card-explorer',
    component: 'CardExplorer',
    title: 'Explorateur de carte',
    description: 'Découvrez les cartes à leurs pleins potentiels !',
  },
  {
    id: 3,
    name: 'ttmc',
    component: 'TTMC',
    title: 'TTMC',
    description: "L'assistant officiel TTMC pour rechercher toutes les questions et réponses en fonction du numéro de la carte.",
    isUnique: true
  },
  {
    id: 4,
    name: 'grid-generator',
    component: 'GridGenerator',
    title: 'Générateur de grille',
    description: 'Génères des grilles colorées avec de nombreux paramètres.',
  },
]
