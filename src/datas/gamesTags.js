// prettier-ignore
// prettier-ignore-file

// prettier-ignore
/* eslint-disable quotes */

// ----------------------------------------
// Important : ids must never change !!!!!!
// ----------------------------------------
export const gamesTags = [
  {id:0,   name:'chance',       title: 'Hasard', description: "Le déroulement du jeu est dépendant du hasard (dés, cartes mélangées, roues, …)"},
  {id:1,   name:'draft',        title: 'Draft', description: "Les joueurs choisissent un élément avant de faire passer le reste des éléments au joueur à côté de lui, qui répète l'opération jusqu'à épuisement de la ressource."},
  {id:2,   name:'deckbuilding', title: 'Deckbuilding', description: "Les joueurs possèdent un paquet de cartes de départ et le fait évoluer en ajoutant ou supprimant des cartes pour le rendre plus puissant."},
  {id:3,   name:'tactical',     title: 'Tactique', description: "Les différents coups sont exécutés avec des combinaisons de mouvements ou d'actions tous identifiés."},
  {id:4,   name:'strategy',     title: 'Stratégie', description: "Les différents coups sont exécutés sur des groupes ou en générant une suite de combinaisons."},
  {id:5,   name:'majority',     title: 'Majorité', description: "Il faut avoir la majorité de tuiles / cartes / jetons pour effectuer des actions ou pour remporter la partie."},
  {id:6,   name:'agility',      title: 'Adresse', description: "L'habilité des joueurs intervient (pistolet dans Anti-gang, fléchettes dans DartWar…)."},
  {id:7,   name:'memory',       title: 'Mémoire', description: "Nécessite de retenir des informations pour pouvoir avancer dans le jeu."},
  {id:8,   name:'culture',      title: 'Culture', description: "Cible une ou plusieurs thématiques sur lesquelles sont posées des questions."},
  {id:9,   name:'bluff',        title: 'Bluff', description: "Les émotions des joueurs et le comportement agissent sur les actions."},
  {id:10,  name:'bid',          title: 'Enchères', description: "Les joueurs devront faire des offres plus importantes que les autres."},
  {id:11,  name:'trading',      title: 'Échange et commerce', description: "Les joueurs échangent entre eux des cartes ou d'autres objets."},
  {id:12,  name:'reflex',       title: 'Réflexe', description: "Les réflexes et/ou la rapidité des joueurs interviennent."},
  {id:13,  name:'path',         title: 'Parcours', description: "La progression des joueurs suit une piste et le joueur arrivant le plus loin ou qui atteint l'arrivée est le gagnant."},
  {id:14,  name:'reflection',   title: 'Réflexion', description: "Les joueurs doivent utiliser la logique."},
  {id:15,  name:'senses',       title: 'Sens', description: "Une bonne utilisation des sens permettra de gagner (trouver un objet dans un sac, trouver quel plat on goûte…)."},
  {id:16,  name:'stop-or-more', title: 'Stop ou encore', description: "Il faut décider de continuer l'action ou de l'arrêter avant que quelque chose de négatif ne se produise."},
  {id:17,  name:'observation',  title: 'Observation', description: "Les joueurs doivent repérer visuellement des éléments."},
  {id:18,  name:'gages',        title: 'Gages', description: "Les règles imposent des gages à tour de rôle ou au joueur ayant raté une épreuve"},
  {id:19,  name:'coop',         title: 'Coopératif', description: "Un jeu coopératif est un jeu dans lequel tous les joueurs gagnent ou perdent ensemble, en cherchant à comprendre, maîtriser et dépasser les règles 'automatiques' du jeu."},
  {id:20,  name:'party',        title: 'En soirée', description: "Vous savez bien..."},
  {id:21,  name:'18',           title: '18+', description: "A votre avis ?"},
  {id:22,  name:'family',       title: 'En famille', description: "Pour toute la famille"},
  {id:23,  name:'kids',         title: 'Pour enfants', description: "Accessible aux enfants"},
  {id:24,  name:'drink',        title: 'Jeu à boire', description: "Buvez maintenant, de l'eau bien sûr !"},
]
