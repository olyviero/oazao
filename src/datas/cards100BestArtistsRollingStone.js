export const cards100BestArtistsRollingStone = [
  { cardId: 1, artist: 'The Beatles' },
  { cardId: 2, artist: 'Bob Dylan' },
  { cardId: 3, artist: 'Elvis Presley' },
  { cardId: 4, artist: 'The Rolling Stones' },
  { cardId: 5, artist: 'Chuck Berry' },
  { cardId: 6, artist: 'Jimi Hendrix' },
  { cardId: 7, artist: 'James Brown' },
  { cardId: 8, artist: 'Little Richard' },
  { cardId: 9, artist: 'Aretha Franklin' },
  { cardId: 10, artist: 'Ray Charles' },
  { cardId: 11, artist: 'Bob Marley & The Wailers' },
  { cardId: 12, artist: 'The Beach Boys' },
  { cardId: 13, artist: 'Buddy Holly' },
  { cardId: 14, artist: 'Led Zeppelin' },
  { cardId: 15, artist: 'Stevie Wonder' },
  { cardId: 16, artist: 'Sam Cooke' },
  { cardId: 17, artist: 'Muddy Waters' },
  { cardId: 18, artist: 'Marvin Gaye' },
  { cardId: 19, artist: 'The Velvet Underground' },
  { cardId: 20, artist: 'Bo Diddley' },
  { cardId: 21, artist: 'Otis Redding' },
  { cardId: 22, artist: 'U2' },
  { cardId: 23, artist: 'Bruce Springsteen' },
  { cardId: 24, artist: 'Jerry Lee Lewis' },
  { cardId: 25, artist: 'Fats Domino' },
  { cardId: 26, artist: 'Ramones' },
  { cardId: 27, artist: 'Prince' },
  { cardId: 28, artist: 'The Clash' },
  { cardId: 29, artist: 'The Who' },
  { cardId: 30, artist: 'Nirvan' },
  { cardId: 31, artist: 'Johnny Cash' },
  { cardId: 32, artist: 'The Miracles' },
  { cardId: 33, artist: 'Lara Everly' },
  { cardId: 34, artist: 'Neil Young' },
  { cardId: 35, artist: 'Michael Jackson' },
  { cardId: 36, artist: 'Madonna' },
  { cardId: 37, artist: 'Roy Orbison' },
  { cardId: 38, artist: 'John Lennon' },
  { cardId: 39, artist: 'David Bowie' },
  { cardId: 40, artist: 'Simon & Garfunkel' },
  { cardId: 41, artist: 'The Doors' },
  { cardId: 42, artist: 'Van Morrison' },
  { cardId: 43, artist: 'Sly & the Family Stone' },
  { cardId: 44, artist: 'Public Enemy' },
  { cardId: 45, artist: 'The Byrds' },
  { cardId: 46, artist: 'Janis Joplin' },
  { cardId: 47, artist: 'Patti Smi' },
  { cardId: 48, artist: 'Run‐D.M.C.' },
  { cardId: 49, artist: 'Elton John' },
  { cardId: 50, artist: 'The Band' },
  { cardId: 51, artist: 'Pink Floyd' },
  { cardId: 52, artist: 'Queen' },
  { cardId: 53, artist: 'The Allman Brothers Band' },
  { cardId: 54, artist: 'Howlin’ Wolf' },
  { cardId: 55, artist: 'Eric Clapton' },
  { cardId: 56, artist: 'Dr. Dre' },
  { cardId: 57, artist: 'Grateful Dead' },
  { cardId: 58, artist: 'Parliament-Funkadelic' },
  { cardId: 59, artist: 'Aerosmith' },
  { cardId: 60, artist: 'Sex Pistols' },
  { cardId: 61, artist: 'Metallica' },
  { cardId: 62, artist: 'Joni Mitchell' },
  { cardId: 63, artist: 'Tina Turner' },
  { cardId: 64, artist: 'Phil Spector' },
  { cardId: 65, artist: 'The Kinks' },
  { cardId: 66, artist: 'Al Green' },
  { cardId: 67, artist: 'Cream' },
  { cardId: 68, artist: 'The Temptations' },
  { cardId: 69, artist: 'Jackie Wilson' },
  { cardId: 70, artist: 'The Police' },
  { cardId: 71, artist: 'Frank Zappa' },
  { cardId: 72, artist: 'AC/DC' },
  { cardId: 73, artist: 'Radiohead' },
  { cardId: 74, artist: 'Hank Williams' },
  { cardId: 75, artist: 'Eagles' },
  { cardId: 76, artist: 'The Shirelles' },
  { cardId: 77, artist: 'Beastie Boys' },
  { cardId: 78, artist: 'The Stooges' },
  { cardId: 79, artist: 'Four Tops' },
  { cardId: 80, artist: 'Elvis Costello' },
  { cardId: 81, artist: 'The Drifters' },
  { cardId: 82, artist: 'Creedence Clearwater Revival' },
  { cardId: 83, artist: 'Eminem' },
  { cardId: 84, artist: 'James Taylor' },
  { cardId: 85, artist: 'Black Sabbath' },
  { cardId: 86, artist: 'Tupac Shakur (2Pac)' },
  { cardId: 87, artist: 'Gram Parsons' },
  { cardId: 88, artist: 'JAY-Z' },
  { cardId: 89, artist: 'The Yardbirds' },
  { cardId: 90, artist: 'Santan' },
  { cardId: 91, artist: 'Tom Petty' },
  { cardId: 92, artist: 'Guns N’ Ros' },
  { cardId: 93, artist: 'Booker T. & The MG’s' },
  { cardId: 94, artist: 'Nine Inch Nails' },
  { cardId: 95, artist: 'Lynyrd Skynyrd' },
  { cardId: 96, artist: 'The Suprem' },
  { cardId: 97, artist: 'R.E.M.' },
  { cardId: 98, artist: 'Curtis Mayfield' },
  { cardId: 99, artist: 'Carl Perkins' },
  { cardId: 100, artist: 'Talking Heads' },
]
