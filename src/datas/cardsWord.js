export const cardsWord = [
  { cardNb: 0, cardWord: 'Science' },
  { cardNb: 1, cardWord: 'Satan' },
  { cardNb: 2, cardWord: 'Cygne' },
  { cardNb: 3, cardWord: 'Sumo' },
  { cardNb: 4, cardWord: 'Cerf' },
  { cardNb: 5, cardWord: 'Ciel' },
  { cardNb: 6, cardWord: 'Singe' },
  { cardNb: 7, cardWord: 'Ski' },
  { cardNb: 8, cardWord: 'Savon' },
  { cardNb: 9, cardWord: 'Zombie' },
  { cardNb: 10, cardWord: 'Déesse' },
  { cardNb: 11, cardWord: 'Dinde' },
  { cardNb: 12, cardWord: 'Douane' },
  { cardNb: 13, cardWord: 'Team' },
  { cardNb: 14, cardWord: 'Tour' },
  { cardNb: 15, cardWord: 'Télé' },
  { cardNb: 16, cardWord: 'Douche' },
  { cardNb: 17, cardWord: 'Tango' },
  { cardNb: 18, cardWord: 'Diva' },
  { cardNb: 19, cardWord: 'Tabac' },
  { cardNb: 20, cardWord: 'NASA' },
  { cardNb: 21, cardWord: 'Note' },
  { cardNb: 22, cardWord: 'None' },
  { cardNb: 23, cardWord: 'Ennemi' },
  { cardNb: 24, cardWord: 'Nerf' },
  { cardNb: 25, cardWord: 'Noël' },
  { cardNb: 26, cardWord: 'Nuage' },
  { cardNb: 27, cardWord: 'Nuque' },
  { cardNb: 28, cardWord: 'Navet' },
  { cardNb: 29, cardWord: 'Nape' },
  { cardNb: 30, cardWord: 'Muse' },
  { cardNb: 31, cardWord: 'Moto' },
  { cardNb: 32, cardWord: 'Moine' },
  { cardNb: 33, cardWord: 'Momie' },
  { cardNb: 34, cardWord: 'Mûre' },
  { cardNb: 35, cardWord: 'Moulin' },
  { cardNb: 36, cardWord: 'Magie' },
  { cardNb: 37, cardWord: 'Maki' },
  { cardNb: 38, cardWord: 'Mafia' },
  { cardNb: 39, cardWord: 'Mamba' },
  { cardNb: 40, cardWord: 'Rose' },
  { cardNb: 41, cardWord: 'Radio' },
  { cardNb: 42, cardWord: 'Ruine' },
  { cardNb: 43, cardWord: 'Rhum' },
  { cardNb: 44, cardWord: 'Rire' },
  { cardNb: 45, cardWord: 'Rallye' },
  { cardNb: 46, cardWord: 'Orage' },
  { cardNb: 47, cardWord: 'Ouragan' },
  { cardNb: 48, cardWord: 'Rêve' },
  { cardNb: 49, cardWord: 'RAP' },
  { cardNb: 50, cardWord: 'Lasso' },
  { cardNb: 51, cardWord: 'Lutin' },
  { cardNb: 52, cardWord: 'Lune' },
  { cardNb: 53, cardWord: 'Lama' },
  { cardNb: 54, cardWord: 'Lyre' },
  { cardNb: 55, cardWord: 'LOL' },
  { cardNb: 56, cardWord: 'Luge' },
  { cardNb: 57, cardWord: 'Lac' },
  { cardNb: 58, cardWord: 'Lave' },
  { cardNb: 59, cardWord: 'Lapin' },
  { cardNb: 60, cardWord: 'Chance' },
  { cardNb: 61, cardWord: 'Château' },
  { cardNb: 62, cardWord: 'Génie' },
  { cardNb: 63, cardWord: 'Jumeaux' },
  { cardNb: 64, cardWord: 'Char' },
  { cardNb: 65, cardWord: 'Chalet' },
  { cardNb: 66, cardWord: 'Juge' },
  { cardNb: 67, cardWord: 'Choc' },
  { cardNb: 68, cardWord: 'Chauve' },
  { cardNb: 69, cardWord: 'Jupon' },
  { cardNb: 70, cardWord: 'Gaz' },
  { cardNb: 71, cardWord: 'Gâteau' },
  { cardNb: 72, cardWord: 'Kiné' },
  { cardNb: 73, cardWord: 'Coma' },
  { cardNb: 74, cardWord: 'Coeur' },
  { cardNb: 75, cardWord: 'Gland' },
  { cardNb: 76, cardWord: 'Cochon' },
  { cardNb: 77, cardWord: 'Caca' },
  { cardNb: 78, cardWord: 'Café' },
  { cardNb: 79, cardWord: 'Cobaye' },
  { cardNb: 80, cardWord: 'Fusée' },
  { cardNb: 81, cardWord: 'Fête' },
  { cardNb: 82, cardWord: 'Vanille' },
  { cardNb: 83, cardWord: 'Fumée' },
  { cardNb: 84, cardWord: 'Forêt' },
  { cardNb: 85, cardWord: 'Vélo' },
  { cardNb: 86, cardWord: 'Vache' },
  { cardNb: 87, cardWord: 'Vague' },
  { cardNb: 88, cardWord: 'Fève' },
  { cardNb: 89, cardWord: 'Phobie' },
  { cardNb: 90, cardWord: 'Pizza' },
  { cardNb: 91, cardWord: 'Bouton' },
  { cardNb: 92, cardWord: 'Poney' },
  { cardNb: 93, cardWord: 'Puma' },
  { cardNb: 94, cardWord: 'Beurre' },
  { cardNb: 95, cardWord: 'Poule' },
  { cardNb: 96, cardWord: 'Pêche' },
  { cardNb: 97, cardWord: 'Bague' },
  { cardNb: 98, cardWord: 'Pouf' },
  { cardNb: 99, cardWord: 'Bombe' },
  { cardNb: 100, cardWord: '' },
  { cardNb: 101, cardWord: '' },
  { cardNb: 102, cardWord: '' },
  { cardNb: 103, cardWord: '' },
  { cardNb: 104, cardWord: '' },
  { cardNb: 105, cardWord: '' },
  { cardNb: 106, cardWord: '' },
  { cardNb: 107, cardWord: '' },
  { cardNb: 108, cardWord: '' },
  { cardNb: 109, cardWord: '' },
  { cardNb: 110, cardWord: '' },
  { cardNb: 111, cardWord: '' },
  { cardNb: 112, cardWord: '' },
  { cardNb: 113, cardWord: '' },
  { cardNb: 114, cardWord: '' },
  { cardNb: 115, cardWord: '' },
  { cardNb: 116, cardWord: '' },
  { cardNb: 117, cardWord: '' },
  { cardNb: 118, cardWord: '' },
  { cardNb: 119, cardWord: '' },
  { cardNb: 120, cardWord: '' },
  { cardNb: 121, cardWord: '' },
  { cardNb: 122, cardWord: '' },
  { cardNb: 123, cardWord: '' },
  { cardNb: 124, cardWord: '' },
  { cardNb: 125, cardWord: '' },
  { cardNb: 126, cardWord: '' },
  { cardNb: 127, cardWord: '' },
  { cardNb: 128, cardWord: '' },
  { cardNb: 129, cardWord: '' },
  { cardNb: 130, cardWord: '' },
  { cardNb: 131, cardWord: '' },
  { cardNb: 132, cardWord: '' },
  { cardNb: 133, cardWord: '' },
  { cardNb: 134, cardWord: '' },
  { cardNb: 135, cardWord: '' },
  { cardNb: 136, cardWord: '' },
  { cardNb: 137, cardWord: '' },
  { cardNb: 138, cardWord: '' },
  { cardNb: 139, cardWord: '' },
  { cardNb: 140, cardWord: '' },
  { cardNb: 141, cardWord: '' },
  { cardNb: 142, cardWord: '' },
  { cardNb: 143, cardWord: '' },
  { cardNb: 144, cardWord: '' },
  { cardNb: 145, cardWord: '' },
  { cardNb: 146, cardWord: '' },
  { cardNb: 147, cardWord: '' },
  { cardNb: 148, cardWord: '' },
  { cardNb: 149, cardWord: '' },
  { cardNb: 150, cardWord: '' },
  { cardNb: 151, cardWord: '' },
  { cardNb: 152, cardWord: '' },
  { cardNb: 153, cardWord: '' },
  { cardNb: 154, cardWord: '' },
  { cardNb: 155, cardWord: '' },
  { cardNb: 156, cardWord: '' },
  { cardNb: 157, cardWord: '' },
  { cardNb: 158, cardWord: '' },
  { cardNb: 159, cardWord: '' },
]
