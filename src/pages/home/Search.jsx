import PropTypes from 'prop-types'
import { useEffect, useState } from 'react'
import FilterControls from '@pages/home/FilterControls'
import GameList from '@pages/home/GameList'

import MyButton from '@ui/MyButton'
import MyInput from '@/components/ui/MyInput'

import { Modal, ModalContent, ModalHeader, ModalBody, ModalFooter, useDisclosure } from '@nextui-org/react'
import { modalMotionProps, modalClassNames } from '@utils/uiConfigs'

import { ImCross } from 'react-icons/im'
import { IoMdRefresh } from 'react-icons/io'

import { FaFilter } from 'react-icons/fa'

import Spacer from '@ui/Spacer'

export default function Search({ games, displaySearch, setDisplaySearch, handleRefreshGames }) {
  const [searchTxt, setSearchTxt] = useState('')

  const [filteredGames, setFilteredGames] = useState([])

  const [durationRange, setDurationRange] = useState([5, 120])
  const [nbPlayers, setNbPlayers] = useState(5)

  const [complexityRange, setComplexityRange] = useState([1, 10])
  const [tagsSelected, setTagsSelected] = useState([])
  const [decksSelected, setDecksSelected] = useState([])
  const [creatorsSelected, setCreatorsSelected] = useState([])

  const [needsApp, setNeedsApp] = useState(false)
  const [needsAccessories, setNeedsAccessories] = useState(false)

  const { isOpen, onOpen, onOpenChange, onClose } = useDisclosure()

  useEffect(() => {
    setFilteredGames(games)
  }, [])

  const openModal = () => {
    onOpen()
  }

  const resetFilters = () => {
    setDurationRange([5, 120])
    setNbPlayers(5)
    setComplexityRange([1, 10])
    setTagsSelected([])
    setCreatorsSelected([])
    setNeedsApp(false)
    setNeedsAccessories(false)
    setFilteredGames(games)
    onClose()
    setDisplaySearch(false)
  }

  const filterGames = () => {
    const filtered = games.filter((game) => {
      return (
        game.name.toLowerCase().includes(searchTxt.toLowerCase()) &&
        game.minDuration <= durationRange[1] &&
        game.maxDuration >= durationRange[0] &&
        game.minPlayers <= nbPlayers &&
        game.maxPlayers >= nbPlayers &&
        game.complexity <= complexityRange[1] &&
        game.complexity >= complexityRange[0] &&
        (tagsSelected.length === 0 || tagsSelected.some((tag) => game?.tags?.includes(tag.name))) &&
        (decksSelected.length === 0 || decksSelected.some((deck) => game?.decks?.includes(deck.name))) &&
        (creatorsSelected.length === 0 || creatorsSelected.some((creator) => game?.creators?.includes(creator.name))) &&
        (!needsApp || game.needsApp) &&
        (!needsAccessories || game.needsAccessories)
      )
    })
    setFilteredGames(filtered)
    setDisplaySearch(true)
    onClose()
  }

  useEffect(() => {
    if (searchTxt === '') {
      setDisplaySearch(false)
      return
    }
    filterGames()
  }, [searchTxt])

  return (
    <div className="w-full">
      <div className="fbc w-full gap-3">
        <MyButton classNames="hidden md:flex bg-color5" onClick={handleRefreshGames}>
          Rafraîchir
        </MyButton>
        <MyButton classNames="md:hidden min-w-2 bg-color5" onClick={handleRefreshGames}>
          <IoMdRefresh size={30} />
        </MyButton>

        <MyInput
          variant="bordered"
          value={searchTxt}
          color="primary"
          type="text"
          callback={(e) => setSearchTxt(e.target.value)}
          label="Rechercher"
        />

        {displaySearch && <div className="text-tiny">{(filteredGames && filteredGames.length) || 0} jeux trouvés</div>}

        <MyButton classNames="hidden md:flex" onClick={openModal}>
          Filtres
        </MyButton>
        <MyButton classNames="md:hidden min-w-2" onClick={openModal}>
          <FaFilter size={20} />
        </MyButton>

        <MyButton classNames="hidden md:flex bg-color2" onClick={resetFilters}>
          Réinitialiser
        </MyButton>
        <MyButton classNames="md:hidden min-w-2 bg-color2" onClick={resetFilters}>
          <ImCross size={20} />
        </MyButton>
      </div>

      {displaySearch && (
        <>
          <Spacer size={1} />
          Search :
          <GameList games={filteredGames} />
        </>
      )}

      <Modal
        backdrop="opaque"
        isOpen={isOpen}
        onOpenChange={onOpenChange}
        scrollBehavior="outside"
        motionProps={modalMotionProps}
        classNames={modalClassNames}
      >
        <ModalContent>
          {(onClose) => (
            <>
              <ModalHeader className="flex w-full flex-col items-center justify-center gap-1">
                Filtres de jeux
              </ModalHeader>
              <ModalBody>
                <FilterControls
                  durationRange={durationRange}
                  setDurationRange={setDurationRange}
                  nbPlayers={nbPlayers}
                  setNbPlayers={setNbPlayers}
                  complexityRange={complexityRange}
                  setComplexityRange={setComplexityRange}
                  tagsSelected={tagsSelected}
                  setTagsSelected={setTagsSelected}
                  creatorsSelected={creatorsSelected}
                  setCreatorsSelected={setCreatorsSelected}
                  needsApp={needsApp}
                  setNeedsApp={setNeedsApp}
                  needsAccessories={needsAccessories}
                  setNeedsAccessories={setNeedsAccessories}
                  decksSelected={decksSelected}
                  setDecksSelected={setDecksSelected}
                />
              </ModalBody>
              <ModalFooter className="flex w-full justify-between">
                <MyButton classNames="bg-color1" onClick={onClose}>
                  Fermer
                </MyButton>
                <MyButton onClick={filterGames}>Appliquer</MyButton>
              </ModalFooter>
            </>
          )}
        </ModalContent>
      </Modal>
    </div>
  )
}

Search.propTypes = {
  games: PropTypes.array.isRequired,
  displaySearch: PropTypes.bool,
  setDisplaySearch: PropTypes.func.isRequired,
  handleRefreshGames: PropTypes.func.isRequired,
}
