import PropTypes from 'prop-types'
import { useNavigate } from 'react-router-dom'
import GameCard from '@/pages/home/GameCard'

export default function GameList({ games }) {
  const navigate = useNavigate()

  return (
    <div className="grid grid-cols-[repeat(auto-fill,minmax(150px,1fr))] gap-6 md:grid-cols-[repeat(auto-fill,minmax(220px,1fr))]">
      {games &&
        games.map((game, index) => (
          <div key={index} className="relative" onClick={() => navigate(`/games/${game.id}`)}>
            <GameCard game={game} />
          </div>
        ))}
    </div>
  )
}

GameList.propTypes = {
  games: PropTypes.array.isRequired,
}
