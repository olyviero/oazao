import PropTypes from 'prop-types'
import { useMemo } from 'react'

import { GiRoundStar } from 'react-icons/gi'

export default function GameCard({ game }) {
  const averageRating = useMemo(() => {
    if (!game.ratingCount || !game.ratingSum) return ''

    return (game.ratingSum / game.ratingCount).toFixed(1)
  }, [game])

  return (
    <div key={game.id} className="cursor-pointer">
      <div
        className="relative flex-row rounded-md sm:flex-col"
        style={{
          backgroundImage: `url(${game.thumbnail})`,
          backgroundSize: 'cover',
          backgroundPosition: 'center',
          backgroundColor: 'rgba(0, 0, 0, 0.2)',
        }}
      >
        <div className="relative flex h-full min-h-[170px] w-full flex-col items-start justify-between gap-2 overflow-hidden p-2 md:min-h-[300px]">
          <div className="a0000 z-10 bg-white/0 hover:bg-white/40"></div>
          <div className="fbc z-20 text-xs">
            <div className="rounded-md bg-white p-1">
              😊 {game.minPlayers}-{game.maxPlayers}
            </div>
            <div className="rounded-md bg-white p-1">
              ⌛️ {game.minDuration}-{game.maxDuration} min
            </div>
          </div>
        </div>
      </div>
      <div className="fbc py-3">
        <p>{game.name}</p>
        <p className="fcc gap-1">
          {averageRating !== '' ? (
            <>
              {averageRating}
              <GiRoundStar size={18} className="text-color3" />
            </>
          ) : (
            '-'
          )}
        </p>
      </div>
    </div>
  )
}

GameCard.propTypes = {
  game: PropTypes.object.isRequired,
}
