import { useEffect, useMemo, useState } from 'react'
import { useParams } from 'react-router-dom'

import { Chip } from '@nextui-org/react'

// import Banner from '@ui/Banner'
import Header from '@ui/Header'
import GameTabs from '@pages/home/GameTabs'

import Page from '@layout/Page'

import MyRating from '@ui/MyRating'

import { namesArrayToTagsArray } from '@utils/games'

import { getAllGames } from '@db/helpers'

export default function Game() {
  const { gameId } = useParams()
  const [game, setGame] = useState(null)
  const [gameTags, setGameTags] = useState(null)

  const averageRating = useMemo(() => {
    if (game) {
      if (!game.ratingCount || !game.ratingSum) return ''
      return (game.ratingSum / game.ratingCount).toFixed(1)
    }
  }, [game])

  const fetchAndStoreGames = async (gameId, forceUpdate = false) => {
    const storedGamesJSON = localStorage.getItem('games')
    let storedGames = storedGamesJSON ? JSON.parse(storedGamesJSON) : null

    let game = storedGames ? storedGames.find((game) => game.id === gameId) : null

    if (forceUpdate || !game) {
      const gamesArray = await getAllGames()
      localStorage.setItem('games', JSON.stringify(gamesArray))
      game = gamesArray.find((game) => game.id === gameId)

      if (!game) {
        console.error("Jeu avec l'ID", gameId, 'non trouvé')
      }
    }

    setGameTags(namesArrayToTagsArray(game.tags))
    setGame(game)
  }

  useEffect(() => {
    if (gameId) fetchAndStoreGames(gameId)
  }, [gameId])

  return (
    <Page>
      {!game ? (
        <div className="flex w-full flex-col gap-3">Chargement ...</div>
      ) : (
        <div className="flex w-full flex-col gap-3">
          <div className="flex w-full flex-col">
            <Header title={game.name} />
            <div className="flex flex-col items-center justify-center gap-5 text-color3">
              <MyRating iconSet="star" size={20} readonly value={averageRating || 0} />
              <div className="flex flex-wrap gap-1">
                {gameTags &&
                  gameTags.map((tag, index) => (
                    <Chip size="sm" key={index} className="bg-lightgray" bordered="true">
                      {tag.title}
                    </Chip>
                  ))}
              </div>
            </div>
            <div className="fcc mt-3 text-tiny">Créé par : {game.authorUsername}</div>
          </div>
          {/* <Banner img={game?.thumbnail} height={200} /> */}
          <GameTabs game={game} fetchAndStoreGames={fetchAndStoreGames} />
        </div>
      )}
    </Page>
  )
}
