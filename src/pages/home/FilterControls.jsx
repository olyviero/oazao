import PropTypes from 'prop-types'

import { Slider, Button, CheckboxGroup, Switch } from '@nextui-org/react'
import MyNumberInput from '@/components/ui/MyNumberInput'
import { MyCheckbox } from '@/components/ui/MyCheckbox'

import { gamesTags } from '@/datas/gamesTags'
import { gamesCreators } from '@/datas/gamesCreators'
import { gamesDecks } from '@/datas/gamesDecks'

export default function FilterControls({
  durationRange,
  setDurationRange,
  nbPlayers,
  setNbPlayers,
  complexityRange,
  setComplexityRange,
  tagsSelected,
  setTagsSelected,
  creatorsSelected,
  setCreatorsSelected,
  needsApp,
  setNeedsApp,
  needsAccessories,
  setNeedsAccessories,
  decksSelected,
  setDecksSelected,
}) {
  return (
    <div className="flex flex-col flex-wrap items-center justify-center gap-5 rounded-xl p-3 md:flex-row">
      <MyNumberInput
        variant="bordered"
        value={nbPlayers}
        color="primary"
        min={1}
        max={50}
        type="number"
        callback={(e) => setNbPlayers(e.target.value)}
        label="Nombre de joueurs"
        required={true}
      />

      <div className="flex w-full gap-5">
        <Slider
          label="Durée de la partie (min)"
          step={5}
          minValue={5}
          maxValue={120}
          defaultValue={[5, 120]}
          value={[durationRange[0], durationRange[1]]}
          showSteps={true}
          showTooltip={false}
          showOutline={false}
          onChange={setDurationRange}
          classNames={{
            base: 'w-full ',
            filler: 'bg-gradient-to-r from-primary to-secondary',
            labelWrapper: 'mb-2',
            label: 'font-bold',
            value: 'font-bold text-xl',
            track: 'bg-primary/30',
            thumb: [
              'transition-size',
              'bg-gradient-to-r from-primary to-secondary',
              'data-[dragging=true]:shadow-lg data-[dragging=true]:shadow-black/20',
              'data-[dragging=true]:w-7 data-[dragging=true]:h-7 data-[dragging=true]:after:h-6 data-[dragging=true]:after:w-6',
            ],
            step: 'data-[in-range=true]:bg-black/30 dark:data-[in-range=true]:bg-white/50',
          }}
        />

        <Slider
          label="Complexité"
          step={1}
          minValue={1}
          maxValue={10}
          value={complexityRange}
          showSteps={true}
          showTooltip={false}
          showOutline={false}
          onChange={setComplexityRange}
          classNames={{
            base: 'w-full',
            filler: 'bg-gradient-to-r from-primary to-secondary',
            labelWrapper: 'mb-2',
            label: 'font-bold',
            value: 'font-bold text-xl',
            track: 'bg-primary/30',
            thumb: [
              'transition-size',
              'bg-gradient-to-r from-primary to-secondary',
              'data-[dragging=true]:shadow-lg data-[dragging=true]:shadow-black/20',
              'data-[dragging=true]:w-7 data-[dragging=true]:h-7 data-[dragging=true]:after:h-6 data-[dragging=true]:after:w-6',
            ],
            step: 'data-[in-range=true]:bg-black/30 dark:data-[in-range=true]:bg-white/50',
          }}
        />
      </div>

      <div className="flex w-full flex-col gap-1">
        <h4>Catégories</h4>
        <CheckboxGroup
          className="gap-1"
          label=""
          orientation="horizontal"
          value={tagsSelected}
          onChange={setTagsSelected}
        >
          {gamesTags.map((tag) => (
            <MyCheckbox key={tag.id} value={tag}>
              {tag.title}
            </MyCheckbox>
          ))}
        </CheckboxGroup>
      </div>

      <div className="flex w-full flex-col gap-1">
        <h4>Création</h4>
        <CheckboxGroup
          className="gap-1"
          label=""
          orientation="horizontal"
          value={creatorsSelected}
          onChange={setCreatorsSelected}
        >
          {gamesCreators.map((creator) => (
            <MyCheckbox key={creator.id} value={creator}>
              {creator.title}
            </MyCheckbox>
          ))}
        </CheckboxGroup>
      </div>

      <div className="flex w-full flex-col gap-1">
        <h4>Jeu nécessaire</h4>
        <CheckboxGroup
          className="gap-1"
          label=""
          orientation="horizontal"
          value={decksSelected}
          onChange={setDecksSelected}
        >
          {gamesDecks.map((el) => (
            <MyCheckbox key={el.id} value={el}>
              {el.title}
            </MyCheckbox>
          ))}
        </CheckboxGroup>
      </div>

      <div className="flex gap-5">
        <div className="flex w-full flex-col gap-1">
          <h4>Nécessite l'application</h4>
          <Switch isSelected={needsApp} onValueChange={setNeedsApp} />
        </div>

        <div className="flex w-full flex-col gap-1">
          <h4>Nécessite des accessoires supplémentaires</h4>
          <Switch isSelected={needsAccessories} onValueChange={setNeedsAccessories} />
        </div>
      </div>
    </div>
  )
}

FilterControls.propTypes = {
  durationRange: PropTypes.array.isRequired,
  setDurationRange: PropTypes.func.isRequired,

  nbPlayers: PropTypes.number.isRequired,

  complexityRange: PropTypes.array.isRequired,
  setComplexityRange: PropTypes.func.isRequired,

  tagsSelected: PropTypes.array.isRequired,
  setTagsSelected: PropTypes.func.isRequired,

  creatorsSelected: PropTypes.array.isRequired,
  setCreatorsSelected: PropTypes.func.isRequired,

  needsApp: PropTypes.bool.isRequired,
  setNeedsApp: PropTypes.func.isRequired,

  needsAccessories: PropTypes.bool.isRequired,
  setNeedsAccessories: PropTypes.func.isRequired,
}
