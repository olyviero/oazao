import PropTypes from 'prop-types'
import { Link, useNavigate } from 'react-router-dom'

import Slider from '@components/Slider'

import { Splide, SplideSlide, SplideTrack } from '@splidejs/react-splide'
import GameCard from '@pages/home/GameCard'
import Spacer from '@ui/Spacer'
import { useMemo } from 'react'

import GameList from '@pages/home/GameList'

const nbGamesPerList = 6

export default function Store({ games }) {
  const navigate = useNavigate()

  // Charger les paramètres depuis localStorage
  const settings = useMemo(() => JSON.parse(localStorage.getItem('settings')) || {}, [])
  const { tagsSelected } = settings

  const filterGamesByTags = (games, tags) => {
    if (!tags || tags.length === 0) return games
    return games.filter((game) => game.tags && game.tags.some((tag) => tags.includes(tag)))
  }

  const gamesFilteredByTags = useMemo(() => {
    if (games) {
      const filtered = filterGamesByTags(games, tagsSelected)
      return filtered.slice(0, nbGamesPerList)
    }
  }, [games, tagsSelected])

  const gamesSortedByPopularity = useMemo(() => {
    if (games) {
      const sorted = [...games].sort((a, b) => b.ratingSum / b.ratingCount - a.ratingSum / a.ratingCount)
      return sorted.slice(0, nbGamesPerList)
    }
  }, [games])

  const gamesSortedByDate = useMemo(() => {
    if (games) {
      const sorted = [...games].sort((a, b) => new Date(b.createdAt) - new Date(a.createdAt))
      return sorted.slice(0, nbGamesPerList)
    }
  }, [games])

  return (
    <div className="fccc">
      <div className="mx-auto w-full max-w-screen-xl gap-3">
        <div className="fbc w-full">
          <h2>Tous les jeux</h2>
        </div>
        <GameList games={games} />

        <Spacer size={1.5} />

        <Slider />

        <Spacer size={1.5} />

        {/* --------------------------- */}
        {/* FOR LATER */}
        {/* --------------------------- */}
        <div className="fbc w-full">
          <h2>Pour vous</h2>
          <Link to="/">Voir plus</Link>
        </div>

        <Splide
          options={{
            type: 'slide',
            perPage: 4,
            gap: '1rem',
            arrows: false,
            pagination: false,
            padding: { right: 80 },
            breakpoints: {
              768: {
                perPage: 2,
                padding: { right: 40 },
              },
            },
          }}
          hasTrack={false}
          aria-label="Games for you"
        >
          {gamesFilteredByTags && (
            <SplideTrack>
              {gamesFilteredByTags.map((game, index) => (
                <SplideSlide key={index} onClick={() => navigate(`/games/${game.id}`)}>
                  <GameCard game={game} />
                </SplideSlide>
              ))}
            </SplideTrack>
          )}
        </Splide>

        <Spacer size={1.5} />

        <div className="fbc w-full">
          <h2>Les plus populaires</h2>
          <Link to="/">Voir plus</Link>
        </div>

        <Splide
          options={{
            type: 'slide',
            perPage: 4,
            gap: '1rem',
            arrows: false,
            pagination: false,
            padding: { right: 80 },
            breakpoints: {
              768: {
                perPage: 2,
                padding: { right: 40 },
              },
            },
          }}
          hasTrack={false}
          aria-label="Games for you"
        >
          {gamesSortedByPopularity && (
            <SplideTrack>
              {gamesSortedByPopularity.map((game, index) => (
                <SplideSlide key={index} onClick={() => navigate(`/games/${game.id}`)}>
                  <GameCard game={game} />
                </SplideSlide>
              ))}
            </SplideTrack>
          )}
        </Splide>

        <Spacer size={1.5} />

        <div className="fbc w-full">
          <h2>Les Nouveautés</h2>
          <Link to="/">Voir plus</Link>
        </div>

        <Splide
          options={{
            type: 'slide',
            perPage: 4,
            gap: '1rem',
            arrows: false,
            pagination: false,
            padding: { right: 80 },
            breakpoints: {
              768: {
                perPage: 2,
                padding: { right: 40 },
              },
            },
          }}
          hasTrack={false}
          aria-label="Games for you"
        >
          {gamesSortedByDate && (
            <SplideTrack>
              {gamesSortedByDate.map((game, index) => (
                <SplideSlide key={index} onClick={() => navigate(`/games/${game.id}`)}>
                  <GameCard game={game} />
                </SplideSlide>
              ))}
            </SplideTrack>
          )}
        </Splide>
      </div>
    </div>
  )
}

Store.propTypes = {
  games: PropTypes.array,
}
