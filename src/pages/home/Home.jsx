import { useEffect, useState } from 'react'

import Store from '@pages/home/Store'
import Search from '@pages/home/Search'
import Page from '@layout/Page'

import Header from '@ui/Header'

import { getAllGames } from '@db/helpers'

export default function Home() {
  const [displaySearch, setDisplaySearch] = useState(false)
  const [games, setGames] = useState([])

  const fetchAndStoreGames = async (forceUpdate = false) => {
    if (forceUpdate || !localStorage.getItem('games')) {
      const gamesArray = await getAllGames()
      localStorage.setItem('games', JSON.stringify(gamesArray))
      setGames(gamesArray)
    } else {
      const storedGames = localStorage.getItem('games')
      setGames(JSON.parse(storedGames))
    }
  }

  useEffect(() => {
    fetchAndStoreGames()
  }, [])

  const handleRefreshGames = () => {
    localStorage.removeItem('games')
    fetchAndStoreGames(true)
  }

  return (
    <Page>
      {games && games.length > 0 && (
        <>
          <Search
            games={games}
            displaySearch={displaySearch}
            setDisplaySearch={setDisplaySearch}
            handleRefreshGames={handleRefreshGames}
          />

          {!displaySearch && <Store games={games} />}
        </>
      )}
    </Page>
  )
}
