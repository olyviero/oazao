import PropTypes from 'prop-types'
import { useEffect, useState } from 'react'
import { useAuth } from '@/contexts/AuthContext'
import { Link } from 'react-router-dom'

import { Tabs, Tab, Card, CardBody, Accordion, AccordionItem } from '@nextui-org/react'

import Spacer from '@ui/Spacer'
import MyButton from '@/components/ui/MyButton'
import MyRating from '@/components/ui/MyRating'

import { namesArrayToToolsArray } from '@/utils/games'

import DynamicToolComponentLoader from '@/components/DynamicToolComponentLoader'
import { tabsClassNames } from '@/utils/uiConfigs'

import { saveUserRating } from '@db/helpers'

export default function GameTabs({ game, fetchAndStoreGames }) {
  const { user } = useAuth()
  const [rating, setRating] = useState(0)
  const [gameTools, setGameTools] = useState(null)

  // Charger la note depuis le localStorage
  useEffect(() => {
    if (user && game) {
      const storedRatings = JSON.parse(localStorage.getItem('ratings')) || []
      const userRatingObj = storedRatings.find((r) => r.gameId === game.id)
      if (userRatingObj) {
        setRating(userRatingObj.rating)
      }
    }
  }, [user, game])

  const handleSaveRating = async () => {
    try {
      await saveUserRating(user.uid, game.id, rating)

      // Mettre à jour le localStorage
      const storedRatings = JSON.parse(localStorage.getItem('ratings')) || []
      const updatedRatings = storedRatings.filter((r) => r.gameId !== game.id)

      // Ajouter ou mettre à jour la note pour le jeu courant
      updatedRatings.push({ gameId: game.id, rating })

      localStorage.setItem('ratings', JSON.stringify(updatedRatings))
      console.log('handleSaveRating', rating)
      fetchAndStoreGames(game.id, true)
    } catch (error) {
      console.error('Erreur lors de la sauvegarde de la note:', error)
    }
  }

  useEffect(() => {
    console.log(rating)
  }, [rating])

  useEffect(() => {
    // Load tools for game
    if (game && game.tools) setGameTools(namesArrayToToolsArray(game.tools))
  }, [game])

  return (
    <div className="mx-auto flex w-full flex-col">
      <Tabs aria-label="Options" fullWidth classNames={tabsClassNames}>
        <Tab key="presentation" title="Présentation">
          <Card className="border-none bg-transparent shadow-none">
            <CardBody className="flex flex-col gap-5">
              {game.txtGear && game.txtGear.trim() !== '' && (
                <div className="flex flex-col gap-2">
                  <h3>Matériel</h3>
                  <p>{game.txtGear || ''}</p>
                </div>
              )}

              {game.txtSetup && game.txtSetup.trim() !== '' && (
                <div className="flex flex-col gap-2">
                  <h3>Mise en place</h3>
                  <p>{game.txtSetup || ''}</p>
                </div>
              )}

              {game.txtHowToPlay && game.txtHowToPlay.trim() !== '' && (
                <div className="flex flex-col gap-2">
                  <h3>Comment jouer ?</h3>
                  <p>{game.txtHowToPlay || ''}</p>
                </div>
              )}

              {game.txtEndGame && game.txtEndGame.trim() !== '' && (
                <div className="flex flex-col gap-2">
                  <h3>Fin de partie</h3>
                  <p>{game.txtEndGame || ''}</p>
                </div>
              )}

              {game.imgUrl && game.imgUrl.trim() !== '' && (
                <div className="flex flex-col gap-2">
                  <h3>Illustration</h3>
                  <img src={game.imgUrl} className="w-full" />
                </div>
              )}

              <Spacer size={2} />

              {game.buylink && game.buylink.trim() !== '' && (
                <Link to={game.buylink} target="_blank">
                  <MyButton classNames="bg-color2">Acheter le jeu complet</MyButton>
                </Link>
              )}
            </CardBody>
          </Card>
        </Tab>
        <Tab key="tools" title="Outils">
          <Card
            className="border-none bg-transparent shadow-none"
            classNames={{
              body: 'px-0 mx-0',
            }}
          >
            <CardBody>
              <Accordion
                variant="splitted"
                className="mx-0 px-0"
                itemClasses={{
                  base: 'py-0 w-full shadow-sm',
                  title: 'text-2xl',
                  trigger: 'px-2 py-0 data-[hover=true]:bg-default-100 rounded-lg h-14 flex items-center shadow-none',
                  // indicator: 'text-medium',
                  content: 'text-small px-0',
                }}
              >
                {gameTools &&
                  gameTools.map((tool, index) => (
                    <AccordionItem
                      key={index}
                      aria-label={tool.title}
                      title={tool.title}
                      className="border-1 border-gray !shadow-none"
                    >
                      <DynamicToolComponentLoader key={index} toolId={tool.id} />
                    </AccordionItem>
                  ))}
              </Accordion>
            </CardBody>
          </Card>
        </Tab>
        <Tab key="community" title="Communauté">
          <Card className="border-none bg-transparent shadow-none">
            <CardBody>
              {!user ? (
                <div className="flex flex-col gap-3">
                  <span className="text-center text-xl">
                    <Link to="/login">Connectes-toi</Link> pour accéder à cette section
                  </span>

                  <Spacer size={1} />

                  <ul className="flex flex-col gap-3">
                    <li>Découvre des variations sur ce jeu et propose les tiennes</li>
                    <li>Note et sauvegarde tes jeux préférés</li>
                    <li>Accède aux commentaires</li>
                    <li>...</li>
                  </ul>
                </div>
              ) : (
                <div className="flex flex-col gap-3">
                  <h3 className="text-center">Ma note</h3>
                  <div className="fccc gap-6">
                    <MyRating iconSet="star" size={50} value={rating || 0} callback={(rate) => setRating(rate)} />
                    <MyButton
                      classNames="bg-purewhite text-black border-black border-1 min-w-[0px]"
                      onClick={() => handleSaveRating()}
                    >
                      Voter !
                    </MyButton>
                  </div>
                  <Spacer size={1} />
                  <h3 className="text-center">Variations</h3>
                  Variations de jeux à venir ...
                  <Spacer size={1} />
                  <h3 className="text-center">Commentaires</h3>
                  Commentaires à venir ...
                </div>
              )}
            </CardBody>
          </Card>
        </Tab>
      </Tabs>
    </div>
  )
}

GameTabs.propTypes = {
  game: PropTypes.object.isRequired,
}
