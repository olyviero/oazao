import { useNavigate } from 'react-router-dom'
import { useAuth } from '@contexts/AuthContext'

import Page from '@layout/Page'
import MyButton from '@ui/MyButton'

import Header from '@ui/Header'
import { useCallback, useEffect, useState } from 'react'
import LinkBox from '@components/ui/LinkBox'

import { getGamesCreatedByUser, getAllGames } from '@db/helpers'

export default function MyGames() {
  const navigate = useNavigate()
  const { user, isMe } = useAuth()
  const [userGames, setUserGames] = useState([])

  const fetchUserGames = useCallback(
    async (forceUpdate = false) => {
      const settingsKey = 'userGames'
      const savedUserGames = localStorage.getItem(settingsKey)

      if (forceUpdate || !savedUserGames) {
        let gamesArray
        if (!isMe) gamesArray = await getGamesCreatedByUser(user.uid)
        else gamesArray = await getAllGames() // For me userGames = games

        setUserGames(gamesArray)
        localStorage.setItem(settingsKey, JSON.stringify(gamesArray))
      } else if (user) {
        setUserGames(JSON.parse(savedUserGames))
      }
    },
    [user, isMe]
  )

  useEffect(() => {
    fetchUserGames()
  }, [fetchUserGames])

  const handleRefreshGames = () => {
    if (user) {
      localStorage.removeItem('games')
      fetchUserGames(true)
    }
  }

  useEffect(() => {
    console.log(user.uid)
  }, [user])
  return (
    <Page>
      <Header title="Mes jeux" />

      <div className="fbc gap-3">
        <MyButton classNames="bg-color5" onClick={handleRefreshGames}>
          Rafraîchir
        </MyButton>

        <MyButton onClick={() => navigate('/gamefactory')} classNames="bg-color4 min-w-0">
          Créer un nouveau jeu
        </MyButton>
      </div>

      <div className="fccc gap-3">
        {!userGames || userGames.length < 1 ? (
          <div>Vous n'avez pas encore créé de jeu ...</div>
        ) : (
          <>
            {userGames &&
              userGames.map((game) => (
                <LinkBox
                  key={game.id}
                  txt={`${game.name}`}
                  txt2={`${game.ratingSum / game.ratingCount || ''}`}
                  txt3={`${game.author !== user.uid ? '(' + game.authorUsername + ')' : ''}`}
                  to={`/gamefactory/${game.id}`}
                />
              ))}
          </>
        )}
      </div>
    </Page>
  )
}
