import { useNavigate } from 'react-router-dom'
import { useAuth } from '@/contexts/AuthContext'
import { doSignOut } from '@/firebase/auth'

import Page from '@layout/Page'

import { customAlert } from '@ui/customAlert'

import SuggestionsBox from '@components/SuggestionsBox'

import { GrEdit } from 'react-icons/gr'

import MyButton from '@ui/MyButton'

import LinkBox from '@ui/LinkBox'
import Header from '@ui/Header'

export default function Profile() {
  const navigate = useNavigate()
  const { user, username, photoURL } = useAuth()

  const handleSignOut = () => {
    customAlert('Déconnexion ?', () =>
      doSignOut().then(() => {
        navigate('/')
      })
    )
  }

  return (
    <Page>
      <Header title="Profil" noreturn />

      {user && (
        <>
          <div className="fbc">
            <div className="flex items-center gap-3">
              <img
                className="mx-auto h-14 w-14 rounded-full border-1"
                src={
                  // photoURL ||
                  'https://toppng.com/uploads/preview/roger-berry-avatar-placeholder-11562991561rbrfzlng6h.png'
                }
                alt="avatar"
              />
              <div className="fc">
                <div className="font-bold">{username || ''}</div>
                <div className="text-tiny">{user.uid}</div>
              </div>
            </div>
            <MyButton
              // onClick={handleEditProfile}
              classNames="bg-color4 min-w-0"
            >
              <GrEdit />
            </MyButton>
          </div>

          <div className="fccc gap-3">
            <LinkBox txt="Paramètres" to="/profile/settings" />
            <LinkBox txt="Mes jeux" to="/profile/mygames" />
          </div>

          {/* <SuggestionsBox /> */}

          <MyButton onClick={handleSignOut} color="danger">
            Déconnexion
          </MyButton>
        </>
      )}
    </Page>
  )
}
