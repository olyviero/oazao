import { useParams, useNavigate } from 'react-router-dom'
import { Input, Textarea, Slider, CheckboxGroup, Switch } from '@nextui-org/react'
import { useEffect, useState } from 'react'
import { useAuth } from '@contexts/AuthContext'
import ImageUploader from '@components/ImageUploader'

import Page from '@layout/Page'

import { MyCheckbox } from '@/components/ui/MyCheckbox'
import MyButton from '@ui/MyButton'

import { customAlert } from '@ui/customAlert'
import Spacer from '@ui/Spacer'

import { gamesTags } from '@/datas/gamesTags'
import { gamesCreators } from '@/datas/gamesCreators'
import { gamesTools } from '@/datas/gamesTools'
import { gamesDecks } from '@/datas/gamesDecks'

import { sliderClassNames } from '@/utils/uiConfigs'

import {
  tagsArrayToNamesArray,
  namesArrayToTagsArray,
  creatorsArrayToNamesArray,
  namesArrayToCreatorsArray,
  toolsArrayToNamesArray,
  namesArrayToToolsArray,
  decksArrayToNamesArray,
  namesArrayToDecksArray,
} from '@/utils/games'

import { getGame, createGame, updateGame, deleteGame } from '@db/helpers'
import Header from '@ui/Header'

export default function GameFactory() {
  const navigate = useNavigate()

  const { user, isMe } = useAuth()
  const { gameKey } = useParams()

  const [name, setName] = useState('')
  const [thumbnailUrl, setThumbnailUrl] = useState('')

  const [durationRange, setDurationRange] = useState([5, 60])
  const [playersRange, setPlayersRange] = useState([2, 5])
  const [complexity, setComplexity] = useState(5)

  const [tagsSelected, setTagsSelected] = useState([])
  const [creatorsSelected, setCreatorsSelected] = useState([])
  const [decksSelected, setDecksSelected] = useState([])
  const [toolsSelected, setToolsSelected] = useState([])

  const [needsApp, setNeedsApp] = useState(false)
  const [needsAccessories, setNeedsAccessories] = useState(false)

  const [buylink, setBuylink] = useState('')

  const [txtGear, setTxtGear] = useState('')
  const [txtSetup, setTxtSetup] = useState('')
  const [txtHowToPlay, setTxtHowToPlay] = useState('')
  const [txtEndGame, setTxtEndGame] = useState('')

  const [imgUrl, setImgUrl] = useState('')
  const [author, setAuthor] = useState(null)

  // Load game data if edition mode
  useEffect(() => {
    // La fonction asynchrone interne pour gérer l'appel asynchrone
    const loadGameData = async () => {
      if (gameKey) {
        const game = await getGame(gameKey)
        if (game) {
          setName(game.name)
          setDurationRange([game.minDuration, game.maxDuration])
          setPlayersRange([game.minPlayers, game.maxPlayers])
          setComplexity(game.complexity || 5)
          if (game.tags) setTagsSelected(namesArrayToTagsArray(game.tags))
          if (game.creators) setCreatorsSelected(namesArrayToCreatorsArray(game.creators))
          if (game.tools) setToolsSelected(namesArrayToToolsArray(game.tools))
          if (game.decks) setDecksSelected(namesArrayToDecksArray(game.decks))
          setNeedsApp(game.needsApp || false)
          setNeedsAccessories(game.needsAccessories || false)
          setBuylink(game.buylink || '')
          setThumbnailUrl(game.thumbnail || '')
          setTxtGear(game.txtGear || '')
          setTxtSetup(game.txtSetup || '')
          setTxtHowToPlay(game.txtHowToPlay || '')
          setTxtEndGame(game.txtEndGame || '')
          setImgUrl(game.imgUrl || '')
          setAuthor(game.author || '')
        }
      }
    }

    loadGameData()
  }, [gameKey])

  // -------------------------------------------------------------------------------------
  const handleCreateOrUpdateGame = async () => {
    const gameData = {
      name,
      minDuration: durationRange[0],
      maxDuration: durationRange[1],
      minPlayers: playersRange[0],
      maxPlayers: playersRange[1],
      complexity: complexity || 5,
      tags: tagsArrayToNamesArray(tagsSelected),
      creators: creatorsArrayToNamesArray(creatorsSelected),
      decks: decksArrayToNamesArray(decksSelected),
      tools: toolsArrayToNamesArray(toolsSelected),
      needsApp,
      needsAccessories,
      buylink,
      thumbnail: thumbnailUrl,
      // createdAt: Date.now(),
      txtGear,
      txtSetup,
      txtHowToPlay,
      txtEndGame,
      imgUrl,
    }

    if (gameKey) {
      // Update
      await updateGame(gameKey, gameData)
    } else {
      // Create
      gameData.author = user?.uid || 'inconnu'
      await createGame(gameData)
    }
    navigate('/profile/mygames')
    console.log('Jeu créé ou mis à jour avec succès !')
  }

  const confirmDeleteGame = (gameKey) => {
    customAlert('Êtes-vous sûr de vouloir supprimer ce jeu ?', () => handleDeleteGame(gameKey))
  }

  const handleDeleteGame = async (gameKey) => {
    await deleteGame(gameKey)
    navigate('/profile/mygames')
  }

  const handleGameThumbnail = () => {
    handleCreateOrUpdateGame()
  }

  useEffect(() => {
    console.log(thumbnailUrl)
  }, [thumbnailUrl])

  return (
    <Page>
      <Header title={gameKey ? 'Éditer le jeu' : 'Créer un jeu'} />
      {isMe && gameKey && author !== user?.uid && (
        <div className="text-center text-xl font-bold text-color1">Attention, ce jeu n'est pas à toi ...</div>
      )}
      {!gameKey || (gameKey && author === user?.uid) || isMe ? (
        <>
          <div className="flex w-full flex-col gap-6">
            {gameKey && (
              <>
                <ImageUploader
                  setThumbnailUrl={setThumbnailUrl}
                  gameKey={gameKey}
                  thumbnailUrl={thumbnailUrl}
                  callBack={handleGameThumbnail}
                />
                <p className="font-bold italic text-color2">
                  (Penser à cliquer sur éditer en bas une fois l'image uploadée)
                </p>
              </>
            )}

            <div className="fccc gap-3 md:flex-row">
              <Input
                variant="bordered"
                color="primary"
                type="text"
                value={name}
                label="Nom"
                onChange={(e) => setName(e.target.value)}
                required
              />
            </div>

            <div className="fccc gap-5 rounded-xl md:flex-row">
              <Slider
                label="Durée de le partie (minutes)"
                step={5}
                minValue={5}
                maxValue={240}
                value={[durationRange[0] || 5, durationRange[1] || 60]}
                showSteps={true}
                showTooltip={false}
                showOutline={false}
                onChange={setDurationRange}
                classNames={sliderClassNames}
              />

              <Slider
                label="Nombre de joueurs"
                step={1}
                minValue={1}
                maxValue={50}
                value={[playersRange[0] || 2, playersRange[1] || 5]}
                showSteps={true}
                showTooltip={false}
                showOutline={false}
                onChange={setPlayersRange}
                classNames={sliderClassNames}
              />

              <Slider
                label="Complexité"
                step={1}
                minValue={1}
                maxValue={10}
                value={complexity}
                showSteps={true}
                showTooltip={false}
                showOutline={false}
                onChange={(e) => setComplexity(e)}
                classNames={sliderClassNames}
              />
            </div>

            <Textarea
              variant="bordered"
              type="text"
              color="primary"
              value={txtGear}
              label="Matériel"
              onChange={(e) => setTxtGear(e.target.value)}
            />

            <Textarea
              variant="bordered"
              type="text"
              color="primary"
              value={txtSetup}
              label="Mise en place"
              onChange={(e) => setTxtSetup(e.target.value)}
            />

            <Textarea
              variant="bordered"
              type="text"
              color="primary"
              value={txtHowToPlay}
              label="Comment jouer ?"
              onChange={(e) => setTxtHowToPlay(e.target.value)}
            />

            <Textarea
              variant="bordered"
              type="text"
              color="primary"
              value={txtEndGame}
              label="Fin de partie"
              onChange={(e) => setTxtEndGame(e.target.value)}
            />

            <div className="fccc gap-3 md:flex-row">
              <Input
                variant="bordered"
                color="primary"
                type="text"
                value={imgUrl}
                label="URL d'une image explicative"
                onChange={(e) => setImgUrl(e.target.value)}
                required
              />
            </div>

            <div className="flex w-full flex-col gap-3">
              <p className="text-small font-bold">Catégories</p>
              <CheckboxGroup
                className="gap-1"
                label=""
                orientation="horizontal"
                value={tagsSelected}
                onChange={setTagsSelected}
              >
                {gamesTags.map((el) => (
                  <MyCheckbox key={el.id} value={el}>
                    {el.title}
                  </MyCheckbox>
                ))}
              </CheckboxGroup>
            </div>

            <div className="flex w-full flex-col gap-3">
              <p className="text-small font-bold">Création</p>
              <CheckboxGroup
                className="gap-1"
                label=""
                orientation="horizontal"
                value={creatorsSelected}
                onChange={setCreatorsSelected}
              >
                {gamesCreators.map((el) => (
                  <MyCheckbox key={el.id} value={el}>
                    {el.title}
                  </MyCheckbox>
                ))}
              </CheckboxGroup>
            </div>

            <div className="flex w-full flex-col gap-3">
              <p className="text-small font-bold">Jeu nécessaire</p>
              <CheckboxGroup
                className="gap-1"
                label=""
                orientation="horizontal"
                value={decksSelected}
                onChange={setDecksSelected}
              >
                {gamesDecks.map((el) => (
                  <MyCheckbox key={el.id} value={el}>
                    {el.title}
                  </MyCheckbox>
                ))}
              </CheckboxGroup>
            </div>

            <div className="fbc gap-3">
              <p className="text-small font-bold">Nécessite l'application</p>
              <Switch isSelected={needsApp} onValueChange={setNeedsApp} />
            </div>

            <div className="fbc gap-3">
              <p className="text-small font-bold">Nécessite des accessoires supplémentaires</p>
              <Switch isSelected={needsAccessories} onValueChange={setNeedsAccessories} />
            </div>

            <div className="flex w-full flex-col gap-3">
              <p className="text-small font-bold">Outils utiles</p>
              <CheckboxGroup
                className="gap-1"
                label=""
                orientation="horizontal"
                value={toolsSelected}
                onChange={setToolsSelected}
              >
                {gamesTools.map((tool) => (
                  <div key={tool.id}>
                    {(isMe || !tool.isUnique) && <MyCheckbox value={tool}>{tool.title}</MyCheckbox>}
                  </div>
                ))}
              </CheckboxGroup>
            </div>

            <div className="fccc gap-3 md:flex-row">
              <Input
                variant="bordered"
                type="text"
                value={buylink}
                label="Lien pour acheter le jeu"
                onChange={(e) => setBuylink(e.target.value)}
              />
            </div>

            <Spacer size={2} />

            <div className="fbc">
              {gameKey && (
                <MyButton classNames="bg-color1" onClick={() => confirmDeleteGame(gameKey)}>
                  Supprimer le jeu
                </MyButton>
              )}

              <MyButton classNames="bg-color4" onClick={handleCreateOrUpdateGame}>
                {gameKey ? 'Éditer' : 'Créer'}
              </MyButton>
            </div>
          </div>
        </>
      ) : (
        <div className="text-center text-xl font-bold text-color1">
          Vous n'avez pas les droits pour éditer ce jeu ...
        </div>
      )}
    </Page>
  )
}
