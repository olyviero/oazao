import { useAuth } from '@contexts/AuthContext'

import Page from '@layout/Page'
import { CheckboxGroup, Switch } from '@nextui-org/react'

import { MyCheckbox } from '@/components/ui/MyCheckbox'
import Spacer from '@ui/Spacer'
import MyButton from '@ui/MyButton'

import { gamesTags } from '@/datas/gamesTags'
import { useCallback, useEffect, useState } from 'react'
import Header from '@ui/Header'

import { tagsArrayToNamesArray, namesArrayToTagsArray } from '@/utils/games'

import { saveUserSettings, getUserSettings } from '@db/helpers'

export default function Settings() {
  const { user } = useAuth()
  const [tagsSelected, setTagsSelected] = useState([])
  const [notifications, setNotifications] = useState(false)

  const fetchSettings = useCallback(async () => {
    if (user) {
      const settingsKey = 'settings'
      const savedSettings = localStorage.getItem(settingsKey)

      if (savedSettings) {
        const settings = JSON.parse(savedSettings)
        setTagsSelected(namesArrayToTagsArray(settings.tagsSelected || []))
        setNotifications(settings.notifications || false)
      } else {
        const settings = await getUserSettings(user.uid)
        if (settings) {
          const newSettings = {
            tagsSelected: namesArrayToTagsArray(settings.favTags || []),
            notifications: settings.notifications || false,
          }
          localStorage.setItem(settingsKey, JSON.stringify(newSettings))
          setTagsSelected(newSettings.tagsSelected)
          setNotifications(newSettings.notifications)
        }
      }
    }
  }, [user])

  useEffect(() => {
    fetchSettings()
  }, [fetchSettings])

  const handleSaveSettings = async () => {
    if (user) {
      const settingsKey = 'settings'
      const newSettings = {
        tagsSelected: tagsArrayToNamesArray(tagsSelected),
        notifications,
      }

      saveUserSettings(user.uid, newSettings)
      localStorage.setItem(settingsKey, JSON.stringify(newSettings))
    }
  }

  return (
    <Page>
      <Header title="Paramètres" />

      <h4>Mes catégories préférées</h4>
      <CheckboxGroup
        className="gap-1"
        label=""
        orientation="horizontal"
        value={tagsSelected}
        onChange={setTagsSelected}
      >
        {gamesTags.map((el) => (
          <MyCheckbox key={el.id} value={el}>
            {el.title}
          </MyCheckbox>
        ))}
      </CheckboxGroup>

      <Spacer size={1} />
      <div className="fbc">
        <h4>Activer les notifications</h4>
        <Switch isSelected={notifications} onValueChange={setNotifications} />
      </div>

      <Spacer />

      <MyButton onClick={handleSaveSettings} classNames="bg-color4">
        Sauvegarder
      </MyButton>
    </Page>
  )
}
