import PropTypes from 'prop-types'

export default function ToolCard({ tool }) {
  return (
    <div key={tool.id} className="cursor-pointer">
      <div
        className="relative flex-row rounded-md sm:flex-col"
        style={{
          backgroundImage: `url(/imgs/tools/${tool.component}.jpg)`,
          backgroundSize: 'cover',
          backgroundPosition: 'center',
          backgroundColor: 'rgba(0, 0, 0, 0.2)',
        }}
      >
        <div className="relative flex h-full min-h-[170px] w-full flex-col items-start justify-between gap-2 overflow-hidden p-2 md:min-h-[300px]">
          <div className="absolute bottom-0 left-0 right-0 top-0 z-10 bg-white/15 hover:bg-white/0"></div>
        </div>
      </div>

      <h3 className="py-1">{tool.title}</h3>
      <p className="py-1">{tool.description}</p>
    </div>
  )
}

ToolCard.propTypes = {
  tool: PropTypes.object,
}
