import { useParams, useNavigate } from 'react-router-dom'
import DynamicToolComponentLoader from '@components/DynamicToolComponentLoader'
import ToolCard from '@pages/tools/ToolCard'

import Page from '@layout/Page'

import { useEffect, useState } from 'react'

import { gamesTools } from '@datas/gamesTools'
import { getToolByID } from '@utils/datasHelpers'
import Header from '@ui/Header'

export default function Tools() {
  const navigate = useNavigate()
  const { toolId } = useParams()
  const [tool, setTool] = useState(null)

  useEffect(() => {
    if (toolId) {
      const fetchedTool = getToolByID(parseInt(toolId, 10))
      setTool(fetchedTool)
    } else {
      setTool(null)
    }
  }, [toolId])

  return (
    <Page>
      {tool ? <Header title={tool.title} /> : <Header title="Outils" noreturn />}

      <div className="flex w-full justify-center gap-3">
        {!toolId ? (
          <div className="grid w-full grid-cols-[repeat(auto-fill,minmax(250px,1fr))] gap-3">
            {gamesTools.map((tool) => (
              <div key={tool.id} onClick={() => navigate(`/tools/${tool.id}`)}>
                <ToolCard tool={tool} />
              </div>
            ))}
          </div>
        ) : (
          <div className="w-full max-w-[900px] flex-col gap-3 text-center">
            <DynamicToolComponentLoader toolId={parseInt(toolId, 10)} />
          </div>
        )}
      </div>
    </Page>
  )
}
