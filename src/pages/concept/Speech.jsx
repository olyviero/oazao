import React, { useEffect, useRef } from 'react'
import gsap from 'gsap'
import { CustomEase } from 'gsap/CustomEase'

gsap.registerPlugin(CustomEase)
const speechAnimEase = CustomEase.create('custom', 'M0,0 C0,1 0,1 0.5,1 1,1 1,1 1,0 ')

const Speech = ({ speech = {}, index }) => {
  // Définir des valeurs par défaut pour les propriétés
  const { opacity = 0, left = 50, top = 50, duration = 1, name = 'default-name', txt = 'Default text' } = speech

  const speechRef = useRef()

  useEffect(() => {
    gsap.set(speechRef.current, {
      xPercent: -50,
      yPercent: -50,
      left: `${left}%`,
      top: `${top}%`,
      duration,
      opacity,
      ease: speechAnimEase,
    })
  }, [left, top, duration, opacity])

  return (
    <div ref={speechRef} className={`speech-container speech-${name}`} data-duration={duration}>
      <div className={`speech`}>{txt}</div>
    </div>
  )
}

export default Speech
