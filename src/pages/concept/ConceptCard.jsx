import React, { useEffect, useRef } from 'react'
import gsap from 'gsap'

const ConceptCard = ({ x, y, frontFace, backFace, classNames, position }) => {
  const cardRef = useRef()

  useEffect(() => {
    if (position) {
      gsap.set(cardRef.current, position)
    }
  }, [position])

  const cardStyle = {
    zIndex: `${x}${y}`,
    position: 'absolute',
    transform: 'translate(-50%, -50%)',
  }

  return (
    <div
      ref={cardRef}
      data-x={x}
      data-y={y}
      className={`concept-card card-${x}-${y} ${classNames.join(' ')}`}
      style={cardStyle}
    >
      {frontFace && (
        <div
          className="card-face card-front"
          style={{ backgroundImage: `url('/imgs/cards-front-border/${x}.${y}.jpg')` }}
        ></div>
      )}
      {backFace && (
        <div
          className="card-face card-back"
          style={{ backgroundImage: `url('/imgs/cards-back/back-black.jpg')` }}
        ></div>
      )}
    </div>
  )
}

export default ConceptCard
