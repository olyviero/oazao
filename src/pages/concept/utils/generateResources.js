/**
 * Generate Resources
 * @returns Array
 */
export function generateResources() {
  let resources = []

  // Oazao Cards front
  for (let x = 0; x <= 15; x++) {
    for (let y = 0; y <= 9; y++) {
      resources.push(`/imgs/cards-front-border/${x}.${y}.jpg`)
    }
  }

  // Oazao Cards back
  resources.push(`/imgs/cards-back/back-black.jpg`)

  // Old Cards front
  resources.push(`/imgs/old-cards/old-2.jpg`)
  resources.push(`/imgs/old-cards/old-3.jpg`)
  resources.push(`/imgs/old-cards/old-6.jpg`)
  resources.push(`/imgs/old-cards/old-8.jpg`)
  resources.push(`/imgs/old-cards/old-10.jpg`)
  resources.push(`/imgs/old-cards/old-a.jpg`)
  resources.push(`/imgs/old-cards/old-r.jpg`)
  resources.push(`/imgs/old-cards/old-v.jpg`)

  // Old Cards front with variations that stay on scroll
  resources.push(`/imgs/old-cards/old-v-0.jpg`)
  resources.push(`/imgs/old-cards/old-d-0.jpg`)
  resources.push(`/imgs/old-cards/old-r-0.jpg`)
  resources.push(`/imgs/old-cards/old-a-0.jpg`)

  // Their variations
  resources.push(`/imgs/old-cards/old-v-1.jpg`)
  resources.push(`/imgs/old-cards/old-v-2.jpg`)
  resources.push(`/imgs/old-cards/old-v-3.jpg`)

  resources.push(`/imgs/old-cards/old-d-1.jpg`)
  resources.push(`/imgs/old-cards/old-d-2.jpg`)
  resources.push(`/imgs/old-cards/old-d-3.jpg`)

  resources.push(`/imgs/old-cards/old-r-1.jpg`)
  resources.push(`/imgs/old-cards/old-r-2.jpg`)
  resources.push(`/imgs/old-cards/old-r-3.jpg`)

  resources.push(`/imgs/old-cards/old-a-1.jpg`)
  resources.push(`/imgs/old-cards/old-a-2.jpg`)
  resources.push(`/imgs/old-cards/old-a-3.jpg`)

  return resources
}
