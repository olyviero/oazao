import { useEffect, useLayoutEffect, useMemo, useState } from 'react'
import gsap from 'gsap'
import { ScrollTrigger } from 'gsap/ScrollTrigger'

import { Splide, SplideSlide, SplideTrack } from '@splidejs/react-splide'
import '@splidejs/react-splide/css'

gsap.registerPlugin(ScrollTrigger)

export default function Concept() {
  const [msIndex, setMsIndex] = useState(0)

  const handleMsSliderMove = (newIndex, prevIndex, destIndex) => {
    setMsIndex(newIndex.index)
  }
  useEffect(() => {}, [msIndex])

  return (
    <div
      className="mx-auto flex w-full flex-col gap-3 p-3 text-center"
      style={{
        backgroundImage: 'url(/imgs/concept/bg-top.jpg)',
        backgroundSize: '100% auto',
        backgroundPosition: 'top center',
        backgroundRepeat: 'no-repeat',
      }}
    >
      <div className="fccc h-[95vh] gap-20">
        <h1 className="text-[90px] md:text-[160px]">OAZAO</h1>
        <h3 className="text-[42px] uppercase">
          le jeu de carte
          <br />
          <span className="text-primary">no limit !</span>
        </h3>
      </div>

      <div className="fccc mb-10 gap-[200px]">
        <div className="fccc mx-auto w-full max-w-[900px] gap-20">
          <h3 className="text-[42px] uppercase">
            <span className="text-primary">160</span> Cartes réparties en <span className="text-primary">10</span>{' '}
            Couleurs
          </h3>
          <img
            className="w-full"
            src="/imgs/concept/allkings.jpg"
            srcSet="/imgs/concept/allkings-m.jpg 768w, /imgs/concept/allkings.jpg 1980w"
            alt="les A0 rois de oazao"
          />
        </div>

        <div className="fccc mx-auto w-full max-w-[900px] gap-20">
          <div className="fccc gap-5">
            <h3 className="text-[42px] uppercase">
              Chacune de l’<span className="text-primary">as</span> au <span className="text-primary">roi</span>
            </h3>
            <h3 className="max-w-[600px] text-[18px]">
              auxquelles on ajoute un <span className="text-primary">zéro</span>, un{' '}
              <span className="text-primary">joker</span> et un <span className="text-primary">elixir</span>
            </h3>
          </div>
          <img
            className="w-full"
            src="/imgs/concept/straight.jpg"
            srcSet="/imgs/concept/straight-m.jpg 768w, /imgs/concept/straight.jpg 1980w"
            alt="les A0 rois de oazao"
          />
        </div>

        <div className="fccc mx-auto max-w-[900px] gap-20">
          <div className="fccc gap-5">
            <h3 className="text-[42px] uppercase">
              <span className="text-primary">2,5 jeux</span> de cartes classiques
            </h3>
            <h3 className="max-w-[600px] text-[18px]">
              Choisis 4 couleurs selon ton humeur et joue à tous tes jeux de 54 cartes préférés
            </h3>
          </div>
          <img
            className="w-full"
            src="/imgs/concept/decks4.jpg"
            srcSet="/imgs/concept/decks4-m.jpg 768w, /imgs/concept/decks4.jpg 1980w"
            alt="les A0 rois de oazao"
          />
        </div>

        <div className="fccc mx-auto max-w-[900px] gap-20">
          <div className="fccc gap-5">
            <h3 className="text-[42px] uppercase">
              Retrouve d'autres <span className="text-primary">genres</span> de jeux
            </h3>
            <h3 className="max-w-[600px] text-[18px]">
              UNO : Assemble des couleurs similaires pour doubler certaines valeurs et assigne des effets à d'autres
              cartes
            </h3>
          </div>
          <img
            className="w-full"
            src="/imgs/concept/uno.jpg"
            srcSet="/imgs/concept/uno-m.jpg 768w, /imgs/concept/uno.jpg 1980w"
            alt="les A0 rois de oazao"
          />
          <img className="" src="/imgs/concept/unoeffects.jpg" alt="" />
        </div>

        <div className="fccc mx-auto max-w-[900px] gap-20">
          <div className="fccc gap-5">
            <h3 className="text-[42px] uppercase">
              Utilise les comme <span className="text-primary">suite</span>
            </h3>
            <h3 className="max-w-[600px] text-[18px]">
              Les cartes vont de 0 à 159, pratique pour de nombreux jeux (6 qui prend, the game, non merci, ... )
            </h3>
          </div>
          <div className="fcc gap-5">
            <div className="fcc text-4xl">...</div>
            <img className="" src="/imgs/concept/bigstraight.jpg" alt="" />
            <div className="fcc text-4xl">...</div>
          </div>
        </div>

        <div className="fccc mx-auto max-w-[900px] gap-20">
          <div className="fccc gap-5">
            <h3 className="text-[42px] uppercase">
              utilise les mots comme <span className="text-primary">thème</span>
            </h3>
            <h3 className="max-w-[600px] text-[18px]">
              Que ce soit pour TTMC, Time's up, Codenames ou juste pour tirer ou mélanger des thèmes au hasard
            </h3>
          </div>
          <div className="fcc gap-5">
            <div className="fcc text-4xl">...</div>
            <img className="" src="/imgs/concept/themes.jpg" alt="" />
            <div className="fcc text-4xl">...</div>
          </div>
        </div>

        <div className="fccc mx-auto max-w-[900px] gap-20">
          <div className="fccc gap-5">
            <h3 className="text-[42px] uppercase">
              une application mobile <span className="text-primary">multi outils</span>
            </h3>
          </div>
          <div className="mx-auto grid gap-10 md:grid-cols-2">
            <div className="flex justify-center md:justify-end">
              <img className="" src="/imgs/concept/phone.jpg" alt="" />
            </div>
            <div className="flex flex-col justify-start text-left">
              <h3>Moteur de recherche</h3>
              <p>
                Trouve un jeu rapidement avec les filtres avancés : Nombre de joueurs, durée de partie, catégories de
                jeux, notes par la communauté, complexité, ...
              </p>
              <h3>Catalogue de règles et de variantes</h3>
              <p>
                Découvre les règles mais aussi les variantes proposées par la communauté. Propose les tiennes et change
                le game !
              </p>

              <h3>Assistants de jeux</h3>
              <p>Utilises les assistants pour donner une autre dimension à certains jeux</p>

              <h3>Les indispensables</h3>
              <p>Compteurs de points personnalisables, générateurs aléatoires en tout genre, tirage de joueur, ...</p>

              <h3>Profil</h3>
              <p>
                Suis tes jeux préférés, propsssose les tiens, échange avec les autres et deviens un créateur célèbre !
              </p>
            </div>
          </div>
        </div>

        <div className="fccc mx-auto max-w-[900px] gap-20">
          <div className="fccc gap-5">
            <h3 className="text-[42px] uppercase">Système majeur</h3>
            <h3 className="max-w-[600px] text-[18px]">Les origines d'une idée</h3>
            <p className="max-w-[600px] text-color8">
              Tout d'abord, saches que ce système qui est à l'origine du concept n'est absolument pas indispensable pour
              profiter pleinenement du jeu mais peut apporter une autre dimension à tes expériences de jeu.
            </p>
            <p className="max-w-[600px]">
              Il donne du sens au nombres abstraits en les transformant en idées intelligibles : Chaque chiffres devient
              un son de consonne auxquel on ajoute ensuite librement des voyelles ou consonnes muettes pour créer des
              mots.
            </p>
          </div>

          <div className="fccc gap-5">
            <div className="fcc w-full border-1 border-lightgray">
              <div
                className={`bg-color0 w-full transition-opacity text-color9 ${
                  msIndex >= 2 ? 'opacity-1' : 'opacity-0'
                }`}
              >
                <div className="nb">0</div>
                <div className="letter">S</div>
              </div>
              <div className={`bg-color1 w-full transition-opacity ${msIndex >= 3 ? 'opacity-1' : 'opacity-0'}`}>
                <div className="nb">1</div>
                <div className="letter">T</div>
              </div>
              <div className={`bg-color2 w-full transition-opacity ${msIndex >= 1 ? 'opacity-1' : 'opacity-0'}`}>
                <div className="nb">2</div>
                <div className="letter">N</div>
              </div>
              <div className={`bg-color3 w-full transition-opacity ${msIndex >= 3 ? 'opacity-1' : 'opacity-0'}`}>
                <div className="nb">3</div>
                <div className="letter">M</div>
              </div>
              <div className={`bg-color4 w-full transition-opacity ${msIndex >= 0 ? 'opacity-1' : 'opacity-0'}`}>
                <div className="nb">4</div>
                <div className="letter">R</div>
              </div>
              <div className={`bg-color5 w-full transition-opacity ${msIndex >= 1 ? 'opacity-1' : 'opacity-0'}`}>
                <div className="nb">5</div>
                <div className="letter">L</div>
              </div>
              <div className={`bg-color6 w-full transition-opacity ${msIndex >= 3 ? 'opacity-1' : 'opacity-0'}`}>
                <div className="nb">6</div>
                <div className="letter">CH</div>
              </div>
              <div
                className={`bg-color7 w-full transition-opacity text-color9 ${
                  msIndex >= 0 ? 'opacity-1' : 'opacity-0'
                }`}
              >
                <div className="nb">7</div>
                <div className="letter">K</div>
              </div>
              <div className={`bg-color8 w-full transition-opacity ${msIndex >= 2 ? 'opacity-1' : 'opacity-0'}`}>
                <div className="nb">8</div>
                <div className="letter">V</div>
              </div>
              <div className={`bg-color9 w-full transition-opacity ${msIndex >= 3 ? 'opacity-1' : 'opacity-0'}`}>
                <div className="nb">9</div>
                <div className="letter">P</div>
              </div>
            </div>

            <Splide
              options={{
                // type: 'loop',
                // cover: true,
                height: 'auto',
                arrows: true,
                pagination: false,
                // autoplay: true,
                // interval: 5000,
              }}
              className="ms"
              hasTrack={false}
              aria-label="My Favorite Images"
              onMoved={(newIndex, prevIndex, destIndex) => handleMsSliderMove(newIndex, prevIndex, destIndex)}
            >
              <SplideTrack>
                <SplideSlide>
                  <h3>Par exemple</h3>
                  <img className="card-img" src="/imgs/cards-front-border/7.4.jpg" alt="Carte du coeur" />
                  <p className="text-[20px] font-bold">
                    <span className="text-color7">Le 7 correspond au son 'K'</span> et{' '}
                    <span className="text-color4">le 4 au son 'R'</span>
                  </p>
                  <p className="text-[20px]">
                    On ajoute les voyelles manquantes : <span className="text-color7">C</span>oeu
                    <span className="text-color4">R</span>
                  </p>
                </SplideSlide>

                <SplideSlide>
                  <h3>Il existe de jolis hasards</h3>
                  <img className="card-img" src="/imgs/cards-front-border/2.5.jpg" alt="Carte de noël" />
                  <p className="text-[20px] font-bold">
                    <span className="text-color2">Le 2 correspond au son 'N'</span> et{' '}
                    <span className="text-color5">le 5 au son 'L'</span>
                  </p>
                  <p className="text-[20px]">
                    Le 25 peut donc se lire : <span className="text-color2">N</span>oë
                    <span className="text-color5">L</span>
                  </p>
                </SplideSlide>

                <SplideSlide>
                  <h3>Un dernier exemple</h3>
                  <img className="card-img" src="/imgs/cards-front-border/0.8.jpg" alt="Carte de savon" />
                  <p className="text-[20px] font-bold">
                    <span className="text-color0">Le 0 correspond au son 'S'</span> et{' '}
                    <span className="text-color8">le 8 au son 'V'</span>
                  </p>
                  <p className="text-[20px]">
                    Le 08 peut donc se lire : <span className="text-color0">S</span>a
                    <span className="text-color8">V</span>on
                  </p>
                </SplideSlide>

                <SplideSlide>
                  <p className="text-[18px]">
                    Voici le tableau complet, on ajoute d'autres sons afin de pouvoir utiliser tous les mots français.
                    Remarquez que ces sons se produisent au même endroit dans la bouche
                  </p>
                  <div className="table">
                    <div className="line line-title font-bold text-color8">
                      <div className="number"></div>
                      <div className="letters">son(s)</div>
                      <div className="helper">aide mémoire</div>
                    </div>
                    <div className="line text-[10px]">
                      <div className="number">0</div>
                      <div className="letters">Z, S</div>
                      <div className="helper">Comme dans "Zéro"</div>
                    </div>
                    <div className="line text-[10px]">
                      <div className="number">1</div>
                      <div className="letters">T, D</div>
                      <div className="helper">Un pied</div>
                    </div>
                    <div className="line text-[10px]">
                      <div className="number">2</div>
                      <div className="letters">N</div>
                      <div className="helper">2 pieds</div>
                    </div>
                    <div className="line text-[10px]">
                      <div className="number">3</div>
                      <div className="letters">M</div>
                      <div className="helper">3 pieds</div>
                    </div>
                    <div className="line text-[10px]">
                      <div className="number">4</div>
                      <div className="letters">R</div>
                      <div className="helper">4 et R sont presque symétriques</div>
                    </div>
                    <div className="line text-[10px]">
                      <div className="number">5</div>
                      <div className="letters">L</div>
                      <div className="helper">Chiffre romain L = 50</div>
                    </div>
                    <div className="line text-[10px]">
                      <div className="number">6</div>
                      <div className="letters">J, Ch, G (doux)</div>
                      <div className="helper">6 et G se ressemblent</div>
                    </div>
                    <div className="line text-[10px]">
                      <div className="number">7</div>
                      <div className="letters">K, Q, G (dur)</div>
                      <div className="helper">Le K est formé de deux 7</div>
                    </div>
                    <div className="line text-[10px]">
                      <div className="number">8</div>
                      <div className="letters">F, V</div>
                      <div className="helper">f manuscrit forme un 8</div>
                    </div>
                    <div className="line text-[10px]">
                      <div className="number">9</div>
                      <div className="letters">P, B</div>
                      <div className="helper">9 et P sont symétriques</div>
                    </div>
                  </div>
                </SplideSlide>
              </SplideTrack>

              {/* <div className="splide__arrows" /> */}
            </Splide>
          </div>
        </div>
      </div>
    </div>
  )
}
