import { gsap } from 'gsap'
import { generateAnimSpeech, generateRandomAngle } from '@pages/concept/utils/animHelpers'

export const timelineIntro = () => {
  return gsap
    .timeline()
    .add(generateAnimSpeech('.speech-history'), '>')
    .to(
      '.old-card.go-away',
      {
        y: -900,
        duration: 1,
        rotationY: generateRandomAngle(2),
        stagger: { amount: 0.1, ease: 'power2.inOut', from: 'random' },
      },
      '<'
    )
    .to('#scroll-indicator', { opacity: 0, duration: 0.2 }, '<')
    .add(generateAnimSpeech('.speech-no-evolution', 0.3), '<+=.3')
    .add(generateAnimSpeech('.speech-until-today', 0.3), '<+=.6')
    .add(generateAnimSpeech('.speech-new-concept', 1.4), '<+=.9')
}
