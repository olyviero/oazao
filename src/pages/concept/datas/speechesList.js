export const speechesList = [
  {
    opacity: 1,
    name: 'history',
    txt: "Le premier jeu de 52 cartes tel que nous le connaissons aujourd'hui est apparu en Europe au cours du 15ème siècle",
  },
  {
    name: 'no-evolution',
    txt: "Depuis, hormis des variations sur le design, le jeu n'a jamais évolué...",
  },
  {
    name: 'until-today',
    txt: "... jusqu'à aujourd'hui.",
  },
  {
    name: 'new-concept',
    txt: 'Voici Oazao',
    top: 5,
  },
  {
    name: 'cards-160',
    txt: '160 cartes réparties en 10 couleurs',
  },
  {
    name: 'card-range',
    txt: "De l'As au Roi",
  },
  {
    name: 'additional-cards',
    txt: 'Auxquelles ont ajoute à chacune un Zero, un Joker et un Elixir pour plus de possibilités',
  },
  {
    name: 'multi-usage',
    txt: "Les cartes peuvent s'utiliser de plusieurs façon",
  },
  {
    name: 'classic-deck',
    txt: 'Pour un jeu de 52 cartes classique, il suffit de prendre 4 couleurs au choix',
  },
  {
    name: 'keep-0-to-9',
    txt: 'Vous pourriez ne garder que les 0 à 9',
  },
  {
    name: 'double-some',
    txt: 'Les doubler en rassemblant certaines couleurs',
  },
  {
    name: 'special-effects',
    txt: "Assigner des effets à d'autres",
  },
  {
    name: 'read-straight',
    txt: 'Elles se lisent aussi sous forme de suite de 00 à 160',
    top: 20,
  },
  {
    name: 'use-words',
    txt: 'Ou peuvent être utilisées comme thèmes dans vos jeux',
    top: 20,
  },
]
