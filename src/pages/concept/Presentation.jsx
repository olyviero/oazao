import { useEffect, useLayoutEffect, useMemo, useState } from 'react'
import gsap from 'gsap'
import { ScrollTrigger } from 'gsap/ScrollTrigger'

// Hooks
import { usePreloadImages } from '@hooks/usePreloadImages'

// Timelines
import { timelineIntro } from '@pages/concept/timelines/intro'
import { timelineCardsX4 } from '@pages/concept/timelines/cardsX4'
import { timelineConcept } from '@pages/concept/timelines/concept'
import { timeline52toUno } from '@pages/concept/timelines/cardsX52'
import { timelineStraight } from '@pages/concept/timelines/straight'
import { timelineWords } from '@pages/concept/timelines/words'

// Components
import OldCard from '@pages/concept/OldCard'
import ConceptCard from '@pages/concept/ConceptCard'
import Speech from '@pages/concept/Speech'
import Effect from '@pages/concept/Effect'

// Utils
import {
  generateCardClassesFromXY,
  generateRandomAngle,
  generateRandomPositionOnCircle,
} from '@pages/concept/utils/animHelpers'
import { generateResources } from '@pages/concept/utils/generateResources'

// Datas
import { speechesList } from '@pages/concept/datas/speechesList'

gsap.registerPlugin(ScrollTrigger)

export default function Concept() {
  const resources = useMemo(() => generateResources(), [])
  const loading = usePreloadImages(resources)

  const [oldCards, setOldCards] = useState([])
  const [conceptCards, setConceptCards] = useState([])
  const [speeches, setSpeeches] = useState([])
  const [effects, setEffects] = useState([])
  const [cardsReady, setCardsReady] = useState(false)

  useEffect(() => {
    if (!loading) {
      const createEverything = () => {
        // -------------------------------------------------------------------------------
        // Old Cards
        // -------------------------------------------------------------------------------
        const oldCardsArray = [
          {
            imageFront: '/imgs/old-cards/old-2.jpg',
            classNames: ['go-away', 'old-2'],
            position: { left: '2%', top: '7%', rotateZ: 30 },
          },
          {
            imageFront: '/imgs/old-cards/old-3.jpg',
            classNames: ['go-away', 'old-3'],
            position: { left: '5%', top: '7%', rotateZ: 45 },
          },
          {
            imageFront: '/imgs/old-cards/old-6.jpg',
            classNames: ['go-away', 'old-6'],
            position: { left: '99%', top: '45%', rotateZ: -125 },
          },
          {
            imageFront: '/imgs/old-cards/old-8.jpg',
            classNames: ['go-away', 'old-8'],
            position: { left: '22%', top: '8%', rotateZ: 36 },
          },
          {
            imageFront: '/imgs/old-cards/old-10.jpg',
            classNames: ['go-away', 'old-10'],
            position: { left: '45%', top: '9%', rotateZ: -56 },
          },
          {
            imageFront: '/imgs/old-cards/old-a.jpg',
            classNames: ['go-away', 'old-a'],
            position: { left: '54%', top: '10%', rotateZ: -15 },
          },
          {
            imageFront: '/imgs/old-cards/old-r.jpg',
            classNames: ['go-away', 'old-r'],
            position: { left: '94%', top: '12%', rotateZ: 30 },
          },
          {
            imageFront: '/imgs/old-cards/old-v.jpg',
            classNames: ['go-away', 'old-v'],
            position: { left: '80%', top: '9%', rotateZ: 75 },
          },
          {
            imageFront: '/imgs/old-cards/old-v-0.jpg',
            classNames: ['stay', 'stay-0'],
            position: { left: '9%', top: '30%', rotateZ: -25 },
          },
          {
            imageFront: '/imgs/old-cards/old-d-0.jpg',
            classNames: ['stay', 'stay-1'],
            position: { left: '32%', top: '12%', rotateZ: 20 },
          },
          {
            imageFront: '/imgs/old-cards/old-r-0.jpg',
            classNames: ['stay', 'stay-2'],
            position: { left: '68%', top: '10%', rotateZ: -30 },
          },
          {
            imageFront: '/imgs/old-cards/old-a-0.jpg',
            classNames: ['stay', 'stay-3'],
            position: { left: '94%', top: '30%', rotateZ: 20 },
          },
        ]
        setOldCards(oldCardsArray)

        // -------------------------------------------------------------------------------
        // Concept Cards
        // -------------------------------------------------------------------------------
        const createAllConceptCards = () => {
          const conceptCardsArray = []

          for (let y = 0; y <= 9; y++) {
            for (let x = 0; x <= 15; x++) {
              const cardClasses = generateCardClassesFromXY(x, y)
              const position = generateRandomPositionOnCircle(2000)
              conceptCardsArray.push({
                x,
                y,
                frontFace: true,
                backFace: true,
                classNames: cardClasses,
                position: {
                  left: `calc(50% + ${position.x}px)`,
                  top: `calc(50% + ${position.y}px)`,
                  scale: 0.8,
                  rotateZ: generateRandomAngle(5),
                  rotateY: -360,
                },
              })
            }
          }

          setConceptCards(conceptCardsArray)
        }
        createAllConceptCards()

        // -------------------------------------------------------------------------------
        // Speeches
        // -------------------------------------------------------------------------------
        setSpeeches(speechesList)

        // -------------------------------------------------------------------------------
        // Uno Effects
        // -------------------------------------------------------------------------------
        const effectArray = ['+2', '+4', 'Change de sens', 'Passe ton tour', 'Change de couleur']
        setEffects(effectArray)

        // -------------------------------------------------------------------------------
        // Ready
        // -------------------------------------------------------------------------------

        setCardsReady(true)
      }

      createEverything()
    }
  }, [loading])

  useLayoutEffect(() => {
    if (cardsReady) {
      let ctx = gsap.context(() => {
        let mainTimeline = gsap.timeline({
          scrollTrigger: {
            trigger: '#page-container',
            start: 'top top',
            end: '+=20000',
            // markers: true,
            scrub: 1,
            pin: '#page-container',
          },
        })

        mainTimeline
          .add(timelineIntro())
          .add(timelineCardsX4(), '<')
          .add(timelineConcept(), '>-=0.9')
          .add(timeline52toUno(), '>')
          .add(timelineStraight(), '>')
          .add(timelineWords(), '>-=0.7')
      })

      return () => ctx.revert()
    }
  }, [cardsReady])

  return (
    <>
      {loading ? (
        <div id="loader" className="section flex h-[90vh] flex-col items-center justify-center px-5 text-center">
          <h2>Chargement de l'expérience immersive</h2>
          <p>Ce ne sera pas long</p>
        </div>
      ) : (
        <>
          <div id="page-container">
            <div id="gradient-bg"></div>
            <div id="mat-bg"></div>
            <div id="overlays">
              <div className="overlay overlay-double"></div>
            </div>
            <div id="cards-container">
              {oldCards.map((card, index) => (
                <OldCard
                  key={index}
                  imageFront={card.imageFront}
                  classNames={card.classNames}
                  position={card.position}
                />
              ))}
              {conceptCards.map((card, index) => (
                <ConceptCard
                  key={index}
                  x={card.x}
                  y={card.y}
                  frontFace={card.frontFace}
                  backFace={card.backFace}
                  classNames={card.classNames}
                  position={card.position}
                />
              ))}
            </div>
            <div id="speech-container">
              {speeches.map((speech, index) => (
                <Speech key={index} speech={speech} index={index} />
              ))}
            </div>
            <div id="effects-container">
              {effects.map((effect, index) => (
                <Effect key={index} effect={effect} index={index} />
              ))}
            </div>
          </div>
          <div id="scroll-indicator">
            <div className="mouse"></div>
            <div className="arrow"></div>
          </div>
        </>
      )}
    </>
  )
}
