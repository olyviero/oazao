import React, { useEffect, useRef } from 'react'
import gsap from 'gsap'

const OldCard = ({ imageFront, classNames, position }) => {
  const cardRef = useRef()

  useEffect(() => {
    if (position) {
      gsap.set(cardRef.current, position)
    }
  }, [position])

  const cardStyle = {
    transform: 'translate(-50%, -50%)',
    backgroundImage: `url(${imageFront})`,
  }

  return <div ref={cardRef} className={`old-card ${classNames.join(' ')}`} style={cardStyle}></div>
}

export default OldCard
