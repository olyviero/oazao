import React, { useEffect, useRef } from 'react'
import gsap from 'gsap'

const Effect = ({ effect, index }) => {
  const effectRef = useRef()

  useEffect(() => {
    gsap.set(effectRef.current, {
      xPercent: -50,
      yPercent: -50,
      left: '50%',
      top: '150%',
    })
  }, [])

  return (
    <div
      ref={effectRef}
      className={`uno-effect uno-effect-${index} ${index === 0 || index === 1 ? 'bigger' : ''}`}
      data-effect={index}
    >
      {effect}
    </div>
  )
}

export default Effect
