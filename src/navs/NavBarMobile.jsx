import { useNavigate, NavLink } from 'react-router-dom'

import {
  Navbar,
  NavbarBrand,
  NavbarMenuToggle,
  NavbarMenuItem,
  NavbarMenu,
  NavbarContent,
  NavbarItem,
  Link,
  Button,
} from '@nextui-org/react'

import logo from '../assets/logo.png'

import { useAuth } from '@contexts/AuthContext'
import { customAlert } from '@ui/customAlert'
import { doSignOut } from '../firebase/auth'
import { useState } from 'react'

import { GiHouse } from 'react-icons/gi'
import { GiArchiveResearch } from 'react-icons/gi'
import { GiHelp } from 'react-icons/gi'
import { GiToolbox } from 'react-icons/gi'
import { MdFactory } from 'react-icons/md'
import { IoMdHappy } from 'react-icons/io'

export default function NavBarMobile() {
  const navigate = useNavigate()
  const [isMenuOpen, setIsMenuOpen] = useState(false)
  const { user } = useAuth()

  const handleSignOut = () => {
    customAlert('Déconnexion ?', () =>
      doSignOut().then(() => {
        navigate('/')
      })
    )
  }

  const handleNavbarClick = () => {
    setIsMenuOpen(!isMenuOpen)
  }
  return (
    <Navbar
      isBordered
      isMenuOpen={isMenuOpen}
      onMenuOpenChange={setIsMenuOpen}
      onClick={handleNavbarClick}
      className="sm:hidden"
    >
      <NavbarContent className="justify-betwen flex">
        <NavbarMenuToggle className="text-black" aria-label={isMenuOpen ? 'Close menu' : 'Open menu'} />
      </NavbarContent>
      <img src={logo} alt="logo oazao" className="max-w-20" />

      <NavbarMenu className="overflow-scroll">
        <NavbarMenuItem className="flex flex-col gap-3 py-3">
          {!user && (
            <>
              <NavLink
                className="navlink-mobile text-md flex w-full flex-col items-center justify-center border-2 border-secondary/40 bg-color9/50 p-1 text-center font-bold !text-color0 hover:bg-color9/100"
                to="/login"
              >
                {/* <GiHouse size={20} />  */}
                Se connecter
              </NavLink>
              <NavLink
                className="navlink-mobile text-md flex w-full flex-col items-center justify-center border-2 border-secondary/40 bg-color9/50 p-1 text-center font-bold !text-color0 hover:bg-color9/100"
                to="/register"
              >
                {/* <GiHouse size={20} />  */}
                S'enregistrer
              </NavLink>
            </>
          )}
          {user && (
            <>
              <NavLink
                className="navlink-mobile text-md flex w-full flex-col items-center justify-center border-2 border-secondary/40 bg-color9/50 p-1 text-center font-bold !text-color0 hover:bg-color9/100"
                to="/home"
              >
                {/* <GiHouse size={20} />  */}
                Accueil
              </NavLink>

              <NavLink
                className="navlink-mobile text-md flex w-full flex-col items-center justify-center border-2 border-secondary/40 bg-color9/50 p-1 text-center font-bold !text-color0 hover:bg-color9/100"
                to="/presentation"
              >
                {/* <GiHelp size={20} />  */}
                Présentation
              </NavLink>

              <NavLink
                className="navlink-mobile text-md flex w-full flex-col items-center justify-center border-2 border-secondary/40 bg-color9/50 p-1 text-center font-bold !text-color0 hover:bg-color9/100"
                to="/concept"
              >
                {/* <GiHelp size={20} />  */}
                Concept
              </NavLink>

              <NavLink
                className="navlink-mobile text-md flex w-full flex-col items-center justify-center border-2 border-secondary/40 bg-color9/50 p-1 text-center font-bold !text-color0 hover:bg-color9/100"
                to="/tools"
              >
                {/* <GiToolbox size={20} />  */}
                Outils
              </NavLink>

              <NavLink
                className="navlink-mobile text-md flex w-full flex-col items-center justify-center border-2 border-secondary/40 bg-color9/50 p-1 text-center font-bold !text-color0 hover:bg-color9/100"
                to="/profile"
              >
                {/* <IoMdHappy size={20} />  */}
                Profil
              </NavLink>

              <NavLink
                className="navlink-mobile logout text-md flex w-full flex-col items-center justify-center border-2 border-secondary/40 bg-color1/50 p-1 text-center font-bold !text-color0 hover:bg-color1/100"
                onClick={handleSignOut}
              >
                Déconnexion
              </NavLink>
            </>
          )}
        </NavbarMenuItem>
      </NavbarMenu>
    </Navbar>
  )
}
