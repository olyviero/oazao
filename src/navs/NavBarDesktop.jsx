import { useAuth } from '../contexts/AuthContext'
import { NavLink } from 'react-router-dom'
import logo from '../assets/logo.png'

export default function NavBarDesktop() {
  const { user, username } = useAuth()

  return (
    <nav id="navbar" className="hidden items-center justify-between gap-1 md:flex">
      <div className="logo w-full">
        <NavLink className="text-bold block w-full p-2 text-4xl" to="/home">
          <img src={logo} alt="logo oazao" className="mx-auto max-w-20" />
        </NavLink>
      </div>
      {!user && (
        <>
          <NavLink className="block w-full p-1" to="/login">
            Connexion
          </NavLink>

          <NavLink className="block w-full p-1" to="/register">
            Inscription
          </NavLink>
        </>
      )}

      {user && (
        <>
          <NavLink className="fcc w-full p-1 text-black" to="/home">
            Accueil
          </NavLink>

          <NavLink className="fcc w-full p-1 text-black" to="/presentation">
            Présentation
          </NavLink>

          <NavLink className="fcc w-full p-1 text-black" to="/concept">
            Concept
          </NavLink>

          <NavLink className="fcc w-full p-1 text-black" to="/tools">
            Outils
          </NavLink>

          <NavLink className="fcc w-full p-1 text-black" to="/profile">
            <span className="text-color0 hover:text-color4">{username || 'Profil'}</span>
          </NavLink>
        </>
      )}
    </nav>
  )
}
