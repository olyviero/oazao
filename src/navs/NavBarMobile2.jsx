import { useAuth } from '../contexts/AuthContext'
import { NavLink } from 'react-router-dom'

import { FaHome } from 'react-icons/fa'
import { FaPersonChalkboard } from 'react-icons/fa6'

import { FaToolbox } from 'react-icons/fa'
import { FaUser } from 'react-icons/fa6'
import { FaRegLightbulb } from 'react-icons/fa'

export default function NavBarMobile2() {
  const { user } = useAuth()

  return (
    <nav
      id="navbar"
      className="fixed bottom-0 left-0 z-50 flex h-16 w-full items-center justify-between gap-1 border-t border-lightgray bg-purewhite md:hidden"
    >
      {user && (
        <>
          <NavLink className="fcc w-full p-1 text-black" to="/home">
            <FaHome size={25} />
          </NavLink>

          <NavLink className="fcc w-full p-1 text-black" to="/presentation">
            <FaPersonChalkboard size={25} />
          </NavLink>

          <NavLink className="fcc w-full p-1 text-black" to="/concept">
            <FaRegLightbulb size={25} />
          </NavLink>

          <NavLink className="fcc w-full p-1 text-black" to="/tools">
            <FaToolbox size={25} />
          </NavLink>

          <NavLink className="fcc w-full p-1 text-black" to="/profile">
            <FaUser size={25} />
          </NavLink>
        </>
      )}
    </nav>
  )
}
