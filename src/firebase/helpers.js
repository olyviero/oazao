import {
  collection,
  query,
  where,
  getDocs,
  doc,
  getDoc,
  setDoc,
  addDoc,
  deleteDoc,
  updateDoc,
  increment,
} from 'firebase/firestore'
import { firestore } from '/src/firebase/firebase-config'

// =======================================================================================
// Users
// =======================================================================================
export const getUserSettings = async (userId) => {
  try {
    const docRef = doc(firestore, 'users', userId)
    const docSnap = await getDoc(docRef)
    if (docSnap.exists()) {
      const data = docSnap.data()
      return data
    } else {
      console.log("Les paramètres utilisateur n'ont pas été trouvés")
      return null
    }
  } catch (error) {
    console.error('Erreur lors de la récupération des paramètres utilisateur:', error)
    return null
  }
}

export const saveUserSettings = async (userId, settings) => {
  try {
    const docRef = doc(firestore, 'users', userId)
    await setDoc(docRef, settings, { merge: true })
  } catch (error) {
    console.error('Erreur lors de la sauvegarde des paramètres utilisateur:', error)
  }
}

export const getUserNameById = async (userId) => {
  try {
    const docRef = doc(firestore, 'users', userId)
    const docSnap = await getDoc(docRef)
    if (docSnap.exists()) {
      const data = docSnap.data()
      return data.username || null
    } else {
      console.log("L'utilisateur n'a pas été trouvé")
      return null
    }
  } catch (error) {
    console.error("Erreur lors de la récupération du nom de l'utilisateur:", error)
    return null
  }
}

// =======================================================================================
// Games
// =======================================================================================
export const getGame = async (gameId) => {
  try {
    const docRef = doc(firestore, 'games', gameId)
    const docSnap = await getDoc(docRef)
    if (docSnap.exists()) {
      return docSnap.data()
    } else {
      console.log("Le jeu n'a pas été trouvé")
      return null
    }
  } catch (error) {
    console.error('Erreur lors de la récupération du jeu:', error)
    return null
  }
}

export const createGame = async (gameData) => {
  try {
    const newGame = await addDoc(collection(firestore, 'games'), {
      ...gameData,
      ratingSum: 0,
      ratingCount: 0,
      createdAt: new Date(),
    })
    return newGame
  } catch (error) {
    console.error('Erreur lors de la création du jeu:', error)
    return null
  }
}

export const updateGame = async (id, gameData) => {
  try {
    const gameDocRef = doc(firestore, 'games', id)

    const gameDocSnap = await getDoc(gameDocRef)
    if (gameDocSnap.exists()) {
      const existingData = gameDocSnap.data()

      const updatedData = {
        ...existingData,
        ...gameData,
      }

      await updateDoc(gameDocRef, updatedData)
      console.log('Jeu mis à jour avec succès')
    } else {
      console.error("Le document n'existe pas")
    }
  } catch (error) {
    console.error('Erreur lors de la mise à jour du jeu:', error)
  }
}

export const deleteGame = async (gameId) => {
  try {
    await deleteDoc(doc(firestore, 'games', gameId))
    console.log('Jeu supprimé avec succès!')
  } catch (error) {
    console.error('Erreur lors de la suppression du jeu:', error)
  }
}

export const getAuthorOfGame = async (gameId) => {
  try {
    const docRef = doc(firestore, 'games', gameId)
    const docSnap = await getDoc(docRef)
    if (docSnap.exists()) {
      const gameData = docSnap.data()
      return gameData.author
    } else {
      console.log("Le jeu n'a pas été trouvé")
      return null
    }
  } catch (error) {
    console.error("Erreur lors de la récupération de l'auteur du jeu:", error)
    return null
  }
}

// export const getAllGames = async () => {
//   try {
//     const gamesCollection = collection(firestore, 'games')
//     const gamesSnapshot = await getDocs(gamesCollection)
//     return gamesSnapshot.docs.map((doc) => ({
//       id: doc.id,
//       ...doc.data(),
//     }))
//   } catch (error) {
//     console.error('Erreur lors de la récupération de tous les jeux:', error)
//     return []
//   }
// }

export const getAllGames = async () => {
  try {
    const gamesCollection = collection(firestore, 'games')
    const gamesSnapshot = await getDocs(gamesCollection)

    const games = await Promise.all(
      gamesSnapshot.docs.map(async (doc) => {
        const gameData = doc.data()
        const authorUsername = await getUserNameById(gameData.author) // Adding author username for simplicity to all games (will be in localstorage)
        return {
          id: doc.id,
          ...gameData,
          authorUsername: authorUsername || 'Inconnu',
        }
      })
    )

    return games
  } catch (error) {
    console.error('Erreur lors de la récupération de tous les jeux:', error)
    return []
  }
}

export const getGamesCreatedByUser = async (userId) => {
  try {
    const gamesRef = collection(firestore, 'games')
    const q = query(gamesRef, where('author', '==', userId))
    const gamesSnapshot = await getDocs(q)
    return gamesSnapshot.docs.map((doc) => ({
      id: doc.id,
      ...doc.data(),
    }))
  } catch (error) {
    console.error("Erreur lors de la récupération des jeux créés par l'utilisateur:", error)
    return []
  }
}

// =======================================================================================
// Ratings
// =======================================================================================
export const saveUserRating = async (userId, gameId, rating) => {
  try {
    const ratingsCollection = collection(firestore, 'ratings')
    const gameDocRef = doc(firestore, 'games', gameId)

    // Rechercher une note existante pour l'utilisateur et le jeu
    const q = query(ratingsCollection, where('userId', '==', userId), where('gameId', '==', gameId))
    const querySnapshot = await getDocs(q)

    if (querySnapshot.empty) {
      // Si aucune note existante, ajouter une nouvelle note
      await addDoc(ratingsCollection, {
        userId,
        gameId,
        rating,
        createdAt: new Date(),
      })
      // Mettre à jour le document du jeu
      await updateDoc(gameDocRef, {
        ratingSum: increment(rating),
        ratingCount: increment(1),
      })
      console.log('Note ajoutée avec succès')
    } else {
      // Si une note existe déjà, la mettre à jour
      const oldRating = querySnapshot.docs[0].data().rating
      console.log(`Ancienne note: ${oldRating}, Nouvelle note: ${rating}`)
      const ratingDocId = querySnapshot.docs[0].id
      const ratingDocRef = doc(firestore, 'ratings', ratingDocId)

      await updateDoc(ratingDocRef, {
        rating,
      })

      // Mettre à jour le document du jeu
      await updateDoc(gameDocRef, {
        ratingSum: increment(rating - oldRating),
      })
      console.log('Note mise à jour avec succès')
    }
  } catch (error) {
    console.error('Erreur lors de la notation du jeu:', error)
  }
}
