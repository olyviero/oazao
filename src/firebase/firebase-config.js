import { initializeApp } from 'firebase/app'
import { getAuth } from 'firebase/auth'
import { getFirestore } from 'firebase/firestore'
// import { getAnalytics } from 'firebase/analytics'

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: 'AIzaSyDlPMHgJTyLHlg_2n6DRCesmWtEVGVauec',
  authDomain: 'oazao-68307.firebaseapp.com',
  projectId: 'oazao-68307',
  storageBucket: 'oazao-68307.appspot.com',
  messagingSenderId: '429207544211',
  appId: '1:429207544211:web:757ce5e60c6f3b02398866',
  measurementId: 'G-22X1ZSPXQT',
}

// Initialize Firebase
const app = initializeApp(firebaseConfig)
const auth = getAuth(app)
// export const analytics = getAnalytics(app)

const firestore = getFirestore(app)

export { app, auth, firestore }
