import { auth, firestore } from './firebase-config'
import {
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  signInAnonymously,
  sendPasswordResetEmail,
  sendEmailVerification,
  updatePassword,
  signInWithPopup,
  GoogleAuthProvider,
} from 'firebase/auth'
import { doc, setDoc } from 'firebase/firestore'

// ---------------------------------------------------------------------------------------
// Create Account  - Email/Password
// ---------------------------------------------------------------------------------------
export const doCreateUserWithEmailAndPassword = async (username, email, password) => {
  return createUserWithEmailAndPassword(auth, email, password)
    .then(() => {
      const user = auth.currentUser
      const userDocRef = doc(firestore, 'users', user.uid)
      setDoc(
        userDocRef,
        {
          username: username,
          email: email,
        },
        { merge: true }
      )
        .then(() => {
          console.log("Nom d'utilisateur enregistré avec succès !")
        })
        .catch((error) => {
          console.error("Erreur lors de l'enregistrement du nom d'utilisateur :", error)
        })
    })
    .catch((error) => {
      var errorCode = error.code
      var errorMessage = error.message
      console.error('Erreur de connexion anonyme :', errorCode, errorMessage)
    })
}

// ---------------------------------------------------------------------------------------
// Sign In - Email/Password
// ---------------------------------------------------------------------------------------
export const doSignInWithEmailAndPassword = (email, password) => {
  return signInWithEmailAndPassword(auth, email, password)
}

// ---------------------------------------------------------------------------------------
// Sign In - Anonymous
// ---------------------------------------------------------------------------------------
export const doSignInAnonymously = (username) => {
  return signInAnonymously(auth)
    .then(() => {
      const user = auth.currentUser
      const userDocRef = doc(firestore, 'users', user.uid)
      setDoc(userDocRef, { username: username })
        .then(() => {
          console.log("Nom d'utilisateur enregistré avec succès !")
        })
        .catch((error) => {
          console.error("Erreur lors de l'enregistrement du nom d'utilisateur :", error)
        })
    })
    .catch((error) => {
      var errorCode = error.code
      var errorMessage = error.message
      console.error('Erreur de connexion anonyme :', errorCode, errorMessage)
    })
}

// ---------------------------------------------------------------------------------------
// Sign In - Google
// ---------------------------------------------------------------------------------------
export const doSignInWithGoogle = async () => {
  const provider = new GoogleAuthProvider()
  signInWithPopup(auth, provider)
    .then(async (result) => {
      const credential = GoogleAuthProvider.credentialFromResult(result)
      const token = credential.accessToken
      const user = result.user

      await setDoc(
        doc(firestore, 'users', user.uid),
        {
          username: user.displayName,
          email: user.email,
          photoURL: user.photoURL,
        },
        { merge: true }
      )

      console.log('Utilisateur ajouté à Firestore avec succès !')
    })
    .catch((error) => {
      const email = error.customData.email
      const credential = GoogleAuthProvider.credentialFromError(error)
      console.error("Erreur lors de l'authentification avec Google :", error.code, error.message)
    })
}

// ---------------------------------------------------------------------------------------
// Sign Out
// ---------------------------------------------------------------------------------------
export const doSignOut = () => {
  return auth.signOut()
}

// TODO :
export const doPasswordReset = (email) => {
  return sendPasswordResetEmail(auth, email)
}

export const doPasswordChange = (password) => {
  return updatePassword(auth.currentUser, password)
}

export const doSendEmailVerification = () => {
  return sendEmailVerification(auth.currentUser, {
    url: `${window.location.origin}/home`,
  })
}
