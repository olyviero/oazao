import { useEffect, useState } from 'react'
import gsap from 'gsap'
import { ScrollTrigger } from 'gsap/ScrollTrigger'

gsap.registerPlugin(ScrollTrigger)

export function usePreloadImages(imageArray, requestPerBatch = 30, delayBetweenBatches = 500) {
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    let isCancelled = false
    let loadedBatches = 0 // Compte le nombre de lots chargés

    const preloadImages = async () => {
      for (let i = 0; i < imageArray.length; i += requestPerBatch) {
        // Récupère un lot de x images à charger
        const batch = imageArray.slice(i, i + requestPerBatch)

        // Promesse qui charge un lot d'images
        const promises = batch.map(
          (src) =>
            new Promise((resolve, reject) => {
              const img = new Image()
              img.src = src
              img.onload = resolve
              img.onerror = reject
            })
        )

        try {
          await Promise.all(promises)
          loadedBatches++
          console.log(`Lot ${loadedBatches} chargé.`)
          if (!isCancelled && i + requestPerBatch < imageArray.length) {
            // Attendre un délai avant de charger le prochain lot
            await new Promise((resolve) => setTimeout(resolve, delayBetweenBatches))
          }
        } catch (error) {
          console.error('Erreur de chargement des images:', error)
        }
      }

      if (!isCancelled) {
        setLoading(false)
      }
    }

    preloadImages()

    return () => {
      isCancelled = true
    }
  }, [imageArray, requestPerBatch, delayBetweenBatches])

  return loading
}
