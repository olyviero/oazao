const getCardImageURL = (name) => {
  return new URL(`/imgs/cards-front/${name}`, import.meta.url).href
}

const getGameImageURL = (name) => {
  return new URL(`/imgs/games/${name}`, import.meta.url).href
}

export { getCardImageURL, getGameImageURL }
