// ---------------------------------------------------------------------------------------
// Modals
// ---------------------------------------------------------------------------------------
export const modalMotionProps = {
  variants: {
    enter: {
      y: 0,
      opacity: 1,
      transition: {
        duration: 0.3,
        ease: 'easeOut',
      },
    },
    exit: {
      y: -20,
      opacity: 0,
      transition: {
        duration: 0.2,
        ease: 'easeIn',
      },
    },
  },
}

export const modalClassNames = {
  body: 'py-6',
  backdrop: 'bg-black/90',
  base: 'text-primary max-w-[900px] right-1',
  header: 'border-b-[1px] border-lightgray',
  footer: 'border-t-[1px] border-lightgray',
  closeButton: '',
}

// ---------------------------------------------------------------------------------------
// Sliders
// ---------------------------------------------------------------------------------------
export const sliderClassNames = {
  base: 'w-full',
  filler: 'bg-gradient-to-r from-primary to-secondary',
  // labelWrapper: 'mb-0',
  label: 'font-bold',
  value: 'font-bold text-xl',
  track: 'bg-primary/30',
  thumb: [
    'transition-size',
    'bg-gradient-to-r from-primary to-secondary',
    'data-[dragging=true]:shadow-lg data-[dragging=true]:shadow-black/20',
    'data-[dragging=true]:w-7 data-[dragging=true]:h-7 data-[dragging=true]:after:h-6 data-[dragging=true]:after:w-6',
  ],
  step: 'data-[in-range=true]:bg-black/30 dark:data-[in-range=true]:bg-white/50',
}

// ---------------------------------------------------------------------------------------
// Tabs
// ---------------------------------------------------------------------------------------
export const tabsClassNames = {
  // base: '',
  tabList: 'gap-6 rounded-none border-b border-divider',
  cursor: 'bg-color4',
  // tab: 'max-w-fit px-1 h-12',
  tabContent: 'group-data-[selected=true]:text-purewhite',
  panel: 'bg-transaprent !m-0 !p-0',
}
