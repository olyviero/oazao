import { gamesTags } from '@/datas/gamesTags'
import { gamesCreators } from '@/datas/gamesCreators'
import { gamesTools } from '@/datas/gamesTools'
import { gamesDecks } from '@/datas/gamesDecks'

// ---------------------------------------------------------------------------------------
// Converters ids <==> items
// ---------------------------------------------------------------------------------------
export const objsArrayToIdsArray = (arr) => {
  return arr.length === 0 ? [] : arr.map((el) => el.id)
}

export const idsArrayToObjArray = (arr, items) => {
  if (arr.length === 0) return []

  return arr.map((id) => items.find((el) => el.id === id)).filter((el) => el !== undefined)
}

export const tagsArrayToIdsArray = (arr) => objsArrayToIdsArray(arr)
export const idsArrayToTagsArray = (arr) => idsArrayToObjArray(arr, gamesTags)

export const creatorsArrayToIdsArray = (arr) => objsArrayToIdsArray(arr)
export const idsArrayToCreatorsArray = (arr) => idsArrayToObjArray(arr, gamesCreators)

export const toolsArrayToIdsArray = (arr) => objsArrayToIdsArray(arr)
export const idsArrayToToolsArray = (arr) => idsArrayToObjArray(arr, gamesTools)

export const decksArrayToIdsArray = (arr) => objsArrayToIdsArray(arr)
export const idsArrayToDecksArray = (arr) => idsArrayToObjArray(arr, gamesDecks)

// ---------------------------------------------------------------------------------------
// Converters names <==> items
// ---------------------------------------------------------------------------------------
export const objsArrayToNamesArray = (arr) => {
  return arr.length === 0 ? [] : arr.map((el) => el.name)
}

export const namesArrayToObjArray = (arr, items) => {
  if (arr.length === 0) return []

  return arr.map((name) => items.find((el) => el.name === name)).filter((el) => el !== undefined)
}

export const tagsArrayToNamesArray = (arr) => objsArrayToNamesArray(arr)
export const namesArrayToTagsArray = (arr) => namesArrayToObjArray(arr, gamesTags)

export const creatorsArrayToNamesArray = (arr) => objsArrayToNamesArray(arr)
export const namesArrayToCreatorsArray = (arr) => namesArrayToObjArray(arr, gamesCreators)

export const toolsArrayToNamesArray = (arr) => objsArrayToNamesArray(arr)
export const namesArrayToToolsArray = (arr) => namesArrayToObjArray(arr, gamesTools)

export const decksArrayToNamesArray = (arr) => objsArrayToNamesArray(arr)
export const namesArrayToDecksArray = (arr) => namesArrayToObjArray(arr, gamesDecks)

// ---------------------------------------------------------------------------------------
// Getters
// ---------------------------------------------------------------------------------------
export const getToolByName = (name) => {
  const tool = gamesTools.find((tool) => tool.name === name)
  return tool.id
}
