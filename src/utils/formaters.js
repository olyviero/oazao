export const addLeadingZeroUnder10 = (number) => {
  if (number < 10) {
    return `0${number}`
  }
  return `${number}`
}
