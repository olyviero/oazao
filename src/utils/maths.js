// Algorithme de Fisher-Yates (ou le mélange de Knuth)
const shuffleArray = (array) => {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1)) // nombre aléatoire entre 0 et i
        ;[array[i], array[j]] = [array[j], array[i]] // échange les éléments à indices i et j
    }
    return array
}

export { shuffleArray }
