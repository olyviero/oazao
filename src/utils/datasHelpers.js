import { cardsWord } from '@datas/cardsWord'
import { gamesTools } from '@datas/gamesTools'
import { cardsChemical } from '@datas/cardsChemical'
import { cardsGeography } from '@datas/cardsGeography'
import { cardsPokemon } from '@datas/cardsPokemon'
import { cards100BestArtistsRollingStone } from '@datas/cards100BestArtistsRollingStone'

// ---------------------------------------------------------------------------------------
// Words
// ---------------------------------------------------------------------------------------
export const getWordByCardID = (id) => {
  const card = cardsWord.find((el) => el.cardNb === id)
  return card ? card.cardWord : undefined
}
// ---------------------------------------------------------------------------------------
// Tools
// ---------------------------------------------------------------------------------------
export const getToolByID = (id) => {
  return gamesTools.find((el) => el.id === id)
}

export const getToolByName = (name) => {
  return gamesTools.find((el) => el.component === name)
}

// ---------------------------------------------------------------------------------------
// Cards Chemicals
// ---------------------------------------------------------------------------------------
export const getChemicalByCardID = (id) => {
  return cardsChemical.find((el) => el.cardId === id)
}

// ---------------------------------------------------------------------------------------
// Cards Geography
// ---------------------------------------------------------------------------------------
export const getGeographyByCardID = (id) => {
  return cardsGeography.find((el) => el.cardId === id)
}

// ---------------------------------------------------------------------------------------
// Cards Pokemon
// ---------------------------------------------------------------------------------------
export const getPokemonByCardID = (id) => {
  return cardsPokemon.find((el) => el.cardId === id)
}

// ---------------------------------------------------------------------------------------
// Cards Artists Rolling Stones
// ---------------------------------------------------------------------------------------
export const getArtistByCardID = (id) => {
  return cards100BestArtistsRollingStone.find((el) => el.cardId === id)
}
