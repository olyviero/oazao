export const gradientGreenToRed = [
  '#D22A2C',
  '#BF3B34',
  '#AC4B3C',
  '#9A5C44',
  '#886C4C',
  '#767D54',
  '#648D5C',
  '#529E64',
  '#41AE6C',
  '#89AB54',
]
