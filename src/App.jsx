import { Route, Routes, useNavigate } from 'react-router-dom'
import { useAuth } from '@contexts/AuthContext'
import NavBarDesktop from '@navs/NavBarDesktop'
// import NavBarMobile from '@navs/NavBarMobile'
import NavBarMobile2 from '@navs/NavBarMobile2'

import Login from '@components/auth/Login'
import Register from '@components/auth/Register'

import Home from '@pages/home/Home'
import Game from '@pages/home/Game'

import Profile from '@pages/profile/Profile'
import Settings from '@pages/profile/Settings'
import MyGames from '@pages/profile/MyGames'
import GameFactory from '@pages/profile/GameFactory'

import Presentation from '@pages/concept/Presentation'
import Concept from '@pages/concept/Concept'

import Tools from '@pages/tools/Tools'
import { useEffect } from 'react'

function App() {
  const { user } = useAuth()
  const navigate = useNavigate()

  useEffect(() => {
    if (user) navigate('/home')
  }, [user])

  return (
    <main className="oazao min-h-screen flex-col overflow-x-hidden bg-background pb-24 text-foreground">
      <NavBarDesktop />
      {/* <NavBarMobile /> */}
      <NavBarMobile2 />

      <Routes>
        <>
          <Route index element={<Login />} />
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} />
        </>

        <Route path="/home" element={<Home />} />
        <Route path="/profile" element={<Profile />} />
        <Route path="/profile/settings" element={<Settings />} />
        <Route path="/profile/myGames" element={<MyGames />} />
        <Route path="/games" element={<Game />} />
        <Route path="/games/:gameId" element={<Game />} />
        <Route path="/presentation" element={<Presentation />} />
        <Route path="/concept" element={<Concept />} />
        <Route path="/tools" element={<Tools />} />
        <Route path="/tools/:toolId" element={<Tools />} />
        <Route path="/tools/:toolId/group/:groupId" element={<Tools />} />

        <Route path="/gamefactory/:gameKey" element={<GameFactory />} />
        <Route path="/gamefactory" element={<GameFactory />} />
      </Routes>
    </main>
  )
}

export default App
