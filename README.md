# OAZAO

## TODO

- filtres de jeux saved in sessionStorage for ease 'session" not "local"

- mes votes / mes commentaires + admin = refresh
- mes notifications
- remove anonymous co => use magiclink if no google
- add facebook co
- fix google avatar : save in firebase storage + local on auth, edit => update both

- add Form : Gather Emails !!!

- useMemo, memo, useCallback : ttmc questions, concept
- fix erreur 429 on concept page : seems to work but in case check: https://docs.gitlab.com/ee/administration/pages/index.html#rate-limits

- list feedbacks for admin somewhere
- add rating system (loggedin users only)

## SECURITY

- fix can vote twice if fast double click on "Vote"
- edit games : security in firebase only my games !!!!
- SAUVEGARDER BDD FIREBASE ??
- context (isMe) : if (user.uid === 'PZgb0ZlXeZSSifgIaAm7xt8Qifi1') setIsMe(true) => dans .env

## FEATS

- refresh local games on game update/create
- refresh my ratings => localstorage delete and set
- new tool : random player selection
- WIP concept page 2 to finish
- page histoire ?

- cta: preorder !!!
- account creation => sendingblue auto
- image banner format for game page

## Layout

- use layout components everywhere : consistancy

# Librairies / Utils

- react simple star rating : https://react-simple-star-rating.vercel.app/?path=/docs/example--default
- icons: https://react-icons.github.io/react-icons/
